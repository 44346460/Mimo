package com.mimo.comet.dao;

import java.util.Collection;
import java.util.Optional;

import com.mimo.comet.provider.ISession;

public interface ISessionDao {

  Collection<ISession> listAll();

  /**
   * 获取特定用户的会话详情
   * 
   * @param uid
   * @return
   */
  Optional<ISession> findByUid(String uid);

  /**
   * 获取当前会话总数
   * 
   * @return
   */
  long getSize();

  /**
   * 用于创建会话
   * 
   * @param session
   */
  void create(ISession session);

  /**
   * 清空会话
   * 
   * @param uid
   */
  Optional<ISession> remove(String uid);

}
