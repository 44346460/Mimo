package com.mimo.comet.user.service;

import java.util.List;

import org.springframework.lang.Nullable;

import com.mimo.comet.provider.ISession;
import com.mimo.common.comet.dto.user.UserLoginReq;
import com.mimo.common.logic.code.StatusCode;
import com.mimo.common.logic.dto.msg.BaseDispatchMessage;

public interface IUserService {

  /**
   * 返回待确认的消息
   * 
   * @param userId
   * @return
   */
  List<BaseDispatchMessage> getPendingMessage(String userId);

  /**
   * 登陆验证
   * 
   * @param req
   * @return
   */
  public StatusCode login(UserLoginReq req);

  public void logout(String userId, @Nullable String msgId);

  /**
   * 验证通过后，再手动做用户与本地会话关系绑定
   * 
   * @param userId
   * @param session
   *          rerturn 由于并发绑定的情况,需要一个标记符用于维护此次绑定是否成功。一般来说，如果绑定的时候,之前的会话还没有清除，则不允许绑定
   */
  public boolean binding(String userId, ISession session);

  /**
   * 解除用户与本地会话关系绑定，一般都是对应着session的清除，同时提供 一下设备ID，用于检测不同设备之意的抢登
   * 
   * @param userId
   * @param msgId
   *          用于标记，当前这个解绑是因为某个指令ID触发。如果有的话，则需要在解绑的同时，进行消息响应
   * @param deviceId
   *          该字段为整个上下文里看到的设备ID, 用于绑定用户时，查看其设备ID，以返回正确的码
   */
  public void unbinding(String userId, @Nullable String msgId, String deviceId);

  /**
   * 解除用户与本地会话关系绑定，一般都是对应着session的清除
   * 
   * @param userId
   * @param msgId
   *          用于标记，当前这个解绑是因为某个指令ID触发。如果有的话，则需要在解绑的同时，进行消息响应
   */
  public void unbinding(String userId, @Nullable String msgId);

}
