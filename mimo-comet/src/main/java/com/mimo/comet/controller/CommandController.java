package com.mimo.comet.controller;

import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mimo.comet.provider.command.AckCommand;
import com.mimo.comet.provider.command.HeartBeatCommand;
import com.mimo.comet.provider.command.JoinRoomCommand;
import com.mimo.comet.provider.command.LogoutCommand;
import com.mimo.comet.provider.command.P2PMessageCommand;

import io.swagger.annotations.ApiOperation;

@Validated
@RestController
@RequestMapping("/command")
public class CommandController {

  @GetMapping("/ack")
  @ApiOperation(value = "客户端确认指令", response = AckCommand.class)
  public Object ack() {
    return "{\"id\":\"123123\",\"type\":\"Ack\",\"target\":\"abd\"}";
  }

  @GetMapping("/heartbeat")
  @ApiOperation(value = "客户端心跳指令", response = HeartBeatCommand.class)
  public Object heartBeat() {
    return "{\"id\":\"123123\",\"type\":\"HeartBeat\"}";
  }

  @GetMapping("/logout")
  @ApiOperation(value = "客户端主动退登指令", response = LogoutCommand.class)
  public Object logout() {
    return "{\"id\":\"123123\",\"type\":\"Logout\"}";
  }

  @GetMapping("/room/list")
  @ApiOperation(value = "客户端主动加入房间指令", response = JoinRoomCommand.class)
  public Object listRoom() {
    return "{\"id\":\"908eb864524b4963b28416ef3ffe8bc2\",\"type\":\"ListRooms\"}";
  }

  @GetMapping("/room/join")
  @ApiOperation(value = "客户端主动加入房间指令", response = JoinRoomCommand.class)
  public Object joinRoom() {
    return "{\"id\":\"908eb864524b4963b28416ef3ffe8bc2\",\"type\":\"JoinRoom\",\"target\":\"hy_001\"}";
  }

  @GetMapping("/room/leave")
  @ApiOperation(value = "客户端主动退出房间指令", response = JoinRoomCommand.class)
  public Object leaveRoom() {
    return "{\"id\":\"908eb864524b4963b28416ef3ffe8bc2\",\"type\":\"LeaveRoom\",\"target\":\"hy_001\"}";
  }

  @GetMapping("/p2p")
  @ApiOperation(value = "客户端主动发送私信（P2P）指令", response = P2PMessageCommand.class)
  public Object p2p() {
    return "{\"id\":\"36e92766f4344377b504ebca36a00f28\",\"type\":\"P2P\",\"target\":\"hongyu\",\"content\":\"点对点消息测试\"}";
  }

  @GetMapping("/p2room")
  @ApiOperation(value = "客户端主动发送私信（P2P）指令", response = P2PMessageCommand.class)
  public Object p2room() {
    return "{\"id\":\"2e71ebb1a0444c2f8b193d1837ab6477\",\"type\":\"P2Room\",\"content\":\"点对房间消息\",\"target\":\"hy_001\"}";
  }

}
