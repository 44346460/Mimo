package com.mimo.comet.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mimo.comet.config.LocalCometServerConfig;
import com.mimo.common.result.BaseResult;
import com.mimo.common.result.CommonDTOResult;

@Validated
@RestController
@RequestMapping("/server")
public class ServerController {

  @Autowired
  private LocalCometServerConfig localCometServerConfig;

  @GetMapping("/info")
  public BaseResult getServerInfo() {
    return new CommonDTOResult<>(localCometServerConfig);
  }

}
