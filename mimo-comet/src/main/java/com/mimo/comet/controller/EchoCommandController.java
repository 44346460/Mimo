package com.mimo.comet.controller;

import java.util.Arrays;

import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mimo.comet.provider.command.echo.AckEchoCommand;
import com.mimo.comet.provider.command.echo.ErrorEchoCommand;
import com.mimo.comet.provider.command.echo.ListRoomsEchoCommand;

import io.swagger.annotations.ApiOperation;

@Validated
@RestController
@RequestMapping("/echo")
public class EchoCommandController {

  @GetMapping("/ack")
  @ApiOperation(value = "服务端确认响应", response = AckEchoCommand.class)
  public Object ack() {
    return "{\"id\":\"123123\",\"type\":\"Ack\",\"target\":\"abd\"}";
  }

  @GetMapping("/error")
  @ApiOperation(value = "服务端主动发送错误响应", response = ErrorEchoCommand.class)
  public Object p2p() {
    return "{\"id\":\"36e92766f4344377b504ebca36a00f28\",\"type\":\"P2P\",\"target\":\"hongyu\",\"content\":\"点对点消息测试\"}";
  }

  @GetMapping("/listrooms")
  @ApiOperation(value = "服务端根据用户ID返回已经加入的房间的响应", response = ListRoomsEchoCommand.class)
  public Object listRooms() {
    ListRoomsEchoCommand cmd = new ListRoomsEchoCommand();
    cmd.setRooms(Arrays.asList("Room_Hongyu_1", "Room_Hongyu_2"));
    cmd.setTarget("hongyu");
    return cmd;
  }
}
