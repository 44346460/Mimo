package com.mimo.comet.controller;

import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mimo.common.logic.dto.msg.P2PMessage;
import com.mimo.common.logic.dto.msg.Room2PeerMessage;
import com.mimo.common.utils.RandomStringUtils;

import io.swagger.annotations.ApiOperation;

@Validated
@RestController
@RequestMapping("/message")
public class MessageDispatchController {

  @GetMapping("/p2p")
  @ApiOperation(value = "客户端收到的消息分发（点对点）", response = P2PMessage.class)
  public Object p2p() {
    return "{\"id\":\"b6b94bdfd9a04588ba8be87be06104a5\",\"type\":\"P2P\",\"from\":\"test\",\"target\":\"hongyu\",\"content\":\"AAAAAAAAAAA\"}";
  }

  @GetMapping("/r2p")
  @ApiOperation(value = "客户端收到的消息分发（房间消息分发）", response = Room2PeerMessage.class)
  public Object r2p() {
    Room2PeerMessage m = new Room2PeerMessage();
    m.setId(RandomStringUtils.uniqueRandom());
    m.setFrom("hongyu");
    m.setTarget("test1");
    m.setContent("Helo world");
    m.setRoomId("Room_hongyu_1");
    m.setOriginalId("b6b94bdfd9a04588ba8be87be06104a5");
    return m;
  }

}
