package com.mimo.comet.config;

import static springfox.documentation.builders.PathSelectors.regex;

import java.util.Collections;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.context.request.async.DeferredResult;

import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;

@Configuration
public class SwaggerConfig {

  @Bean
  public Docket adminApi() {
    // @formatter:off
    return new Docket(DocumentationType.SWAGGER_2).groupName("Session").genericModelSubstitutes(DeferredResult.class)
        .useDefaultResponseMessages(false).forCodeGeneration(false).pathMapping("/").select()
        .paths(regex("/session.*"))// 过滤的接口
        .build().apiInfo(new ApiInfo("本地绑定的会话辅助接口", // title
            "查询", // description
            "0.1.0", // version
            "", // termsOfServiceUrl
            new Contact("Hongyu", "", ""), // contact
            "", // license
            "",// licenseUrl
            Collections.emptyList()
    ));
    // @formatter:on
  }

  @Bean
  public Docket serverApi() {
    // @formatter:off
    return new Docket(DocumentationType.SWAGGER_2).groupName("Server").genericModelSubstitutes(DeferredResult.class)
        .useDefaultResponseMessages(false).forCodeGeneration(false).pathMapping("/").select()
        .paths(regex("/server.*"))// 过滤的接口
        .build().apiInfo(new ApiInfo("当前本机初始化信息", // title
            "当前本机初始化信息", // description
            "0.1.0", // version
            "", // termsOfServiceUrl
            new Contact("Hongyu", "", ""), // contact
            "", // license
            "",// licenseUrl
            Collections.emptyList()
    ));
    // @formatter:on
  }

  @Bean
  public Docket commandApi() {
    // @formatter:off
    return new Docket(DocumentationType.SWAGGER_2).groupName("Command").genericModelSubstitutes(DeferredResult.class)
        .useDefaultResponseMessages(false).forCodeGeneration(false).pathMapping("/").select()
        .paths(regex("/command.*"))// 过滤的接口
        .build().apiInfo(new ApiInfo("客户端发起的请求指令样本", // title
            "客户端发起的请求指令样本", // description
            "0.1.0", // version
            "", // termsOfServiceUrl
            new Contact("Hongyu", "", ""), // contact
            "", // license
            "",// licenseUrl
            Collections.emptyList()
    ));
    // @formatter:on
  }

  @Bean
  public Docket echoApi() {
    // @formatter:off
    return new Docket(DocumentationType.SWAGGER_2).groupName("Echo").genericModelSubstitutes(DeferredResult.class)
        .useDefaultResponseMessages(false).forCodeGeneration(false).pathMapping("/").select()
        .paths(regex("/echo.*"))// 过滤的接口
        .build().apiInfo(new ApiInfo("服务端向客户端请求指令所做的响应样本", // title
            "服务端向客户端请求指令所做的响应样本", // description
            "0.1.0", // version
            "", // termsOfServiceUrl
            new Contact("Hongyu", "", ""), // contact
            "", // license
            "",// licenseUrl
            Collections.emptyList()
    ));
    // @formatter:on
  }

  @Bean
  public Docket messageApi() {
    // @formatter:off
    return new Docket(DocumentationType.SWAGGER_2).groupName("Message").genericModelSubstitutes(DeferredResult.class)
        .useDefaultResponseMessages(false).forCodeGeneration(false).pathMapping("/").select()
        .paths(regex("/message.*"))// 过滤的接口
        .build().apiInfo(new ApiInfo("服务端向客户端分发的消息样本", // title
            "P2P, Room2Peer", // description
            "0.1.0", // version
            "", // termsOfServiceUrl
            new Contact("Hongyu", "", ""), // contact
            "", // license
            "",// licenseUrl
            Collections.emptyList()
    ));
    // @formatter:on
  }
}
