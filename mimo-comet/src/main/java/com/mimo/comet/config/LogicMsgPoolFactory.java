package com.mimo.comet.config;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.stereotype.Component;

import com.mimo.common.rpc.proto.LogicMessageProxyGrpc;
import com.mimo.common.rpc.proto.LogicMessageProxyGrpc.LogicMessageProxyBlockingStub;

import io.grpc.netty.shaded.io.netty.util.internal.ThreadLocalRandom;
import net.devh.boot.grpc.client.inject.GrpcClient;

@Component
public class LogicMsgPoolFactory {

  private final List<LogicMessageProxyGrpc.LogicMessageProxyBlockingStub> holder = new ArrayList<>();

  @PostConstruct
  public void init() throws IllegalAccessException {
    Field[] fields = this.getClass().getDeclaredFields();
    for (Field f : fields) {
      f.setAccessible(true);
      if (f.getType() == LogicMessageProxyGrpc.LogicMessageProxyBlockingStub.class) {
        holder.add((LogicMessageProxyBlockingStub) f.get(this));
      }
    }
  }

  @GrpcClient(value = "logic-messge-1")
  private LogicMessageProxyGrpc.LogicMessageProxyBlockingStub logicMessageProxyStub1;
  @GrpcClient(value = "logic-messge-2")
  private LogicMessageProxyGrpc.LogicMessageProxyBlockingStub logicMessageProxyStub2;
  @GrpcClient(value = "logic-messge-3")
  private LogicMessageProxyGrpc.LogicMessageProxyBlockingStub logicMessageProxyStub3;
  @GrpcClient(value = "logic-messge-4")
  private LogicMessageProxyGrpc.LogicMessageProxyBlockingStub logicMessageProxyStub4;
  @GrpcClient(value = "logic-messge-5")
  private LogicMessageProxyGrpc.LogicMessageProxyBlockingStub logicMessageProxyStub5;
  @GrpcClient(value = "logic-messge-6")
  private LogicMessageProxyGrpc.LogicMessageProxyBlockingStub logicMessageProxyStub6;
  @GrpcClient(value = "logic-messge-7")
  private LogicMessageProxyGrpc.LogicMessageProxyBlockingStub logicMessageProxyStub7;
  @GrpcClient(value = "logic-messge-8")
  private LogicMessageProxyGrpc.LogicMessageProxyBlockingStub logicMessageProxyStub8;
  @GrpcClient(value = "logic-messge-9")
  private LogicMessageProxyGrpc.LogicMessageProxyBlockingStub logicMessageProxyStub9;
  @GrpcClient(value = "logic-messge-10")
  private LogicMessageProxyGrpc.LogicMessageProxyBlockingStub logicMessageProxyStub10;

  @GrpcClient(value = "logic-messge-11")
  private LogicMessageProxyGrpc.LogicMessageProxyBlockingStub logicMessageProxyStub11;
  @GrpcClient(value = "logic-messge-12")
  private LogicMessageProxyGrpc.LogicMessageProxyBlockingStub logicMessageProxyStub12;
  @GrpcClient(value = "logic-messge-13")
  private LogicMessageProxyGrpc.LogicMessageProxyBlockingStub logicMessageProxyStub13;
  @GrpcClient(value = "logic-messge-14")
  private LogicMessageProxyGrpc.LogicMessageProxyBlockingStub logicMessageProxyStub14;
  @GrpcClient(value = "logic-messge-15")
  private LogicMessageProxyGrpc.LogicMessageProxyBlockingStub logicMessageProxyStub15;
  @GrpcClient(value = "logic-messge-16")
  private LogicMessageProxyGrpc.LogicMessageProxyBlockingStub logicMessageProxyStub16;
  @GrpcClient(value = "logic-messge-17")
  private LogicMessageProxyGrpc.LogicMessageProxyBlockingStub logicMessageProxyStub17;
  @GrpcClient(value = "logic-messge-18")
  private LogicMessageProxyGrpc.LogicMessageProxyBlockingStub logicMessageProxyStub18;
  @GrpcClient(value = "logic-messge-19")
  private LogicMessageProxyGrpc.LogicMessageProxyBlockingStub logicMessageProxyStub19;
  @GrpcClient(value = "logic-messge-20")
  private LogicMessageProxyGrpc.LogicMessageProxyBlockingStub logicMessageProxyStub20;

  @GrpcClient(value = "logic-messge-21")
  private LogicMessageProxyGrpc.LogicMessageProxyBlockingStub logicMessageProxyStub21;
  @GrpcClient(value = "logic-messge-22")
  private LogicMessageProxyGrpc.LogicMessageProxyBlockingStub logicMessageProxyStub22;
  @GrpcClient(value = "logic-messge-23")
  private LogicMessageProxyGrpc.LogicMessageProxyBlockingStub logicMessageProxyStub23;
  @GrpcClient(value = "logic-messge-24")
  private LogicMessageProxyGrpc.LogicMessageProxyBlockingStub logicMessageProxyStub24;
  @GrpcClient(value = "logic-messge-25")
  private LogicMessageProxyGrpc.LogicMessageProxyBlockingStub logicMessageProxyStub25;
  @GrpcClient(value = "logic-messge-26")
  private LogicMessageProxyGrpc.LogicMessageProxyBlockingStub logicMessageProxyStub26;
  @GrpcClient(value = "logic-messge-27")
  private LogicMessageProxyGrpc.LogicMessageProxyBlockingStub logicMessageProxyStub27;
  @GrpcClient(value = "logic-messge-28")
  private LogicMessageProxyGrpc.LogicMessageProxyBlockingStub logicMessageProxyStub28;
  @GrpcClient(value = "logic-messge-29")
  private LogicMessageProxyGrpc.LogicMessageProxyBlockingStub logicMessageProxyStub29;
  @GrpcClient(value = "logic-messge-30")
  private LogicMessageProxyGrpc.LogicMessageProxyBlockingStub logicMessageProxyStub30;

  @GrpcClient(value = "logic-messge-31")
  private LogicMessageProxyGrpc.LogicMessageProxyBlockingStub logicMessageProxyStub31;
  @GrpcClient(value = "logic-messge-32")
  private LogicMessageProxyGrpc.LogicMessageProxyBlockingStub logicMessageProxyStub32;
  @GrpcClient(value = "logic-messge-33")
  private LogicMessageProxyGrpc.LogicMessageProxyBlockingStub logicMessageProxyStub33;
  @GrpcClient(value = "logic-messge-34")
  private LogicMessageProxyGrpc.LogicMessageProxyBlockingStub logicMessageProxyStub34;
  @GrpcClient(value = "logic-messge-35")
  private LogicMessageProxyGrpc.LogicMessageProxyBlockingStub logicMessageProxyStub35;
  @GrpcClient(value = "logic-messge-36")
  private LogicMessageProxyGrpc.LogicMessageProxyBlockingStub logicMessageProxyStub36;
  @GrpcClient(value = "logic-messge-37")
  private LogicMessageProxyGrpc.LogicMessageProxyBlockingStub logicMessageProxyStub37;
  @GrpcClient(value = "logic-messge-38")
  private LogicMessageProxyGrpc.LogicMessageProxyBlockingStub logicMessageProxyStub38;
  @GrpcClient(value = "logic-messge-39")
  private LogicMessageProxyGrpc.LogicMessageProxyBlockingStub logicMessageProxyStub39;
  @GrpcClient(value = "logic-messge-40")
  private LogicMessageProxyGrpc.LogicMessageProxyBlockingStub logicMessageProxyStub40;

  @GrpcClient(value = "logic-messge-41")
  private LogicMessageProxyGrpc.LogicMessageProxyBlockingStub logicMessageProxyStub41;
  @GrpcClient(value = "logic-messge-42")
  private LogicMessageProxyGrpc.LogicMessageProxyBlockingStub logicMessageProxyStub42;
  @GrpcClient(value = "logic-messge-43")
  private LogicMessageProxyGrpc.LogicMessageProxyBlockingStub logicMessageProxyStub43;
  @GrpcClient(value = "logic-messge-44")
  private LogicMessageProxyGrpc.LogicMessageProxyBlockingStub logicMessageProxyStub44;
  @GrpcClient(value = "logic-messge-45")
  private LogicMessageProxyGrpc.LogicMessageProxyBlockingStub logicMessageProxyStub45;
  @GrpcClient(value = "logic-messge-46")
  private LogicMessageProxyGrpc.LogicMessageProxyBlockingStub logicMessageProxyStub46;
  @GrpcClient(value = "logic-messge-47")
  private LogicMessageProxyGrpc.LogicMessageProxyBlockingStub logicMessageProxyStub47;
  @GrpcClient(value = "logic-messge-48")
  private LogicMessageProxyGrpc.LogicMessageProxyBlockingStub logicMessageProxyStub48;
  @GrpcClient(value = "logic-messge-49")
  private LogicMessageProxyGrpc.LogicMessageProxyBlockingStub logicMessageProxyStub49;
  @GrpcClient(value = "logic-messge-50")
  private LogicMessageProxyGrpc.LogicMessageProxyBlockingStub logicMessageProxyStub50;

  public LogicMessageProxyGrpc.LogicMessageProxyBlockingStub getLogicMessageStub() {
    return holder.get(ThreadLocalRandom.current().nextInt(holder.size()));
  }
}
