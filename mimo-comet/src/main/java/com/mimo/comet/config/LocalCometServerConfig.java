package com.mimo.comet.config;

import javax.annotation.PostConstruct;
import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.validation.annotation.Validated;

import com.mimo.common.comet.dto.ZoneDTO;
import com.mimo.common.utils.RandomStringUtils;

@Validated
@ConfigurationProperties(prefix = "mimo.comet.server")
public class LocalCometServerConfig {

    /**
     * 每个应用实例的ID需保持唯一性
     */
    private String applicationId = RandomStringUtils.uniqueRandom();

    /**
     * Redis pub/sub 监听本地新的会话连接
     */
    @NotEmpty
    private String clusterChannel;

    /**
     * Redis pub/sub 监听logic session 过期时的强制断开信息
     */
    @NotEmpty
    private String logicExpiredChannel;

    /**
     * 用于获取本机的配置信息
     */
    @NotNull
    @Valid
    private ZoneDTO zone;

    private com.mimo.common.rpc.proto.UserProto.ZoneDTO protoZone;

    @PostConstruct
    private void init() {
        protoZone = ZoneDTO.convert(zone);
    }

    public com.mimo.common.rpc.proto.UserProto.ZoneDTO getLocalProtoZone() {
        return this.protoZone;
    }

    public String getLogicExpiredChannel() {
        return logicExpiredChannel;
    }

    public void setLogicExpiredChannel(String logicExpiredChannel) {
        this.logicExpiredChannel = logicExpiredChannel;
    }

    public String getClusterChannel() {
        return clusterChannel;
    }

    public void setClusterChannel(String clusterChannel) {
        this.clusterChannel = clusterChannel;
    }

    public ZoneDTO getZone() {
        return zone;
    }

    public void setZone(ZoneDTO zone) {
        this.zone = zone;
    }

    public String getApplicationId() {
        return applicationId;
    }

    public void setApplicationId(String applicationId) {
        this.applicationId = applicationId;
    }

}
