package com.mimo.comet.constants;

/**
 * 用于记录C-S之间的连接类型
 * 
 * @author Hongyu
 */
public enum CometType {
  // WS, 基于成本考虑，这是目前主要的接入方式。同时兼容三端
  WebSocket,

  // 自定义长连接实现
  TCP
}
