package com.mimo.comet.constants;

public abstract class UserKeys {
  /**
   * 基于用户的锁操作
   */
  public static final String COMET_USER_LOCKER_PREFIX = "locker:comet:user:";
}
