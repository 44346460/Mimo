package com.mimo.comet.provider.command;

/**
 * {"id":"2e71ebb1a0444c2f8b193d1837ab6477","type":"P2Room","content":"点对房间消息","target":"hy_001"}
 * 
 * @author Hongyu
 */
public class P2RoomMessageCommand extends AbstractMessageCommand {

  public P2RoomMessageCommand() {
    super();
    this.type = CommandType.P2Room;
  }

}
