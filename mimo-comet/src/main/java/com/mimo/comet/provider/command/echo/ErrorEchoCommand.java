package com.mimo.comet.provider.command.echo;

import org.springframework.util.Assert;

import com.mimo.common.logic.code.StatusCode;

public class ErrorEchoCommand extends BaseEchoCommand {

  protected int code;

  protected String msg;

  public ErrorEchoCommand() {
    super();
    this.type = EchoType.Error;
  }

  public static ErrorEchoCommand fromStatusCode(String sourceMsgId, StatusCode statusCode) {
    Assert.isTrue(!statusCode.isSuccess(), "该方法只用于非成功状态码的转换");
    ErrorEchoCommand echo = new ErrorEchoCommand();
    echo.setId(sourceMsgId);
    echo.setCode(statusCode.getCode());
    echo.setMsg(statusCode.getMsg());
    return echo;
  }

  public int getCode() {
    return code;
  }

  public void setCode(int code) {
    this.code = code;
  }

  public String getMsg() {
    return msg;
  }

  public void setMsg(String msg) {
    this.msg = msg;
  }

}
