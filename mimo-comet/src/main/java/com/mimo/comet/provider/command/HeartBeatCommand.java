package com.mimo.comet.provider.command;

public class HeartBeatCommand extends BaseCommand {

  public HeartBeatCommand() {
    super();
    this.type = CommandType.HeartBeat;
  }

}
