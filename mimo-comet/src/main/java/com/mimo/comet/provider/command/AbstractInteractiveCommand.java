package com.mimo.comet.provider.command;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.StringUtils;

import io.swagger.annotations.ApiModelProperty;

/**
 * 高层的交互性指令
 * 
 * @author Hongyu
 */
public abstract class AbstractInteractiveCommand extends BaseCommand {
  private static final Logger log = LoggerFactory.getLogger(AbstractInteractiveCommand.class);

  /**
   * 目标ID最大长度为32位
   */
  protected static final int MAX_TARGET_SIZE = 32;

  /**
   * 根据指令类型，表达不同的目标对象。
   * <li>Ack，则代表确认的消息ID
   * <li>JoinRoom/LeaveRoom/P2Room,则代表房间ID
   * <li>P2P,则代表目标用户ID
   */
  @ApiModelProperty(value = "根据指令类型，表达不同的目标对象. <br>1.Ack，则代表确认的消息ID <br>2. JoinRoom/LeaveRoom/P2Room,则代表房间ID <br>3.P2P,则代表目标用户ID", required = true)
  protected String target;

  @Override
  public boolean isValid() {
    boolean isValid = super.isValid();
    if (isValid && (StringUtils.isEmpty(target) || target.length() > MAX_TARGET_SIZE)) {
      isValid = false;
      log.error("TARGET[{}] 长度非法", target);
    }
    return isValid;
  }

  public String getTarget() {
    return target;
  }

  public void setTarget(String target) {
    this.target = target;
  }

}
