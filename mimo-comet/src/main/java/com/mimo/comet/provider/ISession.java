package com.mimo.comet.provider;

import java.util.Map;

import com.mimo.comet.constants.CometType;

public interface ISession {

  /************************* 会话上下文信息绑定 *******************************/
  public void addAttribute(String key, String value);

  public String getAttribute(String key);

  public Map<String, String> getAttributeMap();

  public String removeAttribute(String key);

  /**
   * 有些特殊场景用于返回被代理的具体会话对象
   * 
   * @return
   */
  public Object getDelegate();

  /**
   * 返回该会话所对应的唯一标识
   * 
   * @return
   */
  public String getSessionId();

  /**
   * 关闭当前会话上下文
   * <p>
   * 需要保证重复调用 ，不会报错。即已经处理关闭状态的session,即使多次调用close，也应该是默认成功
   */
  public void close();

  public void write(String data);

  /**
   * 用于心跳保持
   */
  public void touch();

  /**
   * 用于确认最近一次连接跃的时间，主要是为了控制连接的存活有效性
   * 
   * @return
   */
  public long getLastAccessTime();

  /**
   * 用于判断当前连接状态
   * 
   * @return
   */
  public boolean isClosed();

  /**
   * 用于表达当前会话的所属类型
   * 
   * @return
   */
  public CometType getCometType();

  /**
   * 用于获取当前会话的绑定设备类型
   * <li>ANDROID
   * <li>IOS
   * <li>WEB
   */
  public String getTerminalType();

}
