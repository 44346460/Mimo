package com.mimo.comet.provider.command;

/**
 * {"id":"908eb864524b4963b28416ef3ffe8bc2","type":"LeaveRoom","target":"hy_001"}
 * 
 * @author Hongyu
 */
public class LeaveRoomCommand extends AbstractInteractiveCommand {

  public LeaveRoomCommand() {
    super();
    this.type = CommandType.LeaveRoom;
  }

}
