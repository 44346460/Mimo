package com.mimo.comet.provider.command.echo;

public enum EchoType {
  /*** 确认响应类 **/
  Ack(AckEchoCommand.class),

  /**
   * 用于告诉客户端错误的原因
   */
  Error(ErrorEchoCommand.class),

  /**
   * 查询房间信息
   */
  ListRooms(ListRoomsEchoCommand.class),

  /**
   * 登陆成功的响应
   */
  LoginSuccess(LoginSuccessEchoCommand.class),

  /**
   * 检索未应答消息的响应
   */
  RetrieveUnAck(RetrieveUnAckEchoCommand.class),

  //
  ;

  private Class<? extends BaseEchoCommand> cmdClz;

  EchoType(Class<? extends BaseEchoCommand> cmdClz) {
    this.cmdClz = cmdClz;
  }

  public Class<? extends BaseEchoCommand> getCmdClz() {
    return cmdClz;
  }
}
