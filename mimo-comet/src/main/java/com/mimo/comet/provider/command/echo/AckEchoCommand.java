package com.mimo.comet.provider.command.echo;

public class AckEchoCommand extends BaseEchoCommand {
  public AckEchoCommand() {
    super();
    this.type = EchoType.Ack;
  }
}
