package com.mimo.comet.provider.command;

/**
 * {"id":"908eb864524b4963b28416ef3ffe8bc2","type":"JoinRoom","target":"hy_001"}
 * 
 * @author Hongyu
 */
public class JoinRoomCommand extends AbstractInteractiveCommand {

  public JoinRoomCommand() {
    super();
    this.type = CommandType.JoinRoom;
  }

}
