package com.mimo.comet.provider.command;

import java.util.Objects;

/**
 * @author Hongyu etc-> {"id":"123123","type":"Ack","target":"abd"}
 */
public class AckCommand extends AbstractInteractiveCommand {
  public AckCommand() {
    super();
    this.type = CommandType.Ack;
  }

  public AckCommand(String targetMsgId) {
    this();
    Objects.requireNonNull(targetMsgId, "targetMsgId");
    this.target = targetMsgId;
  }

}
