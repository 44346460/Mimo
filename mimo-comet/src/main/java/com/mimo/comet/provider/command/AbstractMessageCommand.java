package com.mimo.comet.provider.command;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.mimo.common.utils.MessageUtil;

import io.swagger.annotations.ApiModelProperty;

/**
 * 主动消息发送类型
 * 
 * @author Hongyu
 */
public class AbstractMessageCommand extends AbstractInteractiveCommand {
  private static final Logger log = LoggerFactory.getLogger(AbstractMessageCommand.class);

  @ApiModelProperty(value = "主动消息内容,长度最大不能超过128K", required = true)
  protected String content;

  @Override
  public boolean isValid() {
    boolean isValid = super.isValid();
    if (isValid && !MessageUtil.isValidLength(content)) {
      isValid = false;
      log.error("content[{}] 长度非法", content);
    }
    return isValid;
  }

  public String getContent() {
    return content;
  }

  public void setContent(String content) {
    this.content = content;
  }

}
