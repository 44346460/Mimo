package com.mimo.comet.provider;

import com.mimo.comet.constants.CometType;
import com.mimo.comet.provider.command.BaseCommand;
import com.mimo.comet.provider.command.echo.BaseEchoCommand;

/**
 * 定义一套高层抽象的IM操作接口集合, 至于是用Websocket还是TCP, 或者是HTTP轮询, 则由具体的SPI实现
 * 
 * @author Hongyu
 */
public interface ICommandProvider<T extends BaseCommand, S extends BaseEchoCommand> {

  /**
   * 用于标记当前的处理器，用于处理哪一类的连接方式
   */
  public CometType getSupportCometType();

  /**
   * 核心处理入口
   * 
   * @param userId
   * @param cmd
   * @return 如果返回值为非null值,则会把S当成此次处理结果返回给客户端
   */
  public S process(String userId, T cmd);

  /**
   * 考虑消息路由，添加一个支持处理判断项
   * 
   * @param cmd
   * @return
   */
  public boolean support(T cmd);
}
