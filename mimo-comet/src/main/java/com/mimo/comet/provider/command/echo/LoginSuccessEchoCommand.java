package com.mimo.comet.provider.command.echo;

public class LoginSuccessEchoCommand extends BaseEchoCommand {
  public LoginSuccessEchoCommand() {
    super();
    this.type = EchoType.LoginSuccess;
  }
}
