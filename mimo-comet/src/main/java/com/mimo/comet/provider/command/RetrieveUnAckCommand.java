package com.mimo.comet.provider.command;

/**
 * 检索未应答消息。未应答消息堆积的主要原因是用户离线
 * 
 */
public class RetrieveUnAckCommand extends BaseCommand{

  public RetrieveUnAckCommand() {
    super();
    this.type = CommandType.RetrieveUnAck;
  }
}
