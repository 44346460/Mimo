package com.mimo.comet.provider.command;

public enum CommandType {

  /**
   * 检索未应答消息
   */
  RetrieveUnAck(RetrieveUnAckCommand.class),

  /**
   * 用于获取自己加入的所有房间
   */
  ListRooms(ListRoomsCommand.class),

  /**
   * 客户端主动退出时指令
   */
  Logout(LogoutCommand.class),

  /**
   * 心跳包
   */
  HeartBeat(HeartBeatCommand.class),

  /**
   * 消息确认
   */
  Ack(AckCommand.class),

  /**
   * 加入房间
   */
  JoinRoom(JoinRoomCommand.class),

  /**
   * 离开房间
   */
  LeaveRoom(LeaveRoomCommand.class),

  /**
   * 点对点消息
   */
  P2P(P2PMessageCommand.class),

  /**
   * 房间消息投递
   */
  P2Room(P2RoomMessageCommand.class)
  //
  ;

  private Class<? extends BaseCommand> cmdClz;

  CommandType(Class<? extends BaseCommand> cmdClz) {
    this.cmdClz = cmdClz;
  }

  public Class<? extends BaseCommand> getCmdClz() {
    return cmdClz;
  }
}
