package com.mimo.comet.provider.command.echo;

import java.util.List;

import com.mimo.common.logic.dto.msg.BaseDispatchMessage;

import io.swagger.annotations.ApiModelProperty;

public class RetrieveUnAckEchoCommand extends BaseEchoCommand {

  @ApiModelProperty(value = "消息集合", required = true)
  private List<BaseDispatchMessage> messages;

  public RetrieveUnAckEchoCommand() {
    super();
    this.type = EchoType.RetrieveUnAck;
  }

  public List<BaseDispatchMessage> getMessages() {
    return messages;
  }

  public void setMessages(List<BaseDispatchMessage> messages) {
    this.messages = messages;
  }
}
