package com.mimo.comet.provider.command.echo;

import java.util.Collection;
import java.util.Collections;

import io.swagger.annotations.ApiModelProperty;

public class ListRoomsEchoCommand extends BaseEchoCommand {

  /**
   * 已经加入的房间号
   */
  @ApiModelProperty(value = "已经加入的房间号", required = true)
  private Collection<String> rooms;

  public ListRoomsEchoCommand() {
    super();
    this.type = EchoType.ListRooms;
    this.rooms = Collections.emptySet();
  }

  public Collection<String> getRooms() {
    return rooms;
  }

  public void setRooms(Collection<String> rooms) {
    this.rooms = rooms;
  }

}
