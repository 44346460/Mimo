package com.mimo.comet.provider.plugin;

public interface IHeartBeatPlugIn {

  /**
   * 返回当前心跳机制的配置
   * 
   * @return
   */
  public HeartBeatStrategy getStrategy();

  /**
   * 回调通知
   * 
   * @param user
   * @param delegate
   *          底层代理的对象
   */
  public void onExpired(String user, Object delegate);

  /**
   * 由于用户会反复expire,如果在业务心跳允许范围内，则业务则应该对它进行重置，即保活
   * 
   * @param user
   */
  public void reset(String user);

}
