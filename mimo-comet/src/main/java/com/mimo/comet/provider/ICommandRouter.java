package com.mimo.comet.provider;

import com.mimo.comet.provider.command.BaseCommand;

/**
 * 统一客户端指令入口, 主要是通过一定的逻辑处理后，传递给下游的{@link ICommandProvider}具体业务处理器。
 * <p>
 * 路由时需要考虑以下维度:
 * <li>用户的接入类型，即CometType
 * <li>对应CometType的SPI的支持实现程度
 * 
 * @author Hongyu
 */
public interface ICommandRouter {
  /**
   * 根据请求用户以及指令做路由
   * 
   * @param userId
   *          发起人
   * @param cmd
   *          操作指令
   * @param 用于维护本机的session一致性
   * @return 返回值保留，用于业务上变动之预备
   */
  public Object route(String userId, BaseCommand cmd, Object delegate);
}
