package com.mimo.comet.provider.command;

/**
 * @author Hongyu etc-> {"id":"8f812a400ad94e509390a98c1252d249","type":"Logout"}
 */
public class LogoutCommand extends BaseCommand {

  public LogoutCommand() {
    super();
    this.type = CommandType.Logout;
  }

}
