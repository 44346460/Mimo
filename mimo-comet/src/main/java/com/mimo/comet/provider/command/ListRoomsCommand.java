package com.mimo.comet.provider.command;

/**
 * 考虑到该指令的返回值已经可以满足客户端用于查询自己是否存在于某个房间的场景。
 * <p>
 * 所以，统一由客户端获取自己所加入的所有房间后，做本地判断即可。
 * 
 * @author Hongyu
 */
public class ListRoomsCommand extends BaseCommand {
  public ListRoomsCommand() {
    super();
    this.type = CommandType.ListRooms;
  }
}
