package com.mimo.comet.provider.plugin;

import java.time.Duration;

/**
 * 一个高层抽象策略，用于定义心跳机制
 * 
 * @author Hongyu
 */
public class HeartBeatStrategy {
  /**
   * 如果发生了超时失败,则最多可以容忍失败的次数
   */
  private int loss;

  /**
   * 超时时长
   */
  private Duration timeout;

  public int getLoss() {
    return loss;
  }

  public void setLoss(int loss) {
    this.loss = loss;
  }

  public Duration getTimeout() {
    return timeout;
  }

  public void setTimeout(Duration timeout) {
    this.timeout = timeout;
  }

}