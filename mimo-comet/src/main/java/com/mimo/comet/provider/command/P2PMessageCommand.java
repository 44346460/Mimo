package com.mimo.comet.provider.command;

/**
 * {"id":"36e92766f4344377b504ebca36a00f28","type":"P2P","target":"hongyu","content":"点对点消息测试"}
 * 
 * @author Hongyu
 */
public class P2PMessageCommand extends AbstractMessageCommand {
  public P2PMessageCommand() {
    super();
    this.type = CommandType.P2P;
  }

}
