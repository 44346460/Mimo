package com.mimo.comet.event.listener;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import org.springframework.validation.annotation.Validated;

import com.mimo.common.comet.dto.ZoneDTO;

@Validated
public class SessionCreatedEvent {

  @NotEmpty
  private String applicationId;

  @NotEmpty
  private String userId;

  @NotNull
  @Valid
  private ZoneDTO zone;

  @NotEmpty
  private String deviceId;

  @NotNull
  private long timestamp;

  public SessionCreatedEvent() {
    super();
    this.timestamp = System.currentTimeMillis();
  }

  public SessionCreatedEvent(String applicationId, String userId, ZoneDTO zone, String deviceId) {
    this();
    this.applicationId = applicationId;
    this.userId = userId;
    this.zone = zone;
    this.deviceId = deviceId;
  }

  public String getDeviceId() {
    return deviceId;
  }

  public void setDeviceId(String deviceId) {
    this.deviceId = deviceId;
  }

  public long getTimestamp() {
    return timestamp;
  }

  public void setTimestamp(long timestamp) {
    this.timestamp = timestamp;
  }

  public ZoneDTO getZone() {
    return zone;
  }

  public void setZone(ZoneDTO zone) {
    this.zone = zone;
  }

  public String getUserId() {
    return userId;
  }

  public void setUserId(String userId) {
    this.userId = userId;
  }

  public String getApplicationId() {
    return applicationId;
  }

  public void setApplicationId(String applicationId) {
    this.applicationId = applicationId;
  }

}
