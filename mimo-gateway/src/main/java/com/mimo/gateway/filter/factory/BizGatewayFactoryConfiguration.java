package com.mimo.gateway.filter.factory;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration(proxyBeanMethods = false)
public class BizGatewayFactoryConfiguration {

  @Bean
  public RequestTimeGatewayFilterFactory requestTimeGatewayFilterFactory() {
    return new RequestTimeGatewayFilterFactory();
  }

}
