package com.mimo.gateway.config;

import javax.validation.constraints.NotEmpty;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.validation.annotation.Validated;

/**
 * 用于向ShowMe提供一套双端密钥,以便双端进行验签，规则如下：
 * <p>
 * sign= SHA1(secrect+nonce+timestamp)
 * 
 * @author Hongyu
 */
@Validated
@ConfigurationProperties(prefix = "mimo.access")
public class AccessConfig {
  /**
   * AppKey
   */
  @NotEmpty
  private String key;

  /**
   * App Secrect
   */
  @NotEmpty
  private String secrect;

  public String getKey() {
    return key;
  }

  public void setKey(String key) {
    this.key = key;
  }

  public String getSecrect() {
    return secrect;
  }

  public void setSecrect(String secrect) {
    this.secrect = secrect;
  }

}
