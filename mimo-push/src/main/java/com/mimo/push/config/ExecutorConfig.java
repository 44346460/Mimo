package com.mimo.push.config;

import java.time.Duration;
import java.util.concurrent.Executor;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.integration.util.CallerBlocksPolicy;

@Configuration
public class ExecutorConfig {
  private static final int CORE_THREAD = 200;
  private static final int MAX_THREAD = (int) (CORE_THREAD * 1.75);
  private static final int MAX_QUEUE = 2000;

  // 阻塞等待最长时间为10秒
  private static final Duration MAX_WAIT = Duration.ofSeconds(10);

  @Bean("cometMsgExecutor")
  public Executor taskExecutor() {
    return new ThreadPoolExecutor(CORE_THREAD, MAX_THREAD, 0L, TimeUnit.MILLISECONDS,
        new LinkedBlockingQueue<>(MAX_QUEUE), r -> new Thread(r, "comet-msg-executor-" + r.hashCode()),
        new CallerBlocksPolicy(MAX_WAIT.toMillis()));
  }
}
