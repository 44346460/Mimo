package com.mimo.push.binder;

import org.springframework.cloud.stream.annotation.Input;
import org.springframework.messaging.SubscribableChannel;

/**
 * 用于消费Logic模块端转储的投递消息
 * 
 * @author Hongyu
 */
public interface MessageBrokerProcessor {

  String INPUT = "MessageBrokerInput";

  @Input(INPUT)
  SubscribableChannel input();
}
