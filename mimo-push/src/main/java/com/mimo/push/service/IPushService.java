package com.mimo.push.service;

import com.mimo.common.logic.dto.msg.BaseDispatchMessage;

/**
 * 用于消费 IM Broker中的消息, 并投递到真正的client 连接中。这里对Comet模块有强依赖
 * 
 * @author Hongyu
 */
public interface IPushService {

  /**
   * 作为 Broker 消费的入口
   * 
   * @param msg
   */
  public void onMessage(BaseDispatchMessage msg);

  /**
   * 直接消息投递到客户端的接口
   * 
   * @param targetUserId
   *          投递目标对象
   * @param msg
   */
  public void send(String targetUserId, BaseDispatchMessage msg);

  /**
   * 对于推送消息失败时，定义一个异常回调
   * 
   * @param targetUserId
   * @param msg
   */
  public void onFailed(String targetUserId, BaseDispatchMessage msg, Throwable throwable);
}
