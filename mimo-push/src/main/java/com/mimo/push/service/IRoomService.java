package com.mimo.push.service;

public interface IRoomService {

  Boolean isMemberOf(String roomId, String userId);

  void leave(String roomId, String userId);
}
