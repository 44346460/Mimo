package com.mimo.push.service;

import javax.annotation.Nullable;

import com.mimo.common.comet.dto.ZoneDTO;

public interface IUserZoneService {

  @Nullable
  ZoneDTO getZoneByUser(String user);

  /**
   * 用于清空该用户的zone信息
   * 
   * @param user
   */
  void clear(String user);
}
