package com.mimo.push.handler;

import org.springframework.web.bind.annotation.ControllerAdvice;

import com.mimo.common.exception.ApplicationException;
import com.mimo.common.exception.handler.AbstractExceptionHandler;
import com.mimo.common.result.BaseResult;

/**
 * 考虑到本应用只是纯粹的后台服务，所有的返回结果都以JSON为主。 当本应用遇到 任何非预见性的错误时，统一返回如下JSON: { code : -1; msg : "系统繁忙" }
 */

@ControllerAdvice
public class GlobalExceptionHandler extends AbstractExceptionHandler {

  @Override
  protected BaseResult specificExceptionHandler(ApplicationException ae) {

    BaseResult ret = super.specificExceptionHandler(ae);

    return ret;
  }

}