package com.mimo.push.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.mimo.common.logic.dto.msg.BaseDispatchMessage;
import com.mimo.common.result.BaseResult;
import com.mimo.common.result.CommonDTOResult;
import com.mimo.push.service.IPushService;
import com.mimo.push.service.IUserZoneService;

import io.swagger.annotations.ApiOperation;

@Validated
@RestController
@RequestMapping("/push")
public class PushController {

  @Autowired
  private IPushService pushService;

  @Autowired
  private IUserZoneService userZoneService;

  /**
   * 这是一个调测接口,应该只用用于测试目的
   * 
   * @param msg
   * @return
   */
  @Deprecated
  @PostMapping(value = "/send")
  @ApiOperation(value = "向用户直接投递消息", notes = "该操作实时且同步,基于调试目的", response = String.class)
  public BaseResult send(@Valid @RequestBody BaseDispatchMessage msg) {
    pushService.send(msg.getTarget(), msg);
    return BaseResult.SUCCESS_RET;
  }

  /**
   * 这是一个调测接口,应该只用用于测试目的
   * 
   * @param userId
   * @return
   */
  @Deprecated
  @PostMapping(value = "/getZoneByUserId")
  public BaseResult getZoneByUserId(@RequestParam String userId) {
    return new CommonDTOResult<>(userZoneService.getZoneByUser(userId));
  }

}
