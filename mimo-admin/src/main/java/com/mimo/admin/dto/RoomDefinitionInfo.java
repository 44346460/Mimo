package com.mimo.admin.dto;

import java.io.Serializable;
import java.util.Date;

import org.springframework.validation.annotation.Validated;

import com.mimo.common.utils.JsonUtils;

import io.swagger.annotations.ApiModelProperty;

@Validated
public class RoomDefinitionInfo implements Serializable {

  // 最大的TTL
  public static final long THREE_DAYS = 60 * 60 * 24 * 3L;

  // 默认的TTL
  public static final long DEFAULT = -1L;

  @ApiModelProperty(value = "房间ID,调用方需要保证其唯一性", required = true)
  private String roomId;

  /**
   * 房间的拥有者，即管理者，一般是创建人，当然也可以由接口直接指定
   */
  @ApiModelProperty(value = "房间的拥有者，即管理者，一般是创建人，当然也可以由接口直接指定", required = true)
  private String owner;

  /**
   * 房间允许的人数上限,一旦创建, 该字段则不允许再变更.
   * <p>
   * 人数上限取值范围为[1_000, 10_000]
   */

  @ApiModelProperty(value = "房间允许的人数上限,一旦创建, 该字段则不允许再变更.人数上限取值范围为[1_000, 10_000]", required = true)
  private Integer capacity;
  /**
   * 一个房间最大生存时间（单位秒，默认值为-1，取值范围为[-1, 3天] ）
   * <li>自创建开始，一旦到达ttl的规定时限，则房间会强制销毁
   * <li>如果值为-1，则系统认定不受ttl时限，但会不定期检查房间人数, 当房间人数为0时，则强制销毁
   * <p>
   * 一旦创建, 该字段则不允许再变更
   */

  @ApiModelProperty(value = "一个房间最大生存时间（单位秒，默认值为-1，取值范围为[-1, 3天] ) ,且不允许为 0", required = true)
  private long ttl = DEFAULT;

  @ApiModelProperty(value = "房间的创建日期", hidden = true)
  private Date createdDate = new Date();

  public String getOwner() {
    return owner;
  }

  public void setOwner(String owner) {
    this.owner = owner;
  }

  public String getRoomId() {
    return roomId;
  }

  public void setRoomId(String roomId) {
    this.roomId = roomId;
  }

  public Integer getCapacity() {
    return capacity;
  }

  public void setCapacity(Integer capacity) {
    this.capacity = capacity;
  }

  public long getTtl() {
    return ttl;
  }

  public void setTtl(long ttl) {
    this.ttl = ttl;
  }

  public Date getCreatedDate() {
    return createdDate;
  }

  public void setCreatedDate(Date createdDate) {
    this.createdDate = createdDate;
  }

  @Override
  public String toString() {
    return JsonUtils.toJsonString(this);
  }

}
