package com.mimo.admin.dto;

import java.io.Serializable;
import java.util.Set;

import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiModelProperty.AccessMode;

/**
 * 用于描述一个房间当前的数据信息
 * <li>房间里的用户
 * <li>房间人数
 *
 * @author Hongyu
 */
public class RoomStatisticsDTO implements Serializable {
  private static final long serialVersionUID = 1L;

  @ApiModelProperty(value = "房间ID唯一性", required = true, accessMode = AccessMode.READ_ONLY)
  private String id;

  @ApiModelProperty(value = "创建房间时的配置信息", required = true, accessMode = AccessMode.READ_ONLY)
  private RoomDefinitionInfo definition;

  @ApiModelProperty(value = "存放着加入该房间的用户ID", required = true, accessMode = AccessMode.READ_ONLY)
  private Set<String> users;

  @ApiModelProperty(value = "当前房间人数", required = true, accessMode = AccessMode.READ_ONLY)
  private int size;

  public RoomDefinitionInfo getDefinition() {
    return definition;
  }

  public void setDefinition(RoomDefinitionInfo definition) {
    this.definition = definition;
  }

  public Set<String> getUsers() {
    return users;
  }

  public void setUsers(Set<String> users) {
    this.users = users;
  }

  public int getSize() {
    return size;
  }

  public void setSize(int size) {
    this.size = size;
  }

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

}
