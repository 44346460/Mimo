package com.mimo.admin.config;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import org.springframework.format.Formatter;
import org.springframework.stereotype.Component;

@Component
public class DateFormatter implements Formatter<Date> {

  private static ThreadLocal<SimpleDateFormat> threadLocal = new ThreadLocal<SimpleDateFormat>() {
    @Override
    protected SimpleDateFormat initialValue() {
      return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    }
  };

  @Override
  public Date parse(String text, Locale locale) throws ParseException {
    return threadLocal.get().parse(text);
  }

  @Override
  public String print(Date date, Locale locale) {
    return threadLocal.get().format(date);
  }
}
