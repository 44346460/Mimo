package com.mimo.admin.config;

import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.validation.constraints.NotEmpty;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.validation.annotation.Validated;

@Validated
@ConfigurationProperties(prefix = "mimo.admin")
public class AdminServerConfig {

  //默认超时时间30分钟
  private long sessionTimeout = TimeUnit.MINUTES.toSeconds(30);

  @NotEmpty
  private List<String> domains;

  //是否开启csrf拦截
  private boolean csrfEnable = Boolean.TRUE;

  //是否开启开发者模式检测
  private boolean devtoolsDetectEnable = Boolean.TRUE;

  public boolean isDevtoolsDetectEnable() {
    return devtoolsDetectEnable;
  }

  public void setDevtoolsDetectEnable(boolean devtoolsDetectEnable) {
    this.devtoolsDetectEnable = devtoolsDetectEnable;
  }

  public long getSessionTimeout() {
    return sessionTimeout;
  }

  public void setSessionTimeout(long sessionTimeout) {
    this.sessionTimeout = sessionTimeout;
  }

  public List<String> getDomains() {
    return domains;
  }

  public void setDomains(List<String> domains) {
    this.domains = domains;
  }

  public boolean isCsrfEnable() {
    return csrfEnable;
  }

  public void setCsrfEnable(boolean csrfEnable) {
    this.csrfEnable = csrfEnable;
  }
}
