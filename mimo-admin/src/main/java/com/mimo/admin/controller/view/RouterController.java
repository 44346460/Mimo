package com.mimo.admin.controller.view;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * 
 */
@Api(value = "点击菜单跳转")
@Controller
@RequestMapping("/router")
public class RouterController {

  @GetMapping(value = "/monitor/user")
  @ApiOperation(value = "数据监控-用户数据")
  public ModelAndView monitorUser() {

    ModelAndView mav = new ModelAndView("monitor/user");
    handleDate(mav);
    return mav;
  }

  @GetMapping(value = "/monitor/room")
  @ApiOperation(value = "数据监控-房间数据")
  public ModelAndView monitorRoom() {

    ModelAndView mav = new ModelAndView("monitor/room");
    handleDate(mav);
    return mav;
  }

  @GetMapping(value = "/monitor/message")
  @ApiOperation(value = "数据监控-消息数据")
  public ModelAndView monitorMessage() {
    
    ModelAndView mav = new ModelAndView("monitor/message");
    handleDate(mav);
    return mav;
  }

  @GetMapping(value = "/roomManage/roomList")
  @ApiOperation(value = "房间管理-房间列表")
  public String roomManageList() {
    return "room/roomList";
  }

  @GetMapping(value = "/userManage/userList")
  @ApiOperation(value = "用户管理-用户列表")
  public ModelAndView userManageList() {

    ModelAndView mav = new ModelAndView("user/userList");

    Date now = new Date();
    SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd 00:00:00");
    SimpleDateFormat sf2 = new SimpleDateFormat("yyyy-MM-dd 23:59:59");

    mav.addObject("loginBegin", sf.format(now));
    mav.addObject("loginEnd", sf2.format(now));
    return mav;
  }

  /**
   * 用于携带当前时间和七天前的时间返回给前端接收
   *
   * @param mav
   */
  public void handleDate(ModelAndView mav) {

    Date end = new Date();
    SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd 00:00:00");
    SimpleDateFormat sf2 = new SimpleDateFormat("yyyy-MM-dd 23:59:59");
    Date start = new Date(end.getTime() - 24 * 3600 * 1000 * 7);
    mav.addObject("start", sf.format(start));
    mav.addObject("end", sf2.format(end));

  }
}
