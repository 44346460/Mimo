package com.mimo.admin.controller.room;

import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.mimo.admin.dto.RoomAdminDTO;
import com.mimo.admin.dto.RoomStatisticsDTO;
import com.mimo.admin.proxy.MetricProxy;
import com.mimo.common.dto.Entry;
import com.mimo.common.logic.room.dto.RoomMemberQueryCriteria;
import com.mimo.common.result.CollectionResult;

import io.swagger.annotations.ApiOperation;

/**
 * 
 * @date 2021/1/26 17:49
 * @description
 **/
@RestController
@RequestMapping("/room")
public class RoomController {

  @Autowired
  private MetricProxy metricProxy;

  @PostMapping("/list")
  @ApiOperation(value = "房间列表", notes = "房间列表")
  public CollectionResult<RoomAdminDTO> roomList(@RequestParam int page, @RequestParam int size, @RequestParam(required = false) String roomId,
      @RequestParam(required = false) String userId) {

    CollectionResult<RoomStatisticsDTO> result = metricProxy.queryRoom();
    Collection<RoomStatisticsDTO> items = result.getItems();

    if (StringUtils.hasText(userId)) {
      items = metricProxy.listJointRooms(userId).getItems();
    }

    if (StringUtils.hasText(roomId)) {
      items = items.stream().filter(e -> roomId.equals(e.getDefinition().getRoomId())).collect(Collectors.toList());
    }

    PageRequest pr = PageRequest.of(page - 1, size);
    List<RoomAdminDTO> list = items.stream()
        .map(info -> {
          RoomAdminDTO dto = new RoomAdminDTO();
          dto.setRoomId(info.getDefinition().getRoomId());
          dto.setCapacity(info.getDefinition().getCapacity());
          dto.setOwner(info.getDefinition().getOwner());
          dto.setTtl(info.getDefinition().getTtl());
          dto.setSize(info.getSize());
          dto.setCreatedDate(info.getDefinition().getCreatedDate());
          return dto;
        })
        .skip(pr.getOffset()) //分页
        .limit(size)
        .collect(Collectors.toList());

    return new CollectionResult<>(list, (long) items.size());
  }

  @PostMapping("/members")
  @ApiOperation(value = "房间成员", notes = "房间成员")
  public CollectionResult<Entry<String, Date>> members(@RequestBody RoomMemberQueryCriteria criteria) {

    criteria.setPage(criteria.getPage() - 1);
    CollectionResult<Entry<String, Double>> result = metricProxy.queryRoomMembers(criteria);

    List<Entry<String, Date>> list = result.getItems().stream()
        .map(en -> new Entry<>(en.getKey(), new Date(en.getValue().longValue())))
        .collect(Collectors.toList());

    return new CollectionResult<>(list, result.getTotal());
  }
}
