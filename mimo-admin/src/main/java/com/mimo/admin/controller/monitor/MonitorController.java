package com.mimo.admin.controller.monitor;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.mimo.admin.constants.MetricType;
import com.mimo.admin.proxy.MetricProxy;
import com.mimo.common.dto.Entry;
import com.mimo.common.result.CollectionResult;
import com.mimo.common.result.CommonDTOResult;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

/**
 * 
 * @date 2021/1/26 16:08
 * @description 数据监控
 **/
@RestController
@RequestMapping("/monitor")
public class MonitorController {

  @Autowired
  private MetricProxy metricProxy;

  @PostMapping(value = "/query")
  @ApiOperation(value = "基于自然日格式定位统计维度的数值")
  public CommonDTOResult<Entry<List<String>, List<Long>>> query(@RequestParam MetricType metricType,
      @ApiParam(required = true, name = "startLeft", value = "基于yyyy-MM-dd HH:mm:ss的格式定位自然日,起始时间") @RequestParam Date startLeft,
      @ApiParam(required = true, name = "startRight", value = "基于yyyy-MM-dd HH:mm:ss的格式定位自然日,结束时间") @RequestParam Date startRight) {

    CollectionResult<Entry<String, Long>> result = metricProxy.query(metricType, startLeft, startRight);

    List<String> dateList = new ArrayList<>();
    List<Long> numList = new ArrayList<>();

    result.getItems().forEach(info -> {
      dateList.add(info.getKey());
      numList.add(info.getValue());
    });
    Entry<List<String>, List<Long>> entry = new Entry<>();
    entry.setKey(dateList);
    entry.setValue(numList);

    return new CommonDTOResult<>(entry);
  }

}
