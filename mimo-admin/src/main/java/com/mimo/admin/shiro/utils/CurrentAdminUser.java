package com.mimo.admin.shiro.utils;

import java.util.Objects;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.session.Session;

/**
 * 登录用户信息
 *
 * 
 */
public class CurrentAdminUser {

  private CurrentAdminUser() {
    throw new UnsupportedOperationException();
  }


  /**
   * 获取当前登录账号对象
   *
   * @return 当前登录用户名
   */
  public static String getUserCode() {
    Session session = SecurityUtils.getSubject().getSession(false);
    if (Objects.isNull(session)) {
      return null;
    }
    return (String) session.getAttribute("userCode");


  }


}
