package com.mimo.admin.shiro.config;

import java.util.List;
import java.util.Set;

import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.SimpleAuthenticationInfo;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;

/**
 * 权限验证以及授权
 *
 * 
 */

public class ShiroRealm extends AuthorizingRealm {
  private static final Logger logger = LoggerFactory.getLogger(ShiroRealm.class);


  @Value("#{'${mimo.admin.urlPermission}'.split(',')}")
  private List<String> urlPermission;

  @Value("#{'${mimo.admin.routerPermission}'.split(',')}")
  private List<String> routerPermission;

  @Value("#{'${mimo.admin.roles}'.split(',')}")
  private Set<String> roles;

  /**
   * 登录认证
   */
  @Override

  protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken authenticationToken) {
    // UsernamePasswordToken对象用来存放提交的登录信息
    UsernamePasswordToken token = (UsernamePasswordToken) authenticationToken;

    return new SimpleAuthenticationInfo(token.getUsername(), // 用户
        String.valueOf(token.getPassword()), // 密码
        getName()// realm name
    );

  }

  /**
   * 权限认证，为当前登录的Subject授予角色和权限 经测试：本例中该方法的调用时机为需授权资源被访问时 经测试：并且每次访问需授权资源时都会执行该方法中的逻辑，这表明本例中默认并未启用AuthorizationCache 经测试
   * ：如果连续访问同一个URL（比如刷新），该方法不会被重复调用，Shiro有一个时间间隔（也就是cache时间，在ehcache-shiro.xml中配置），超过这个时间间隔再刷新页面 ，该方法会被执行
   */
  @Override
  protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principalCollection) {
    logger.info("##################执行Shiro权限认证##################");

    // 权限信息对象info,用来存放查出的用户的所有的角色（role）及权限（permission）
    SimpleAuthorizationInfo info = new SimpleAuthorizationInfo();
    info.setRoles(roles);
    // 用户的权限集合
    info.addStringPermissions(urlPermission);
    info.addStringPermissions(routerPermission);
    // 用户角色
    return info;
  }
}
