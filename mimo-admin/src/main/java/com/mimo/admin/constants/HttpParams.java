package com.mimo.admin.constants;

public class HttpParams {
  //请求头key
  public static final String SESSIONS_TATUS = "sessionStatus";
  public static final String REFERER = "Referer";
  public static final String X_REQUESTED_WITH = "X-Requested-With";

  //一般为请求头自定义值
  public static final String UNAUTHORIZED = "unauthorized";
  public static final String SESSIONS_TIMEOUT = "sessionTimeout";
  public static final String XML_HTTP_REQUEST = "XMLHttpRequest";

}
