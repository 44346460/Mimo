package com.mimo.admin.exception.result;

import com.mimo.common.result.BaseResult;

public class MismatchPasswordResult extends BaseResult {

  public MismatchPasswordResult() {
    code = 4009;
    msg = "账户或密码错误";
  }

  @Override
  public String toString() {
    return "MismatchPasswordResult [code=" + code + ", msg=" + msg + "]";
  }
}
