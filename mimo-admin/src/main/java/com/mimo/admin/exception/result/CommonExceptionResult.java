package com.mimo.admin.exception.result;

import com.mimo.common.result.BaseResult;

/**
 * Created by yangjian
 */
public class CommonExceptionResult extends BaseResult {

  /**
   * @param messageKey
   * @param args
   */
  public CommonExceptionResult(String messageKey, Object... args) {
    this.code = SYSTEM_ERROR_CODE;
    this.msg = messageKey;
  }
}
