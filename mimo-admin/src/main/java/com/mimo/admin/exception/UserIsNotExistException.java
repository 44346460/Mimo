package com.mimo.admin.exception;

import com.mimo.common.exception.ApplicationException;


public class UserIsNotExistException extends ApplicationException {

  /**
   * 
   */
  private static final long serialVersionUID = 1L;
}
