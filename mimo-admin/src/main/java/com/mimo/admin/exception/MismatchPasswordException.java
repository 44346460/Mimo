package com.mimo.admin.exception;

import com.mimo.common.exception.ApplicationException;


public class MismatchPasswordException extends ApplicationException {

  private static final long serialVersionUID = 2761229821515051899L;

}
