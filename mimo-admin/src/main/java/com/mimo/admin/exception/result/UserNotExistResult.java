package com.mimo.admin.exception.result;

import com.mimo.common.result.BaseResult;

public class UserNotExistResult extends BaseResult {

  public UserNotExistResult() {
    code = 11007;
    msg = "用户不存在";
  }
}
