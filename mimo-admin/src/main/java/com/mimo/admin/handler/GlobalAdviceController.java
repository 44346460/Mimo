package com.mimo.admin.handler;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.StringTrimmerEditor;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.servlet.resource.ResourceUrlProvider;

@ControllerAdvice
public class GlobalAdviceController {

  @Autowired
  private ResourceUrlProvider resourceUrlProvider;

  @InitBinder /* Converts empty strings into null when a form is submitted */
  public void initBinder(WebDataBinder binder) {
    binder.registerCustomEditor(String.class, new StringTrimmerEditor(true));
  }

  @ModelAttribute("urls")
  public ResourceUrlProvider urls() {
    return this.resourceUrlProvider;
  }
  //
  // @ModelAttribute
  // public void setVaryResponseHeader(HttpServletResponse response) {
  // response.setHeader("Pragma", "No-cache");
  // response.setHeader("Cache-Control", "no-store");
  // }

}
