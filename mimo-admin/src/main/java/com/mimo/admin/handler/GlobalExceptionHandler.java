package com.mimo.admin.handler;

import org.springframework.web.bind.annotation.ControllerAdvice;

import com.mimo.admin.exception.MismatchPasswordException;
import com.mimo.admin.exception.UserIsNotExistException;
import com.mimo.admin.exception.result.CommonExceptionResult;
import com.mimo.admin.exception.result.MismatchPasswordResult;
import com.mimo.admin.exception.result.UserNotExistResult;
import com.mimo.common.exception.ApplicationException;
import com.mimo.common.exception.CommonMessageException;
import com.mimo.common.exception.handler.AbstractExceptionHandler;
import com.mimo.common.result.BaseResult;

/**
 * 
 */
@ControllerAdvice
public class GlobalExceptionHandler extends AbstractExceptionHandler {

  @Override
  protected BaseResult specificExceptionHandler(ApplicationException ae) {

    BaseResult ret = super.specificExceptionHandler(ae);

    if (ae instanceof MismatchPasswordException) {
      ret = new MismatchPasswordResult();
    } else if (ae instanceof UserIsNotExistException) {
      ret = new UserNotExistResult();
    } else if (ae instanceof CommonMessageException) {
      Object[] data = Object[].class.cast(ae.getData());
      ret = new CommonExceptionResult((String) data[0], (Object[]) data[1]);
    }
    return ret;
  }

}
