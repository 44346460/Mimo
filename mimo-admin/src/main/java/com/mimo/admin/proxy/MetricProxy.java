package com.mimo.admin.proxy;

import java.util.Date;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import com.mimo.admin.constants.MetricType;
import com.mimo.admin.dto.RoomStatisticsDTO;
import com.mimo.common.dto.Entry;
import com.mimo.common.logic.room.dto.RoomMemberQueryCriteria;
import com.mimo.common.logic.user.dto.UserDTO;
import com.mimo.common.logic.user.dto.UserQueryCriteria;
import com.mimo.common.result.CollectionResult;
import com.mimo.common.result.CommonDTOResult;

@FeignClient("logic-service")
public interface MetricProxy {

  @PostMapping(value = "/metric/query")
  CollectionResult<Entry<String, Long>> query(@RequestParam MetricType type, @RequestParam Date from, @RequestParam Date to);

  @PostMapping(value = "/user/admin/query")
  CollectionResult<UserDTO> queryUser(@RequestBody UserQueryCriteria criteria);

  @GetMapping(value = "/user/admin/load")
  CommonDTOResult<UserDTO> loadUser(@RequestParam String userId);

  @PostMapping(value = "/room/admin/list")
  CollectionResult<RoomStatisticsDTO> queryRoom();

  @PostMapping("/room/admin/members")
  CollectionResult<Entry<String, Double>> queryRoomMembers(@RequestBody RoomMemberQueryCriteria query);

  @PostMapping("/room/admin/listJointRooms")
  CollectionResult<RoomStatisticsDTO> listJointRooms(@RequestParam String userId);

}
