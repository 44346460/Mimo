layui.use(['table', 'jquery', 'form', 'mylayer', 'laydate', 'mytable'], function () {

    var $ = layui.$;
    var table = layui.table;
    var mytable = layui.mytable;


    mytable.initTable('listTableRoom', {
        url: '/room/list'
        , method: 'post'
        , id: 'listTableRoom'
        , formId: 'queryFormRoom'
        , initForm: false
        , done: function (res, curr, count) {
            $("#total").text(count);
        }
    });


    table.on('tool(listTableRoom)', function (obj) {
        var data = obj.data;

        layer.open({
            type: 1
            , title: "房间成员"
            , area: '900px'
            , content: $('#userModel')
            , btn: ["关闭"]
            , offset: '10px'
            , success: function (layero, index) {
                $("#roomId").val(data.roomId)
                $("#spanRoomId").text(data.roomId);
                mytable.initTable('listTableUser', {
                    url: '/room/members'
                    , id: 'listTableUser'
                    , method: 'post'
                    , contentType: 'application/json'
                    , initForm: false
                    , formId: "userForm"
                    , height: 480
                });
            },
            end: function () {
                $('#userForm').clearData();
            }
        });
    });

    $('#query').click(function () {
        mytable.reloadTable(table, "listTableRoom", {formId: "queryFormRoom", id: 'listTableRoom'});
    });
    $('#queryUser').click(function () {
        mytable.reloadTable(table, "listTableUser", {formId: "userForm", id: 'listTableUser'});
    });

})
