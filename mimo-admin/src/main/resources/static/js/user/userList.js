layui.use(['table', 'jquery', 'form', 'mylayer', 'laydate', 'mytable'], function () {

    var form = layui.form;
    var $ = layui.$;
    var laydate = layui.laydate;
    var mylayer = layui.mylayer;
    var table = layui.table;
    var mytable = layui.mytable;
    var myform = layui.myform;


    const begin = $("#loginBegin").val();
    const end = $("#loginEnd").val();

    mytable.initTable('listTable', {
        url: '/user/list'
        , method: 'post'
        , contentType: 'application/json'
        , done: function (res, curr, count) {
            $("#total").text(count);
        }
    });

    table.on('tool(listTable)', function (obj) {
        var data = obj.data;

        layer.open({
            type: 1
            , title: "用户详细"
            , area: '850px'
            , content: $('#detailModel')
            , btn: ["关闭"]
            , success: function (layero, index) {

                var url = "/user/detail"
                var param = {userId: data.uid}

                $.post(url, param, function (result) {
                    if (!result.success) {
                        mylayer.failMsg(result.msg);
                        return;
                    }
                    //区域信息
                    result.detail.zoneName = result.detail.zone.name
                    result.detail.zoneIp = result.detail.zone.ip
                    result.detail.zonePort = result.detail.zone.port

                    //设备信息
                    result.detail.deviceTerminal = result.detail.device.terminal
                    result.detail.os = result.detail.device.os
                    result.detail.brand = result.detail.device.brand
                    result.detail.model = result.detail.device.model
                    result.detail.appVersion = result.detail.device.appVersion
                    
                    if (result.detail.online == true) {
                        result.detail.online = "是"
                    } else {
                        result.detail.online = "否"
                    }
                    console.log(result.detail)
                    myform.val("detailForm", result.detail);
                })
            },
            end: function () {
                $('#detailForm').clearData();
            }
        });

    });

    laydate.render({
        elem: '#createdDate',
        type: 'datetime',
        range: $.dateSeparater,
        done: function (value, date, endDate) {
            if (!value) {
                $("#begin,#end").val('');
                return;
            }
            $("#begin").val(value.split($.dateSeparater)[0].trim());
            $("#end").val(value.split($.dateSeparater)[1].trim());

        },

    });

    laydate.render({
        elem: '#loginDate',
        type: 'datetime',
        range: $.dateSeparater,
        value: begin + ' ~ ' + end,
        done: function (value, date, endDate) {
            if (!value) {
                $("#loginBegin,#loginEnd").val('');
                return;
            }
            $("#loginBegin").val(value.split($.dateSeparater)[0].trim());
            $("#loginEnd").val(value.split($.dateSeparater)[1].trim());

        },

    });


    function reloadTable() {
        mytable.reloadTable(table);
    }

    $('#query').click(reloadTable);

})
