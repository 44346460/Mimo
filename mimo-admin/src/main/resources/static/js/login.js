'use strict';

layui.use(['jquery', 'form', 'myform', 'mylayer'], function () {

    var form = layui.form;
    var myform = layui.myform;
    var $ = layui.$;
    var mylayer = layui.mylayer;

    form.on('submit(login)', function (data) {
        if (!myform.verifyForm('loginForm')) return;

        var url = '/login';
        var param = $('#loginForm').serializeJSON();

        param.password = $("#password").val()

        $.post(url, param, function (result) {
            if (!result.success) {
                mylayer.failMsg(result.msg);
                return;
            }

            location.href = '/main';
            mylayer.close(index);
            $('#loginForm').clearData();
        });
        return false;
    });


})