layui.use(['table', 'jquery', 'laydate', 'mytable', 'mylayer', 'element', 'laydatePro'], function () {
    var table = layui.table;
    var $ = layui.$;
    var laydate = layui.laydate;
    var mytable = layui.mytable;
    var mylayer = layui.mylayer;
    var element = layui.element;
    var myform = layui.myform;
    var laydatePro = layui.laydatePro;


    const queryForm = {
        queryFormNewUser: 'queryFormNewUser',
        queryFormLoginUser: 'queryFormLoginUser',
        queryFormAccessUser: 'queryFormAccessUser'
    };

    const tabFilter = {
        newUser: 'newUser',
        loginUser: 'loginUser',
        accessUser: 'accessUser'
    }
    const metricType = {
        User_Create: "User_Create",
        User_Login: "User_Login",
        User_Active: "User_Active",
    }

    var begin = $("#startLeftNewUser").val();
    var end = $("#startRightNewUser").val();

    initDate();

    load(metricType.User_Create, queryForm.queryFormNewUser, tabFilter.newUser);
    load(metricType.User_Login, queryForm.queryFormLoginUser, tabFilter.loginUser);
    load(metricType.User_Active, queryForm.queryFormAccessUser, tabFilter.accessUser);
    

    function load(metricType, queryForm, tabFilter) {

        var url = "/monitor/query"
        var param = $('#' + queryForm).serializeJSON();
        param.metricType = metricType;

        $.post(url, param, function (result) {
            if (!result.success) {
                mylayer.failMsg(result.msg);
                return;
            }

            var dateArr = result.detail.key;
            var numArr = result.detail.value;
            initEchart(tabFilter, dateArr, numArr)
        });
    }

    function resizeCharts(myChart) {
        $(window).resize(function () {
            myChart.resize();
        });
    }

    function initEchart(tabFilter, xAxis, series) {

        // 基于准备好的dom，初始化echarts实例
        var myEchart = echarts.init(document.getElementById(tabFilter));

        // 指定图表的配置项和数据
        var option = {
            xAxis: {
                type: 'category',
                data: xAxis
            },
            yAxis: {
                type: 'value'
            },
            series: [{
                data: series,
                type: 'line',
                smooth: true

            }],
            tooltip: {
                trigger: 'axis',
                axisPointer: {
                    type: 'cross',
                    label: {
                        backgroundColor: '#6a7985'
                    }
                }
            }
        };
        // 使用刚指定的配置项和数据显示图表。
        myEchart.setOption(option);
        resizeCharts(myEchart)
    }

    laydate.render({
        elem: '#newUserCreateDate',
        type: 'date', // 新增两个类型的支持 range的时候的date和datetime
        range: $.dateSeparater,
        format: "yyyy-MM-dd HH:mm:ss",
        value: begin + " ~ " + end,
        quickConfirm: true, // 是否选择之后快速确定
        quickSelect: [
            'lastMonth', // 上个月
            'thisMonth', // 这个月
            {
                title: '最近7天',
                value: function () {
                    var date1 = new Date();
                    var date2 = new Date();
                    date1.setDate(date1.getDate() - 7);
                    date1.setHours(0, 0, 0, 0);
                    date2.setHours(23, 59, 59, 0);
                    return [date1, date2];
                }()
            },
            {
                title: '最近30天',
                value: function () {
                    var date1 = new Date();
                    var date2 = new Date();
                    date1.setDate(date1.getDate() - 30);
                    date1.setHours(0, 0, 0, 0);
                    date2.setHours(23, 59, 59, 0);
                    return [date1, date2];
                }()
            },
            {
                title: '最近90天',
                value: function () {
                    var date1 = new Date();
                    var date2 = new Date();
                    date1.setDate(date1.getDate() - 90);
                    date1.setHours(0, 0, 0, 0);
                    date2.setHours(23, 59, 59, 0);
                    return [date1, date2];
                }()
            }

        ],
        done: function (value, date) {

            if (!value) {
                $("#startLeftNewUser,#startRightNewUser").val('');
                return;
            }
            $("#startLeftNewUser").val(value.split($.dateSeparater)[0].trim());
            $("#startRightNewUser").val(value.split($.dateSeparater)[1].trim());

            load(metricType.User_Create, queryForm.queryFormNewUser, tabFilter.newUser)
        }
    });

    laydate.render({
        elem: '#loginUserCreateDate',
        type: 'date', // 新增两个类型的支持 range的时候的date和datetime
        range: $.dateSeparater,
        format: "yyyy-MM-dd HH:mm:ss",
        value: begin + " ~ " + end,
        quickConfirm: true, // 是否选择之后快速确定
        quickSelect: [
            'lastMonth', // 上个月
            'thisMonth', // 这个月
            {
                title: '最近7天',
                value: function () {
                    var date1 = new Date();
                    var date2 = new Date();
                    date1.setDate(date1.getDate() - 7);
                    date1.setHours(0, 0, 0, 0);
                    date2.setHours(23, 59, 59, 0);
                    return [date1, date2];
                }()
            },
            {
                title: '最近30天',
                value: function () {
                    var date1 = new Date();
                    var date2 = new Date();
                    date1.setDate(date1.getDate() - 30);
                    date1.setHours(0, 0, 0, 0);
                    date2.setHours(23, 59, 59, 0);
                    return [date1, date2];
                }()
            },
            {
                title: '最近90天',
                value: function () {
                    var date1 = new Date();
                    var date2 = new Date();
                    date1.setDate(date1.getDate() - 90);
                    date1.setHours(0, 0, 0, 0);
                    date2.setHours(23, 59, 59, 0);
                    return [date1, date2];
                }()
            }

        ],
        done: function (value, date) {

            if (!value) {
                $("#startLeftLoginUser,#startRightLoginUser").val('');
                return;
            }
            $("#startLeftLoginUser").val(value.split($.dateSeparater)[0].trim());
            $("#startRightLoginUser").val(value.split($.dateSeparater)[1].trim());

            load(metricType.User_Login, queryForm.queryFormLoginUser, tabFilter.loginUser)
        }
    });

    laydate.render({
        elem: '#accessUserCreateDate',
        type: 'date', // 新增两个类型的支持 range的时候的date和datetime
        range: $.dateSeparater,
        format: "yyyy-MM-dd HH:mm:ss",
        value: begin + " ~ " + end,
        quickConfirm: true, // 是否选择之后快速确定
        quickSelect: [
            'lastMonth', // 上个月
            'thisMonth', // 这个月
            {
                title: '最近7天',
                value: function () {
                    var date1 = new Date();
                    var date2 = new Date();
                    date1.setDate(date1.getDate() - 7);
                    date1.setHours(0, 0, 0, 0);
                    date2.setHours(23, 59, 59, 0);
                    return [date1, date2];
                }()
            },
            {
                title: '最近30天',
                value: function () {
                    var date1 = new Date();
                    var date2 = new Date();
                    date1.setDate(date1.getDate() - 30);
                    date1.setHours(0, 0, 0, 0);
                    date2.setHours(23, 59, 59, 0);
                    return [date1, date2];
                }()
            },
            {
                title: '最近90天',
                value: function () {
                    var date1 = new Date();
                    var date2 = new Date();
                    date1.setDate(date1.getDate() - 90);
                    date1.setHours(0, 0, 0, 0);
                    date2.setHours(23, 59, 59, 0);
                    return [date1, date2];
                }()
            }

        ],
        done: function (value, date) {

            if (!value) {
                $("#startLeftAccessUser,#startRightAccessUser").val('');
                return;
            }
            $("#startLeftAccessUser").val(value.split($.dateSeparater)[0].trim());
            $("#startRightAccessUser").val(value.split($.dateSeparater)[1].trim());

            load(metricType.User_Active, queryForm.queryFormAccessUser, tabFilter.accessUser)
        }
    });


    function initDate() {
        $("#startLeftNewUser").val(begin);
        $("#startRightNewUser").val(end);
        $("#startLeftLoginUser").val(begin);
        $("#startRightLoginUser").val(end);
        $("#startLeftAccessUser").val(begin);
        $("#startRightAccessUser").val(end);
    }

})