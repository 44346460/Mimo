layui.use(['table', 'jquery', 'laydate', 'mytable', 'mylayer', 'element', 'laydatePro'], function () {
    var table = layui.table;
    var $ = layui.$;
    var laydate = layui.laydate;
    var mytable = layui.mytable;
    var mylayer = layui.mylayer;
    var myform = layui.myform;
    var laydatePro = layui.laydatePro;


    const queryForm = {
        queryFormP2P: 'queryFormP2P',
        queryFormRoom: 'queryFormRoom'
    };

    const tabFilter = {
        P2PMessage: 'P2PMessage',
        roomMessage: 'roomMessage'
    }

    const MetricType = {
        Msg_P2P_Upstream: "Msg_P2P_Upstream",
        Msg_P2P_Downstream: "Msg_P2P_Downstream",
        Msg_Room_Upstream: "Msg_Room_Upstream",
        Msg_Room_Downstream: "Msg_Room_Downstream",
    }

    var begin = $("#startLeftRoom").val();
    var end = $("#startRightRoom").val();
    initDate();


    load(MetricType.Msg_P2P_Upstream, MetricType.Msg_P2P_Downstream, queryForm.queryFormP2P, tabFilter.P2PMessage)
    load(MetricType.Msg_Room_Upstream, MetricType.Msg_Room_Downstream, queryForm.queryFormRoom, tabFilter.roomMessage)


    function load(metricTypeUp, metricTypeDown, queryForm, tabFilter) {

        var url = "/monitor/query"
        var param = $('#' + queryForm).serializeJSON();
        param.metricType = metricTypeUp

        //获取上行数据
        $.post(url, param, function (result) {
            if (!result.success) {
                mylayer.failMsg(result.msg);
                return;
            }
            var dateArr = [];
            var upArr = [];
            dateArr = result.detail.key;
            upArr = result.detail.value;

            //获取下行数据
            param.metricType = metricTypeDown
            $.post(url, param, function (result) {
                var downArr = [];

                if (!result.success) {
                    mylayer.failMsg(result.msg);
                    return;
                }
                downArr = result.detail.value;
                initEchart(tabFilter, dateArr, upArr, downArr)
            });

        });
    }

    function resizeCharts(myChart) {
        $(window).resize(function () {
            myChart.resize();
        });
    }

    function initEchart(tabFilter, xAxis, seriesUp, seriesDown) {

        // 基于准备好的dom，初始化echarts实例
        var myEchart = echarts.init(document.getElementById(tabFilter));
        
        var option = {
            tooltip: {
                trigger: 'axis'
            },
            legend: {
                data: ['上行消息数', '下行消息数']
            },
            grid: {
                left: '3%',
                right: '4%',
                bottom: '3%',
                containLabel: true
            },
            xAxis: {
                type: 'category',
                data: xAxis
            },
            yAxis: {
                type: 'value'
            },
            series: [
                {
                    name: '上行消息数',
                    type: 'line',
                    data: seriesUp
                },
                {
                    name: '下行消息数',
                    type: 'line',
                    data: seriesDown
                }
            ]
        };

        // 使用刚指定的配置项和数据显示图表。
        myEchart.setOption(option);
        resizeCharts(myEchart)
    }


    laydate.render({
        elem: '#P2PCreateDate',
        type: 'date', // 新增两个类型的支持 range的时候的date和datetime
        range: $.dateSeparater,
        format: "yyyy-MM-dd HH:mm:ss",
        value: begin + " ~ " + end,
        quickConfirm: true, // 是否选择之后快速确定
        quickSelect: [
            'lastMonth', // 上个月
            'thisMonth', // 这个月
            {
                title: '最近7天',
                value: function () {
                    var date1 = new Date();
                    var date2 = new Date();
                    date1.setDate(date1.getDate() - 7);
                    date1.setHours(0, 0, 0, 0);
                    date2.setHours(23, 59, 59, 0);
                    return [date1, date2];
                }()
            },
            {
                title: '最近30天',
                value: function () {
                    var date1 = new Date();
                    var date2 = new Date();
                    date1.setDate(date1.getDate() - 30);
                    date1.setHours(0, 0, 0, 0);
                    date2.setHours(23, 59, 59, 0);
                    return [date1, date2];
                }()
            },
            {
                title: '最近90天',
                value: function () {
                    var date1 = new Date();
                    var date2 = new Date();
                    date1.setDate(date1.getDate() - 90);
                    date1.setHours(0, 0, 0, 0);
                    date2.setHours(23, 59, 59, 0);
                    return [date1, date2];
                }()
            }

        ],
        done: function (value, date) {
            if (!value) {
                $("#startLeftP2P,#startRightP2P").val('');
                return;
            }
            $("#startLeftP2P").val(value.split($.dateSeparater)[0].trim());
            $("#startRightP2P").val(value.split($.dateSeparater)[1].trim());
            load(MetricType.Msg_P2P_Upstream, MetricType.Msg_P2P_Downstream, queryForm.queryFormP2P, tabFilter.P2PMessage)
        }
    });

    laydate.render({
        elem: '#roomCreateDate',
        type: 'date', // 新增两个类型的支持 range的时候的date和datetime
        range: $.dateSeparater,
        format: "yyyy-MM-dd HH:mm:ss",
        value: begin + " ~ " + end,
        quickConfirm: true, // 是否选择之后快速确定
        quickSelect: [
            'lastMonth', // 上个月
            'thisMonth', // 这个月
            {
                title: '最近7天',
                value: function () {
                    var date1 = new Date();
                    var date2 = new Date();
                    date1.setDate(date1.getDate() - 7);
                    date1.setHours(0, 0, 0, 0);
                    date2.setHours(23, 59, 59, 0);
                    return [date1, date2];
                }()
            },
            {
                title: '最近30天',
                value: function () {
                    var date1 = new Date();
                    var date2 = new Date();
                    date1.setDate(date1.getDate() - 30);
                    date1.setHours(0, 0, 0, 0);
                    date2.setHours(23, 59, 59, 0);
                    return [date1, date2];
                }()
            },
            {
                title: '最近90天',
                value: function () {
                    var date1 = new Date();
                    var date2 = new Date();
                    date1.setDate(date1.getDate() - 90);
                    date1.setHours(0, 0, 0, 0);
                    date2.setHours(23, 59, 59, 0);
                    return [date1, date2];
                }()
            }

        ],
        done: function (value, date) {

            if (!value) {
                $("#startLeftRoom,#startRightRoom").val('');
                return;
            }
            $("#startLeftRoom").val(value.split($.dateSeparater)[0].trim());
            $("#startRightRoom").val(value.split($.dateSeparater)[1].trim());
            load(MetricType.Msg_Room_Upstream, MetricType.Msg_Room_Downstream, queryForm.queryFormRoom, tabFilter.roomMessage)
        }
    });

    function initDate() {
        $("#startLeftP2P").val(begin);
        $("#startRightP2P").val(end);
        $("#startLeftRoom").val(begin);
        $("#startRightRoom").val(end);
    }


})