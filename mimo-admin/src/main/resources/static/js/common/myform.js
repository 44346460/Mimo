'use strict';
  
layui.define(['form','jquery','layer','mylayer'],function(exports){
  
  var form = layui.form;
  var $ = layui.$;
  var layer = layui.layer;
  var mylayer = layui.mylayer;
  var ELEM = '.layui-form';

  form.verify({
    'letterfirst-number': function (value, item) {
      if (/[^a-zA-Z0-9]|^[0-9]/g.test(value)) {
        return '只能输入字母和数字,且不能以数字开头'
      }
    }
    , 'positive-integer': function (value, item) {
      if (/[^0-9]|^0/g.test(value)) {
        return '只能输入正整数'
      }
    }
    , 'float-number': function (value, item) {
      if (/[^.\d]|^\./g.test(value)) {
        return '只能输入数字和.'
      }
    }
    , 'letter-number': function (value, item) {
      if (/[^a-zA-Z0-9]/g.test(value)) {
        return '只能输入字母和数字'
      }
    }
    , 'only-number': function (value, item) {
      if (/[^0-9]/g.test(value)) {
        return '只能输入数字'
      }
      if(item.max && Number(value) > Number(item.max)){
        return '输入数字不能大于' + item.max
      }
      if(item.min && Number(value) < Number(item.min)){
        return '输入数字不能小于' + item.min
      }
    }
    , 'accurate-two': function (value, item) {
      if (/[^.\d]|^\./g.test(value)) {
        return '只能输入数字和.'
      }
      if(item.max && Number(value) > Number(item.max)){
        return '输入数字不能大于' + item.max
      }
      if(item.min && Number(value) < Number(item.min)){
        return '输入数字不能小于' + item.min
      }
    }
    , 'minlength': function (value, item) {
      if(item.required && item.minLength && value.length < Number(item.minLength)){
        return '输入长度不能小于' + item.minLength
      }
    }

  });
  
  var Myform = function(){
    this.config = form.config;
  };
  
  Myform.prototype.verifyForm = function(filter){
    
    var formElem = $(ELEM + '[lay-filter="' + filter +'"]');
    var verifyElem = formElem.find('*[lay-verify]'), //获取需要校验的元素
    verify = myform.config.verify, stop = null,
    DANGER = 'layui-form-danger';
  
    //开始校验
    layui.each(verifyElem, function(_, item){
      var othis = $(this)
      ,vers = othis.attr('lay-verify').split('|')
      ,verType = othis.attr('lay-verType') //提示方式
      ,value = othis.val();
      
      othis.removeClass(DANGER);
      layui.each(vers, function(_, thisVer){
        var isTrue //是否命中校验
        ,errorText = '' //错误提示文本
        ,isFn = typeof verify[thisVer] === 'function';
        
        //匹配验证规则
        if(verify[thisVer]){
          var isTrue = isFn ? errorText = verify[thisVer](value, item) : !verify[thisVer][0].test(value);
          errorText = errorText || verify[thisVer][1];
          
          //如果是必填项或者非空命中校验，则阻止提交，弹出提示
          if(isTrue){
            //提示层风格
            if(verType === 'tips'){
              layer.tips(errorText, function(){
                if(typeof othis.attr('lay-ignore') !== 'string'){
                  if(item.tagName.toLowerCase() === 'select' || /^checkbox|radio$/.test(item.type)){
                    return othis.next();
                  }
                }
                return othis;
              }(), {tips: 1});
            } else if(verType === 'alert') {
              layer.alert(errorText, {title: '提示', shadeClose: true});
            } else {
              layer.msg(errorText, {icon: 5, shift: 6});
            }
            item.focus(); //非移动设备自动定位焦点
            othis.addClass(DANGER);
            return stop = true;
          }
        }
      });
      if(stop) return stop;
    });
    
    if(stop){
      return false;
    }else{
      return true;
    }
  }
  
  //校验form中可见的元素
  Myform.prototype.checkVisible = function(filter){

    var DANGER = 'layui-form-danger';
    var formElem = $(ELEM + '[lay-filter="' + filter +'"]');
    
    for(let _this of formElem.find("input:visible,textarea:visible")){
      $(_this).removeClass(DANGER);
      
      if($(_this).prop("required") && !$(_this).prop("disabled") && (!$(_this).val() || $(_this).val().trim().length == 0)){
        var errorText = "必填项不能为空";
        validateTips(errorText,_this);
        return false;
      }
      
      if($(_this).attr('max') && Number($(_this).val()) > Number($(_this).attr('max'))){
        var errorText = '输入数字不能大于' + $(_this).attr('max');
        validateTips(errorText,_this);
        return false;
      }
      
      if($(_this).attr('min') && Number($(_this).val()) < Number($(_this).attr('min'))){
        var errorText = '输入数字不能小于' + $(_this).attr('min');
        validateTips(errorText,_this);
        return false;
      }
    }
    
    return true;
  };
  
  function validateTips(errorText,_this){
    var DANGER = 'layui-form-danger';
    
    layer.tips(errorText, function(){
      return _this;
    }(), {tips: 1});
    
    $(_this).addClass(DANGER);
    $(_this).focus();
  }
  
  //初始赋值
  Myform.prototype.val = function(filter, object){
    var that = this
    ,formElem = $(ELEM + '[lay-filter="' + filter +'"]');
    if(!formElem[0]) formElem = $("#" + filter);
    
    formElem.each(function(index, item){
      var itemFrom = $(this);
      layui.each(object, function(key, value){
        var itemElem = itemFrom.find('[name="'+ key +'"]')
        ,type;
        
        //如果对应的表单不存在，则不执行
        if(!itemElem[0]) return;
        type = itemElem[0].type;
        
        //如果为复选框
        if(type === 'checkbox'){
          itemElem[0].checked = value;
        } else if(type === 'radio') { //如果为单选框
          itemElem.each(function(){
            if(this.value === value ){
              this.checked = true
            }     
          });
        } else { //其它类型的表单
          itemElem.val(value);
        }
        
        if(itemElem.is('span') || itemElem.is('div')){
          itemElem.text(value);
        }else if(itemElem.is('img') || itemElem.is('video')){
          itemElem.attr('src',value);
        }
        
      });
    });
    form.render(null, filter);
  };
  
  
  var myform = new Myform();

  exports('myform', myform);
}); 


 