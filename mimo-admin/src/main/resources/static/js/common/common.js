'use strict';
//config的设置是全局的
layui.config({
    base: '/js/common/' //假设这是你存放拓展模块的根目录
    , cache: true
}).extend({ //设定模块别名
    mylayer: 'mylayer' //如果 mymod.js 是在根目录，也可以不用设定别名
    , mytable: 'mytable'
    , myform: 'myform'
});

layui.extend({ //设定模块别名
    formSelects: '{/}/plugins/formSelects/formSelects-v4.min'  // {/}的意思即代表采用自有路径，即不跟随 base 路径
    , laydatePro: '{/}/plugins/layui/laydatePro/laydatePro'
});

layui.define(['form', 'element', 'mylayer', 'layer', 'jquery'], function (exports) {

    var form = layui.form,
        element = layui.element,
        $ = layui.$,
        layer = layui.layer,
        mylayer = layui.mylayer;

    //日期范围选择分隔符
    $.dateSeparater = '~';
    $.serializeJSON.defaultOptions.skipFalsyValuesForTypes = ["string"];

    $.validate = {
        trim: $("input,textarea").blur(function () {
            $(this).val($(this).val().trim());
        }),
        noIllegal: $("body").on('input', '.no-illegal', function (e) {
            //不能输入特殊字符
            $(this).val($(this).val().replace(/[/'"?.`!@#$%^&*]/g, ''));
        }),
        positiveInteger: $("body").on('input', '.positive-integer', function (e) {
            //正整数(最小1)
            $(this).val($(this).val().replace(/[^0-9]|^0/g, ''));
        }),
        floatNumber: $("body").on('input', '.float-number', function (e) {
            //金额，可输入数字和.
            $(this).val($(this).val().replace(/[^.\d]|^\./g, ''));
        }),
        letterFirstAndNumber: $("body").on('input', '.letterfirst-number', function (e) {
            //只能输入字母和数字
            $(this).val($(this).val().replace(/[^a-zA-Z0-9]|^[0-9]/g, ''));
        }),
        letterNumber: $('body').on('input', '.letter-number', function (e) {
            //只能输入字母和数字
            $(this).val($(this).val().replace(/[^a-zA-Z0-9]/g, ''));
        }),
        onlyNumber: $("body").on('input', '.only-number', function (e) {
            //只允许输入数字
            $(this).val($(this).val().replace(/[^0-9]/g, ''));
        }),
        noNumber: $("body").on('input', '.no-number', function (e) {
            //不能输入数字
            $(this).val($(this).val().replace(/[0-9]/g, ''));
        }),
        accurateTwo: function () {
            $("body").on('input', '.accurate-two', function () {
                var val = $(this).val().replace(/[^.\d]|^\./g, '').replace(/\.{2,}/g, ".")
                    .replace(".", "$#$").replace(/\./g, "").replace("$#$", ".")
                    .replace(/^(\-)*(\d+)\.(\d\d).*$/, '$1$2.$3');

                $(this).val(val);

            });

            $("input.accurate-two").blur(function () {
                if (!$(this).val()) return;
                let number = Number($(this).val());
                number = isNaN(number) ? '' : number.toFixed(2);
                $(this).val(number);
            });
        },
        init: function () {
            this.trim;
            this.noIllegal;
            this.positiveInteger;
            this.floatNumber;
            this.letterNumber
            this.letterFirstAndNumber;
            this.onlyNumber;
            this.accurateTwo();
        }
    }

    $.validate.init();

    $('body').on("mouseover mouseout", ".showme-tips", function (event) {
        var tipindex;
        if (event.type == "mouseover") {
            //鼠标悬浮
            tipindex = layer.tips($(this).attr("data-title"), this, {tips: 1});
        } else if (event.type == "mouseout") {
            //鼠标离开
            layer.close(tipindex);
        }
    })

    /**
     * 将数值四舍五入(保留2位小数)后格式化成金额形式
     *
     * @param num 数值(Number或者String)
     * @return 金额格式的字符串,如'1,234,567.45'
     * @type String
     */
    $.formatCurrency = function (num) {
        num = num.toString().replace(/\$|\,/g, '');
        if (isNaN(num))
            num = "0";
        var sign = (num == (num = Math.abs(num)));
        num = Math.floor(num * 100 + 0.50000000001);
        var cents = num % 100;
        num = Math.floor(num / 100).toString();
        if (cents < 10)
            cents = "0" + cents;
        for (var i = 0; i < Math.floor((num.length - (1 + i)) / 3); i++)
            num = num.substring(0, num.length - (4 * i + 3)) + ',' +
                num.substring(num.length - (4 * i + 3));
        return (((sign) ? '' : '-') + num + '.' + cents);
    }

    /**
     * 将正整型数值，格式化成千分位形式
     *
     * @param num 数值(Integer(1234567))
     * @return 千分位格式的字符串,如'1,234,567'
     * @type String
     */
    $.formatInteger = function (num) {
        num = num.toString().replace(/\$|\,/g, '');
        if (isNaN(num))
            num = "0";
        for (var i = 0; i < Math.floor((num.length - (1 + i)) / 3); i++)
            num = num.substring(0, num.length - (4 * i + 3)) + ',' +
                num.substring(num.length - (4 * i + 3));
        return num;
    }

    $.fn.clearData = function (id) {

        //class=not-clear则不清空
        $(this).find("input").not(".not-clear").each(function () {
            let type = $(this).prop('type');

            if (type === 'text' || type === 'number' || type === 'hidden' || type === 'password' || type === 'textarea') {
                $(this).val("");
            }
        });

        $(this).find("textarea").val("");
        $(this).find("img,video").removeAttr("src");

        $(this).find("select").each(function () {
            $(this).find("option:first").prop("selected", true);
        });
    };

    $("#reset").click(function () {
        $("#queryForm").clearData();
    });

    $("button[type=reset]").click(function () {
        $(this).closest('form').clearData();
    });

    $("#back").click(function () {
        history.back();
    });

    /**
     * 初始化树形结构
     * id:树形结构所在html的id
     * url:查询url
     * param:查询时的参数,json格式
     */
    $.initCheckboxTree = function (treeid, url, param) {
        var promise = new Promise(function (resolve, reject) {

            let setting = {
                check: {
                    enable: true
                },
                view: {
                    showIcon: false
                },
                data: {
                    simpleData: {
                        enable: true
                    }
                }
            };

            $.get(url, null, function (result) {
                if (result.success) {
                    expandRoot(result.treeNodes);
                    $.fn.zTree.init($("#" + treeid), setting, result.treeNodes);
                    resolve();
                } else {
                    mylayer.failMsg("初始化失败");
                }
            });

        });
        return promise;
    }

    //默认展开root菜单
    function expandRoot(zNodes) {
        for (let node of zNodes) {
            if (node.level == 0) {
                node.open = true;
                break;
            }
        }
    }

    String.prototype.replaceAll = function (f, e) {//把f替换成e
        var reg = new RegExp(f, "g"); //创建正则RegExp对象
        return this.replace(reg, e);
    }

    String.prototype.endWith = function (endStr) {
        var d = this.length - endStr.length;
        return (d >= 0 && this.lastIndexOf(endStr) == d)
    }

    /**
     * treeids: 格式'id1,id2,id3'
     * url: url
     * param: 参数
     */
    $.initSimpleTree = function (treeids, url, param) {
        var promise = new Promise(function (resolve, reject) {

            let setting = {
                data: {
                    simpleData: {
                        enable: true
                    }
                },
                callback: {
                    beforeExpand: $.zTreeBeforeExpand,
                    onExpand: $.zTreeOnExpand,
                    onClick: $.zTreeOnClick
                }
            };

            $.get(url, null, function (result) {
                if (result.success) {
                    expandRoot(result.treeNodes);
                    for (let treeid of treeids.split(',')) {
                        $.fn.zTree.init($("#" + treeid), setting, result.treeNodes);
                    }
                    resolve();
                } else {
                    mylayer.failMsg("初始化树形结构失败");
                }
            });

        });
        return promise;
    }


    /**
     * 用于判断当前字符串是否为空，对于空字符串会当成不存在
     */
    $.isEmptyString = function (strings) {
        return strings.replace(/(^s*)|(s*$)/g, "").length == 0;
    };

    /**
     * post异步提交，json格式交互
     *
     * url:提交url
     * jsonParam:参数
     * callback:异步提交执行完毕的回调函数
     */
    $.ajaxPost = function (url, jsonParam, callback) {
        $.ajax({
            type: "POST",
            url: url,
            data: jsonParam,
            contentType: "application/json",
            complete: function (xhr) {
                if (timeout(xhr)) return;
                if (unauthorized(xhr)) return;
                callback(xhr.responseJSON);
            }
        });
    };

    /**
     * get异步提交，json格式交互(请优先使用$.get())
     *
     * url:提交url
     * jsonParam:参数
     * callback:异步提交执行完毕的回调函数
     */
    $.ajaxGet = function (url, jsonParam, callback) {
        $.ajax({
            type: "GET",
            url: url,
            data: jsonParam,
            contentType: "application/json",
            complete: function (xhr) {
                if (timeout(xhr)) return;
                if (unauthorized(xhr)) return;
                callback(xhr.responseJSON);
            }
        });
    };

    /**
     * 异步下载
     * url: 导出的url
     * method: get 或者 post
     * data: 参数
     */
    $.ajaxDownload = function (url, method, data, callback) {
        callback = callback || function () {
        };
        var xhr = new XMLHttpRequest();
        xhr.onerror = function () {
            mylayer.failMsg("网络请求异常", function () {
                callback(false);
            });
            $.cancelOverlay();
        };
        xhr.onloadstart = function () {
            $.loadingOverlay();
        };
        xhr.onprogress = function updateProgress(event) {
            if (event.lengthComputable) {
                var completedPercent = Math.floor(100 * event.loaded / event.total);
                console.log("onprogress" + completedPercent);
            }
        };
        xhr.onload = function () {
            $.cancelOverlay();
            console.log("onload：xhr.status" + xhr.status);
            if (xhr.status != 200) return;

            var blob = new Blob([xhr.response]);

            if ("application/octet-stream" == xhr.response.type) {
                // 请求正常，将文件写入本地
                var fileName = xhr.getResponseHeader("content-disposition").replace('attachment;filename=', '');
                mysaveAs(blob, fileName);
                callback(true);
            } else if ("application/json" == xhr.response.type || 'text/plain' == xhr.response.type) {
                // 请求异常，将流转换为字符，再转换为json对象，获取msg
                var reader = new FileReader();
                reader.onload = function (event) {
                    //提示用户
                    mylayer.failMsg(JSON.parse(reader.result).msg, function () {
                        callback(false);
                    });
                };
                reader.readAsText(blob);
            } else {
                mylayer.failMsg("导出失败" + ':' + xhr.response.type);
            }
        };

        if (method.toLowerCase() === 'get') {
            if (data) {
                url = url + '?' + data;
            }
            xhr.open(method, url);
            xhr.responseType = 'blob';
            try {
                xhr.send(null);
            } catch (e) {
                console.log(e);
            }
        } else if (method.toLowerCase() === 'post') {
            xhr.open(method, url);
            xhr.responseType = 'blob';
            try {
                xhr.setRequestHeader('Content-Type', 'application/json');
                var param = {};
                for (var key in data) {
                    if (data[key] !== '') {
                        param[key] = data[key];
                    }
                }
                xhr.send(JSON.stringify(param));
            } catch (e) {
                console.log(e);
            }
        }


    };

    /**
     * 从数组中删除一个元素
     *
     * array:数组
     * delElement:要删除的元素
     */
    $.removeFromArray = function (array, delElement) {
        $.each(array, function (index, item) {
            // index是索引值（即下标）   item是每次遍历得到的值；
            if (item == delElement) {
                array.splice(index, 1);
            }
        });
    };
    

    function timeout(xhr) {
        var sessionStatus = xhr.getResponseHeader('sessionStatus');
        $.cancelOverlay();
        var hasTimeout = sessionStatus && sessionStatus === 'sessionTimeout';
        if (hasTimeout) {
            mylayer.failMsg("会话超时，请重新登录", function () {
                location.href = '/login';
            });
        }

        return hasTimeout;
    }

    function unauthorized(xhr) {
        var sessionStatus = xhr.getResponseHeader('sessionStatus');
        $.cancelOverlay();
        var isUnauthorized = sessionStatus && sessionStatus === 'unauthorized';
        if (isUnauthorized) {
            mylayer.failMsg("您没有访问权限，请重新登录", function () {
                location.href = '/login';
            });
        }

        return isUnauthorized;
    }

    $("form").submit(function () {
        $('input').blur();
        $.loadingOverlay();
    });

    $.ajaxSetup({
        timeout: 1800000,
        beforeSend: function (xhr) {
            $.loadingOverlay();
        },
        complete: function (xhr, status) {
            if (status == 'timeout') {
                $.cancelOverlay();
                mylayer.failMsg("请求失败,一会再试啊亲");
            } else {
                timeout(xhr);
                unauthorized(xhr);
            }
        },
        error: function (xhr, status, error) {
            mylayer.failMsg("请求失败,一会再试啊亲");
        }
    });

    $.loadingOverlay = function () {
        layer.load();
    };

    $.cancelOverlay = function () {
        layer.closeAll('loading');
    };

    $('button:not(.login), input').keydown(function (event) {
        switch (event.keyCode) {
            case 13:
                return false;
        }
    });

});

//对Date的扩展，将 Date 转化为指定格式的String
//月(M)、日(d)、小时(h)、分(m)、秒(s)、季度(q) 可以用 1-2 个占位符， 
//年(y)可以用 1-4 个占位符，毫秒(S)只能用 1 个占位符(是 1-3 位的数字) 
Date.prototype.Format = function (fmt) {
    var o = {
        "M+": this.getMonth() + 1, //月份
        "d+": this.getDate(), //日
        "h+": this.getHours(), //小时
        "m+": this.getMinutes(), //分
        "s+": this.getSeconds(), //秒
        "q+": Math.floor((this.getMonth() + 3) / 3), //季度
        "S": this.getMilliseconds() //毫秒
    };
    if (/(y+)/.test(fmt)) fmt = fmt.replace(RegExp.$1, (this.getFullYear() + "").substr(4 - RegExp.$1.length));
    for (var k in o)
        if (new RegExp("(" + k + ")").test(fmt)) fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) : (("00" + o[k]).substr(("" + o[k]).length)));
    return fmt;
}

//将秒数转换为HH:mm:ss格式
function formatTime(seccond) {

    if (seccond > 0) {
        var h = Math.floor(seccond / (60 * 60));
        var m = Math.floor((seccond - (h * 60 * 60)) / 60);
        var s = seccond - (h * 60 * 60) - (m * 60);


        return format(h) + ':' + format(m) + ':' + format(s);
    } else {
        return '00:00:00';
    }

    function format(num) {
        return num < 10 ? "0" + num : num;
    }
}

function mysaveAs(blob, fileName) {
    saveAs(blob, fileName);
}

var RandomStringUtils = {
    /**
     * @returns {*} 32位数字字母混合的随机字符串
     */
    uniqueRandom32: function () {
        return this.uniqueRandom(32);
    },
    /**
     * @param len 长度
     * @param radix 基数
     * @returns {string} 指定长度和基数的随机字符串
     */
    uniqueRandom: function (len, radix) {
        var chars = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz'.split('');
        var uuid = [], i;
        radix = radix || chars.length;

        if (len) {
            // Compact form
            for (i = 0; i < len; i++) uuid[i] = chars[0 | Math.random() * radix];
        } else {
            // rfc4122, version 4 form
            var r;

            // rfc4122 requires these characters
            uuid[8] = uuid[13] = uuid[18] = uuid[23] = '-';
            uuid[14] = '4';

            // Fill in random data.  At i==19 set the high bits of clock sequence as
            // per rfc4122, sec. 4.1.5
            for (i = 0; i < 36; i++) {
                if (!uuid[i]) {
                    r = 0 | Math.random() * 16;
                    uuid[i] = chars[(i == 19) ? (r & 0x3) | 0x8 : r];
                }
            }
        }

        return uuid.join('');
    }
}

