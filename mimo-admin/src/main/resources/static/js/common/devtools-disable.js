;(function () {

  var devtoolsDetectEnable = document.getElementById('devtoolsDetectEnable').value;

  //测试环境可关闭，如果关闭则不探测
  if (devtoolsDetectEnable === 'false') return;

  // 检测控制台打开状态
  devtoolsDetector.addListener(function (isOpen) {
    // isOpen = true -> 已打开开发者模式
    console.log('isOpen=', isOpen)

    if (isOpen) {
      // do something...
      var curmenu = JSON.parse(window.sessionStorage.getItem("curmenu"));
      var pathname = curmenu && curmenu.href || location.pathname;
      window.location.href = '/devtools?pathname=' + pathname;
    }
  });
  devtoolsDetector.lanuch();

  // 禁止快捷键打开控制台
  document.addEventListener('keydown', function (e) {
    return !(
        123 === e.keyCode || //F12
        (e.ctrlKey || e.ctrlkey) && (e.shiftKey || e.shiftkey) && (
            73 == e.keyCode || 74 == e.keyCode || 75 == e.keyCode || 76 == e.keyCode
        ) //ctrl + shift + i / j / k / l
    ) || (e.returnValue = false)
  });

  // 禁止右击
  document.oncontextmenu = function (event) {
    if (window.event) {
      event = window.event;
    }
    try {
      var the = event.srcElement;
      if (!((the.tagName == "INPUT" && the.type.toLowerCase() == "text") || the.tagName == "TEXTAREA")) {
        return false;
      }
      return true;
    } catch (e) {
      return false;
    }
  }
})()