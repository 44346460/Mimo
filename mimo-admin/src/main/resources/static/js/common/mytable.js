'use strict';
  
layui.define(['table','jquery','myform'],function(exports){
  
  var table = layui.table;
  var myform = layui.myform;
  var $ = layui.$;
  
  var mytable = {
      //默认page为true，支持分页
      initTable: initTable
      ,renderTable: renderTable
      ,reloadTable: reloadTable
  } 
  
  function reloadTable(reload,filter,options){
    filter = filter || 'listTable';
    options = options || {};
    options.formId = options.formId || 'queryForm';
    
    reload.reload(filter,{
      where: options.where || $('#' + options.formId).serializeJSON({skipFalsyValuesForTypes: []})
      ,page: {
        curr: 1 //重新从第 1 页开始
      }
    });
    
  }
  
  function buildOptions(options){
    options = options || {};
    
    options.id = options.id || 'listTable';
    options.cellMinWidth = options.cellMinWidth || 120;
    options.page = typeof options.page === 'boolean' ? options.page:true;
    options.skin = options.skin || 'line';
    options.toolbar = options.toolbar || '#toolbarHead';
    options.defaultToolbar = options.defaultToolbar || ['filter'];
    options.response = options.response || {
      countName: 'total' //数据总数的字段名称，默认：count
      ,dataName: 'items' //数据列表的字段名称，默认：data
    };

    if ($('#' + options.formId)[0]) {
      options.where = typeof options.where === 'object' && options.where
          || typeof options.where === 'function' && options.where()
          || $('#' + options.formId).serializeJSON({skipFalsyValuesForTypes: []});
    }
    
    var optionDone = options.done;
    options.done = function(res, curr, count){
      
      if(options.initForm && $('#' + options.formId)[0]){
        var key = options.formId == 'queryForm' ? location.pathname :
            location.pathname + '-' + options.formId;

        layui.sessionData('queryParam', {
          key: key
          , value: $('#' + options.formId).serializeJSON({skipFalsyValuesForTypes: []})
        });
      }
      
      var done = typeof optionDone !== 'function' && typeof renderDone === 'function' ? renderDone:optionDone;
      if(done){
        done(res, curr, count);
      }
    };
    
    if(options.page){
      options.request = options.request || {
        pageName: 'page'
          ,limitName: 'size'
        }
      options.limits = options.limits || [10,50,100];
    }else{
      
      options.limit = 10000000;
      options.limits = [10000000];
    }
  }
  
  //页面加载将缓存的查询条件赋值给form
  function setQueryParam(options){
    var key = options.formId == 'queryForm' ? location.pathname :
        location.pathname + '-' + options.formId;
    
    if (options.initForm && layui.sessionData('queryParam')[key]) {
      myform.val(options.formId, layui.sessionData('queryParam')[key]);
    }
  }
  
  function initTable(layFilter,options){
    
    options.formId = options.formId || 'queryForm';
    options.initForm = typeof options.initForm === 'boolean' ? options.initForm:true;
    setQueryParam(options);
    buildOptions(options);
    
    table.init(layFilter,options);

  };
  
  function renderTable(options){
    
    options.formId = options.formId || 'queryForm';
    options.initForm = typeof options.initForm === 'boolean' ? options.initForm:true;
    setQueryParam(options);
    buildOptions(options);
    options.elem = '#' + options.id;
    
    return table.render(options);
  };

  exports('mytable', mytable);
}); 


 