'use strict';
  
layui.define('layer',function(exports){
  var mylayer = {
      msg: function(msg,options,callback){
        options = options || {};
        options.time = options.time || 1000;
        layer.load(2, {time: options.time});
        layer.msg(msg, {time: options.time},callback);
      },
      successMsg: function(msg,options,callback) {
        if(typeof(options) === 'function'){
          callback = options;
          options = {
              time: 1000
          }
        }else{
          options = options || {};
          options.time = options.time || 1000;
        }

        layer.load(2, {time: options.time});
        layer.msg(msg, {icon: 1, time: options.time},callback);
      },
      failMsg: function(msg,options,callback) {
        if(typeof(options) === 'function'){
          callback = options;
          options = {
              time: 1000
          }
        }else{
          options = options || {};
          options.time = options.time || 1000;
        }
        
        msg = msg || "no message";
        layer.load(2, {time: options.time});
        layer.msg(msg, {icon: 2, time: options.time},callback);
      },
      confirm: function(content, yes, cancel, options={}){
        layer.confirm(content, {icon: 3, title:'提示',closeBtn: 0}, 
            function(index){
              layer.close(index);
              if(typeof(yes) == 'function'){
                yes(index);
              }else{
                mylayer.failMsg("callback is not a function");
              }
            },function(index){
              layer.close(index);
              if(typeof(cancel) == 'function'){
                cancel(index);
              }
            });
      },
      close: function(index){
        layer.close(index);
      },
      viewLarge: function(options){
        if (!options || !options.src) return;
        layer.photos({
          photos: {
            "title": "", //相册标题
            "id": '', //相册id
            "start": 0, //初始显示的图片序号，默认0
            "data": [   //相册包含的图片，数组格式
              {
                "alt": "",
                "pid": '', //图片id
                "src": options.src, //原图地址
                "thumb": '' //缩略图地址
              }
            ]
          }
          ,anim: 0 //0-6的选择，指定弹出图片动画类型，默认随机（请注意，3.0之前的版本用shift参数）
        }); 
      }
    };
  
  exports('mylayer', mylayer);
}); 


 