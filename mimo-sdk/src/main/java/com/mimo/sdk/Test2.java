package com.mimo.sdk;

import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.mimo.sdk.session.ICallback;

public class Test2 {
  public static final Logger log = LoggerFactory.getLogger(Test2.class);

  public static void main(String[] args) throws Exception {
    String roomId = "hy_001";
    Collection<String> users = Arrays.asList("hongyu1", "test");
    String token = "abc";
    String deviceId = "dddd";
    String url = "wss://mimo-ws.elelive.net/ws/connect";

    users.parallelStream().forEach(userId -> {
      AtomicInteger counter = new AtomicInteger(0);

      MimoClient client = new MimoClient(url, userId, token, deviceId, new ICallback() {
        @Override
        public void onP2PMessage(String userId, String content) {
          log.info("时间:{},userId:{} -> content:{}", new Date().toLocaleString(), userId, content);
        }

        @Override
        public void onJoinRoomSuccess(String roomId) {
          log.info("user:[{}]  加群【{}】成功", userId, roomId);
        }

        @Override
        public void onJoinRoomFailed(String roomId) {
          log.info("user:[{}]  加群【{}】失败", userId, roomId);
        }

        @Override
        public void onLeaveRoomSuccess(String roomId) {
          log.info("user:[{}]  离群【{}】成功", userId, roomId);
        }

        @Override
        public void onLeaveRoomFailed(String roomId) {
          log.info("user:[{}] 离群【{}】失败", userId, roomId);
        }

        @Override
        public void onRoomMessage(String roomId, String userId, String content) {
          log.info("user:[{}] 房间总数:{}", userId, counter.incrementAndGet());
        }

        @Override
        public void onErrorMessage(int code, String content) {
          log.error("user:[{}] 异常信息Code:{}, msg:{}", userId, code, content);
        }

        @Override
        public void onComplete(Throwable e) {
          log.warn("user:[{}] 连接中断", userId);
        }

        @Override
        public void onConnected() {
          log.warn("user:[{}] 连接成功", userId);
        }

        @Override
        public void onConnecting() {
          log.warn("user:[{}] 正在连接中", userId);
        }
      });
      client.connect();
      try {
        TimeUnit.SECONDS.sleep(1);
        client.joinRoom(roomId);
      } catch (InterruptedException e1) {
        log.error("", e1);
      }
    });

    System.in.read();
  }

}
