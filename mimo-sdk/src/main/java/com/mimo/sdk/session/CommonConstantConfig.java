package com.mimo.sdk.session;

import java.util.HashMap;
import java.util.Map;

public final class CommonConstantConfig {

  public static final long MIMO_EXPIRE_IN_MILL = 7000; // 7秒，则退场

  public static final long MSG_DROP_EXPIRE_IN_MILL = 5000; // 消息队列超过5秒的消息，会认定为失败

  public static final long MIMO_HEARTBEAT_TIMING = 5000;// 心跳间隔

  public static final long MIMO_MSG_CHECKING_TIMING = 3000;// 过期消息检查间隔

  // 以下Code则中断重连，直接退出
  public static final Map<Integer, String> terminates = new HashMap<>();
  static {
    terminates.put(1000, "Login Failed");
    terminates.put(1001, "User Blocked");
    terminates.put(1003, "Force Logout");
  }

}
