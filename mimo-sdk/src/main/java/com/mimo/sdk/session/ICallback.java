package com.mimo.sdk.session;

import javax.annotation.Nullable;

/**
 * 一个简单的回调接口汇集处
 * 
 * @author Hongyu
 */
public interface ICallback {
  /**
   * 点对点消息回调
   * 
   * @param userId
   *          发起人
   * @param content
   *          发送的内容
   */
  void onP2PMessage(String userId, String content);

  /**
   * 加房间成功的回调
   * 
   * @param roomId
   *          成功加入该房间的回调
   */
  void onJoinRoomSuccess(String roomId);

  /**
   * 加房间失败的回调
   * 
   * @param roomId
   */
  void onJoinRoomFailed(String roomId);

  /**
   * 离开房间成功的回调
   */
  void onLeaveRoomSuccess(String roomId);

  /**
   * 离开房间失败的回调
   */
  void onLeaveRoomFailed(String roomId);

  /**
   * 房间消息回调
   * 
   * @param roomId
   *          房间来源
   * @param userId
   *          房间的发起人
   * @param content
   *          房间消息内容
   */
  void onRoomMessage(String roomId, String userId, String content);

  /**
   * 业务异常回调,这里就不做太多的异常表示，把一些不可控的Error暂时统一扔到此处
   * 
   * @param code
   * @param content
   */
  void onErrorMessage(int code, String content);

  /**
   * 连接最终结束，即整个会话最终被中止
   * <p>
   * 如果入参为非空，则意味着是Error导致的连接中断
   */
  void onComplete(@Nullable Throwable e);

  /**
   * 连接成功
   */
  void onConnected();

  /**
   * 重连中
   */
  void onConnecting();

}
