package com.mimo.sdk.session;

public enum ConnectionStatus {
  // 初始状态
  None,

  // 正在连接中
  Connecting,

  // 连接成功
  Connected,

  // 断连，可能是一个中间状态
  Disconnected,

  // 终止状态，一般是退登
  Terminate;
}
