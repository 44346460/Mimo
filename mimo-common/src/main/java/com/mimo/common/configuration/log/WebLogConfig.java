package com.mimo.common.configuration.log;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration(proxyBeanMethods = false)
public class WebLogConfig {

  @Bean
  public WebLogAspect webLogAspect() {
    return new WebLogAspect();
  }

}
