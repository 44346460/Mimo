package com.mimo.common.configuration.mrlock.component;

import java.util.Set;

/**
 * 想要针对Redis生成多个键值，需要处理的场景过多，由具体业务的提供去定义,如何抽取出真正的K-V集合.<br>
 * 当用户提供该实现时，会扫描该接口具体实现所需要的类型入参，并传给该方法。 <br>
 * 但是，假设同一个接口入参有多个同样的类型参数时，则系统处理时，会默认抛出异常，因为不知道要绑定哪一个。
 * 
 * @author Hongyu
 */
public abstract class MultiRedisKeysGenerator<T> {

  protected Class<T> type;

  public MultiRedisKeysGenerator() {
    this.type = (Class<T>) getClass();
  }

  /**
   * 返回key集合，必须是非空，否则会报异常
   * 
   * @param source
   * @return
   */
  public abstract Set<String> generate(T source);
}
