package com.mimo.common.configuration.security.rbac;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.mimo.common.configuration.security.rbac.annotation.AccessAspect;
import com.mimo.common.configuration.security.rbac.service.IAccessDenyService;
import com.mimo.common.configuration.security.rbac.service.IAccessIdentityService;
import com.mimo.common.configuration.security.rbac.service.impl.DefaultAccessDenyServiceImpl;
import com.mimo.common.configuration.security.rbac.service.impl.HttpAccessIdentityServiceImpl;

@Configuration
public class AccessControlAutoConfiguration {

  /**
   * 提供一个基于Http上下文的身份Id识别器
   */
  @Bean
  @SuppressWarnings("rawtypes")
  @ConditionalOnMissingBean(IAccessIdentityService.class)
  public IAccessIdentityService httpAccessIdentityService() {
    return new HttpAccessIdentityServiceImpl();
  }

  /**
   * 默认的非法访问处理器
   */
  @Bean
  @SuppressWarnings("rawtypes")
  @ConditionalOnMissingBean(IAccessDenyService.class)
  public IAccessDenyService accessDenyService() {
    return new DefaultAccessDenyServiceImpl();
  }

  @SuppressWarnings("rawtypes")
  @Bean
  public AccessAspect accessAspect(@Autowired IAccessIdentityService accessIdentityService,
      @Autowired IAccessDenyService accessDenyService) {
    return new AccessAspect(accessDenyService, accessIdentityService);
  }
}
