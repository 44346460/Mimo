package com.mimo.common.configuration.limit.aspect;

public interface IAccessLimitService {
  public boolean accessIncr(String key, int count, long expireInMill);
}
