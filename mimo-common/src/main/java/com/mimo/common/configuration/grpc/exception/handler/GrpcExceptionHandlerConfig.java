package com.mimo.common.configuration.grpc.exception.handler;

import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration(proxyBeanMethods = false)
public class GrpcExceptionHandlerConfig {

  @ConditionalOnClass(name = "net.devh.boot.grpc.server.service.GrpcService")
  @Bean
  public GrpcExceptionHandlerAspect grpcExceptionHandlerAspect() {
    return new GrpcExceptionHandlerAspect();
  }

}
