package com.mimo.common.configuration;

import org.springframework.context.ApplicationContextAware;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.mimo.common.component.ApplicationContextHelper;

@Configuration(proxyBeanMethods = false)
public class ContextConfiguration {

  @Bean
  public ApplicationContextAware applicationContextHelper() {
    return new ApplicationContextHelper();
  }

}
