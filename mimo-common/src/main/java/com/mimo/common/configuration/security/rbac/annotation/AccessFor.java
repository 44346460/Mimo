package com.mimo.common.configuration.security.rbac.annotation;

import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import org.springframework.core.annotation.AliasFor;

import io.grpc.netty.shaded.io.netty.handler.codec.http.HttpHeaderValues;

/**
 * 构造一个接口权限的约束. 允许 "role" 进行访问.
 * <p>
 * 反之,如果一个接口上没有该注解,则不允许访问
 * 
 * @author Hongyu
 */
@Documented
@Retention(RUNTIME)
@Target(METHOD)
public @interface AccessFor {

  /**
   * 考虑到角色的多重性,暂时用于约束来自于App端的访问控制. 可以参考 {@link HttpHeaderValues}的相关Role值
   * 
   * @return
   */
  @AliasFor("role")
  String value() default "xApp";

  @AliasFor("value")
  String role() default "xApp";

}
