package com.mimo.common.configuration.security.rbac.service;

import javax.servlet.http.HttpServletRequest;

public interface IAccessIdentityService<I> {
  /**
   * @param Context
   *          从上下文中得到用户信息
   * @return
   */
  public I getIdentity(HttpServletRequest context);
}
