package com.mimo.common.configuration.semaphore.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.util.concurrent.TimeUnit;

/**
 * @author Hongyu
 */
@Target({ ElementType.METHOD })
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface DistributedRedisSemaphore {

  /**
   * 最大的信号量分发，需要保证在同一个key上下文的信号量大小是一致的.
   * <p>
   * <b>此处要特别说明,如果业务上同一个key被前后调用时,设置大小不一致，则系统则会永远以第一个被调用时的maxPermits作为桶的大小。且，由于该功能为分布式，即永久的被持久化 </b>
   */
  int maxPermits();

  /**
   * 可等待时间：10
   * 
   * @return
   */
  int waited() default 10;

  /**
   * 时间单位，默认单位为秒
   * 
   * @return
   */
  TimeUnit timeunit() default TimeUnit.SECONDS;

  /**
   * 支持SPEL解析,请用Template样板方式, 比如 key = #{#user.userId}
   * <p>
   * <b>
   * <li>特别说明，由于信号量是一个永久性的存在，所以，于redis上，表现为K-V String的模式，且TTL为-1
   * <li>在使用时,要特别小心这种ttl=-1这种key数量增加的处理
   */
  String key();

  /**
   * 如果无法成功获得信号量的时候，需要抛出的异常
   * 
   * @return
   */
  Class<? extends RuntimeException> exception() default RuntimeException.class;
}
