package com.mimo.common.configuration.limit;

import org.springframework.boot.autoconfigure.AutoConfigureAfter;
import org.springframework.boot.autoconfigure.data.redis.RedisAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.data.redis.core.script.DefaultRedisScript;
import org.springframework.data.redis.core.script.RedisScript;
import org.springframework.scripting.support.ResourceScriptSource;

import com.mimo.common.configuration.limit.aspect.AccessLimitAspect;
import com.mimo.common.configuration.limit.aspect.IAccessLimitService;

@Configuration
@AutoConfigureAfter(RedisAutoConfiguration.class)
public class AccessLimitConfiguration {

  @Bean("accessLimitScript")
  public RedisScript<Boolean> accessLimitScript() {
    DefaultRedisScript<Boolean> redisScript = new DefaultRedisScript<>();
    redisScript.setScriptSource(new ResourceScriptSource(new ClassPathResource("/lua/access_limit.lua")));
    redisScript.setResultType(Boolean.class);
    return redisScript;
  }

  @Bean
  public IAccessLimitService accessLimitAspect() {
    return new AccessLimitAspect();
  }
}
