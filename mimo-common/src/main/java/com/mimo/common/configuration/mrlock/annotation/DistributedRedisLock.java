package com.mimo.common.configuration.mrlock.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.util.concurrent.TimeUnit;

import com.mimo.common.configuration.mrlock.component.MultiRedisKeysGenerator;

/**
 * 目前该锁支持可重入概念
 * 
 * @author Hongyu
 */
@Target({ ElementType.METHOD })
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface DistributedRedisLock {

  /**
   * 可等待时间：10
   * 
   * @return
   */
  int waited() default 10;

  /**
   * 时间单位，默认单位为秒
   * 
   * @return
   */
  TimeUnit timeunit() default TimeUnit.SECONDS;

  /**
   * Keys的超时时间,必须大于0
   * 
   * @return
   */
  int expired() default 10;

  /**
   * 如果在参数入参中发现多个入参符合 k-v生成器的期望值，是否允许使用第一个入参。而不抛出异常。默认为false.
   * 
   * @return
   */
  boolean firstEnable() default false;

  /**
   * 公平锁，默认允许饥饿
   * 
   * @return
   */
  boolean fair() default false;

  /**
   * <b>如果Key不为空,则优先使用Key.
   * <p>
   * <b>支持SPEL解析,请用Template样板方式, 比如 key = #{#user.userId}
   * 
   * @return
   */
  String key() default "";

  /**
   * <b>只有key为空的情况下</b> 由业务层自行注入K-V提供逻辑 {@link com.mimo.common.configuration.mrlock.component.MultiRedisKeysGenerator}
   * 
   * @return
   */
  @SuppressWarnings("rawtypes")
  Class<? extends MultiRedisKeysGenerator> generator() default MultiRedisKeysGenerator.class;

  /**
   * 如果无法成功获取锁的时候，需要抛出的异常
   * 
   * @return
   */
  Class<? extends RuntimeException> failedLockException() default RuntimeException.class;

}
