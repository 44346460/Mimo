package com.mimo.common.configuration.broadcast.listener;

import com.mimo.common.configuration.broadcast.event.NotificationEvent;

public interface EventListener<E extends NotificationEvent> {

  public void onEvent(E event);
}
