package com.mimo.common.configuration.broadcast.event;

import java.util.Objects;

public abstract class NotificationEvent {
  // 定义事件来源
  private final Object source;

  public NotificationEvent(Object source) {
    super();
    Objects.requireNonNull(source, "source is required");
    this.source = source;
  }

  public Object getSource() {
    return source;
  }

}
