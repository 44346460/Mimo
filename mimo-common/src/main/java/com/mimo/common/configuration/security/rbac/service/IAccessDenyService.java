package com.mimo.common.configuration.security.rbac.service;

import java.lang.reflect.Method;

/**
 * 当用户访问越权时处理,一般由系统捕获后,对该接口做回调用.业务可以根据这些反馈信息做一些记录之类的动作
 * 
 * @author Hongyu
 */
public interface IAccessDenyService<T> {

  /**
   * @param identity
   *          访问者身份
   * @param resource
   *          目标资源
   */
  public void onDenied(T identity, Method resource);
}
