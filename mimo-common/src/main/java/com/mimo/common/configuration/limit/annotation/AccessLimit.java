package com.mimo.common.configuration.limit.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.util.concurrent.TimeUnit;

@Target({ ElementType.METHOD })
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface AccessLimit {

  /**
   * 可进入的总次数
   */
  int count();

  /**
   * 锁定期时间单位,默认值是秒
   * 
   * @return
   */
  TimeUnit timeUnit() default TimeUnit.SECONDS;

  /**
   * 锁定时长,默认值为10
   * 
   * @return
   */
  long expire() default 10;

  /**
   * <b>支持SPEL解析,请用Template样板方式, 比如 key = #{#user.userId}
   * 
   * @return
   */
  String key();

  /**
   * 如果无法成功获取锁的时候，需要抛出的异常
   * 
   * @return
   */
  Class<? extends RuntimeException> exception() default RuntimeException.class;

}
