package com.mimo.common.configuration.security.rbac.service.impl;

import java.lang.reflect.Method;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.mimo.common.configuration.security.rbac.service.IAccessDenyService;

/**
 * 默认根据用户ID,进行拒绝处理
 * 
 * @author Hongyu
 */
public class DefaultAccessDenyServiceImpl implements IAccessDenyService<String> {
  private static final Logger log = LoggerFactory.getLogger(DefaultAccessDenyServiceImpl.class);

  @Override
  public void onDenied(String identity, Method resource) {
    log.error("方法[{}]接口发现入侵,原请求账户为:{}", resource, identity);
  }

}
