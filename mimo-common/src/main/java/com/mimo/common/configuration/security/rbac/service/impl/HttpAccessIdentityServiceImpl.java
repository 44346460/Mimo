package com.mimo.common.configuration.security.rbac.service.impl;

import javax.servlet.http.HttpServletRequest;

import com.mimo.common.configuration.security.rbac.service.IAccessIdentityService;
import com.mimo.common.http.header.HttpHeaderParams;

/**
 * 基于当前业务,用户的识别来自于请求头
 * 
 * @author Hongyu
 */
public class HttpAccessIdentityServiceImpl implements IAccessIdentityService<String> {
  @Override
  public String getIdentity(HttpServletRequest context) {
    return context.getHeader(HttpHeaderParams.App_Key);
  }
}
