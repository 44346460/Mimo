package com.mimo.common.comet.dto.user;

import java.io.Serializable;

import org.springframework.util.StringUtils;
import org.springframework.validation.annotation.Validated;

import com.mimo.common.rpc.proto.UserProto.DeviceDTO.Builder;

/**
 * 客户端设备信息
 *
 * 
 */
@Validated
public class DeviceDTO implements Serializable {
  private static final long serialVersionUID = 1L;

  // 设备类型: WEB,android,ios
  private String terminal;

  // 操作系统
  private String os;

  // 设备品牌，比如OPPO，VIVO
  private String brand;

  // 手机型号
  private String model;

  // 应用版本，比如小象直播的应用版本
  private String appVersion;

  public DeviceDTO() {
    super();
  }

  public DeviceDTO(String terminal, String os, String brand, String model, String appVersion) {
    super();
    this.terminal = terminal;
    this.os = os;
    this.brand = brand;
    this.model = model;
    this.appVersion = appVersion;
  }

  public static com.mimo.common.rpc.proto.UserProto.DeviceDTO convert(DeviceDTO device) {
    Builder builder = com.mimo.common.rpc.proto.UserProto.DeviceDTO.newBuilder();
    if (StringUtils.hasText(device.getTerminal())) {
      builder.setTerminal(device.getTerminal());
    }
    if (StringUtils.hasText(device.getOs())) {
      builder.setOs(device.getOs());
    }
    if (StringUtils.hasText(device.getBrand())) {
      builder.setBrand(device.getBrand());
    }
    if (StringUtils.hasText(device.getModel())) {
      builder.setModel(device.getModel());
    }
    if (StringUtils.hasText(device.getAppVersion())) {
      builder.setAppVersion(device.getAppVersion());
    }
    return builder.build();
  }

  public static DeviceDTO convert(com.mimo.common.rpc.proto.UserProto.DeviceDTO proto) {
    DeviceDTO device = new DeviceDTO();
    device.setTerminal(proto.getTerminal());
    device.setOs(proto.getOs());
    device.setBrand(proto.getBrand());
    device.setModel(proto.getModel());
    device.setAppVersion(proto.getAppVersion());
    return device;
  }

  public String getTerminal() {
    return terminal;
  }

  public void setTerminal(String terminal) {
    this.terminal = terminal;
  }

  public String getOs() {
    return os;
  }

  public void setOs(String os) {
    this.os = os;
  }

  public String getBrand() {
    return brand;
  }

  public void setBrand(String brand) {
    this.brand = brand;
  }

  public String getModel() {
    return model;
  }

  public void setModel(String model) {
    this.model = model;
  }

  public String getAppVersion() {
    return appVersion;
  }

  public void setAppVersion(String appVersion) {
    this.appVersion = appVersion;
  }
}
