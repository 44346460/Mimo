package com.mimo.common.comet.dto.user;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import com.mimo.common.comet.dto.ZoneDTO;
import com.mimo.common.rpc.proto.UserProto;

@Valid
public class UserLoginReq {
  @NotEmpty
  private String uid;

  @NotEmpty
  private String token;

  @NotEmpty
  private String terminal; // 设备类型

  @NotEmpty
  private String version; // 客户端的SDK版本

  @Valid
  @NotNull
  private ZoneDTO zone;

  @NotEmpty
  private String ip;

  @NotEmpty
  private String deviceId;

  @Valid
  @NotNull
  private DeviceDTO device;

  public UserLoginReq() {
    super();
  }

  public UserLoginReq(@NotEmpty String uid, @NotEmpty String token, @Valid @NotNull ZoneDTO zone, @NotEmpty String ip,
      @NotEmpty String deviceId, @NotEmpty String terminal, @NotEmpty String version,
      @Valid @NotNull DeviceDTO device) {
    super();
    this.uid = uid;
    this.token = token;
    this.zone = zone;
    this.ip = ip;
    this.deviceId = deviceId;
    this.terminal = terminal;
    this.version = version;
    this.device = device;
  }

  public static UserLoginReq convert(UserProto.UserLoginReq req) {
    UserLoginReq ret = new UserLoginReq();
    ret.setUid(req.getUid());
    ret.setToken(req.getToken());
    ret.setIp(req.getIp());
    ret.setDeviceId(req.getDeviceId());
    ret.setTerminal(req.getTerminal());
    ret.setVersion(req.getVersion());

    ret.setZone(ZoneDTO.convert(req.getZone()));
    ret.setDevice(DeviceDTO.convert(req.getDevice()));

    return ret;
  }

  public static UserProto.UserLoginReq convert(UserLoginReq req) {
    return UserProto.UserLoginReq.newBuilder().setUid(req.getUid()).setDeviceId(req.getDeviceId()).setIp(req.getIp())
        .setToken(req.getToken()).setZone(ZoneDTO.convert(req.getZone())).setTerminal(req.getTerminal())
        .setVersion(req.getVersion()).setDevice(DeviceDTO.convert(req.getDevice())).build();
  }

  public String getTerminal() {
    return terminal;
  }

  public void setTerminal(String terminal) {
    this.terminal = terminal;
  }

  public String getVersion() {
    return version;
  }

  public void setVersion(String version) {
    this.version = version;
  }

  public ZoneDTO getZone() {
    return zone;
  }

  public void setZone(ZoneDTO zone) {
    this.zone = zone;
  }

  public String getUid() {
    return uid;
  }

  public void setUid(String uid) {
    this.uid = uid;
  }

  public String getToken() {
    return token;
  }

  public void setToken(String token) {
    this.token = token;
  }

  public String getIp() {
    return ip;
  }

  public void setIp(String ip) {
    this.ip = ip;
  }

  public String getDeviceId() {
    return deviceId;
  }

  public void setDeviceId(String deviceId) {
    this.deviceId = deviceId;
  }

  public DeviceDTO getDevice() {
    return device;
  }

  public void setDevice(DeviceDTO device) {
    this.device = device;
  }

}
