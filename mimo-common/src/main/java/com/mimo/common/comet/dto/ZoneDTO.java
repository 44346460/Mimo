package com.mimo.common.comet.dto;

import java.io.Serializable;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import org.springframework.validation.annotation.Validated;

/**
 * 用于描述用户在集群中的Location，即所在的物理区域
 *
 * @author Hongyu
 */
@Validated
public class ZoneDTO implements Serializable {
  
  private static final long serialVersionUID = 1L;

  @NotEmpty
  private String name;

  @NotEmpty
  private String ip;

  @NotNull
  private Integer port;

  public static com.mimo.common.rpc.proto.UserProto.ZoneDTO convert(ZoneDTO z) {
    return com.mimo.common.rpc.proto.UserProto.ZoneDTO.newBuilder().setIp(z.getIp()).setName(z.getName())
        .setPort(z.getPort()).build();
  }

  public static ZoneDTO convert(com.mimo.common.rpc.proto.UserProto.ZoneDTO proto) {
    ZoneDTO z = new ZoneDTO();
    z.setIp(proto.getIp());
    z.setName(proto.getName());
    z.setPort(proto.getPort());
    return z;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getIp() {
    return ip;
  }

  public void setIp(String ip) {
    this.ip = ip;
  }

  public Integer getPort() {
    return port;
  }

  public void setPort(Integer port) {
    this.port = port;
  }

  @Override
  public String toString() {
    return "ZoneDTO [name=" + name + ", ip=" + ip + ", port=" + port + "]";
  }

}
