package com.mimo.common.admin.logic;

import java.util.Date;

/**
 * 
 * @date 2021/1/29 11:52
 * @description
 **/
public class UserAdminDTO {

  private int page = 0;

  private int size = 10;

  private String userId;

  private Date begin;

  private Date end;

  private boolean online;

  public int getPage() {
    return page;
  }

  public void setPage(int page) {
    this.page = page;
  }

  public int getSize() {
    return size;
  }

  public void setSize(int size) {
    this.size = size;
  }

  public String getUserId() {
    return userId;
  }

  public void setUserId(String userId) {
    this.userId = userId;
  }

  public Date getBegin() {
    return begin;
  }

  public void setBegin(Date begin) {
    this.begin = begin;
  }

  public Date getEnd() {
    return end;
  }

  public void setEnd(Date end) {
    this.end = end;
  }

  public boolean isOnline() {
    return online;
  }

  public void setOnline(boolean online) {
    this.online = online;
  }
}
