package com.mimo.common.http.header;

/**
 * 收归当前系统使用所需要的请求头标记
 * 
 * @author Hongyu
 */
public abstract class HttpHeaderParams {

  /**
   * 下发给ShowMe的Key值
   */
  public static final String App_Key = "App-Key";

  /**
   * 客户端用于传输额外的信息内容
   */
  public static final String User_Agent = "User-Agent";

  /**
   * 用来标记请求来源,主要是用于区分来自于客户端的请求. 网关需要对客户端的请求进行标记.以便业务端进行识别
   */
  public static final String X_Man_For = "X-Man-For";

  /**
   * 用户端的语言区域信息
   */
  public static final String AcceptLanguage = "Accept-Language";

  /**
   * 用户端的设备Id
   */
  public static final String DEVICE_ID = "deviceId";

  /**
   * 用户端的设备上下文
   */
  public static final String TERMINAL = "Device";

  /**
   * 直接经过SLB的XFF
   */
  public static final String X_FORWARDED_FOR = "x-forwarded-for";

  /**
   * 经过Nginx的clientIp
   */
  public static final String CLIENT_IP = "clientip";

}
