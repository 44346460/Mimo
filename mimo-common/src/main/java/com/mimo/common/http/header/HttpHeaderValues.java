package com.mimo.common.http.header;

/**
 * 收归当前系统使用所需要的请求头标记中的一些特定值
 * 
 * @author Hongyu
 */
public abstract class HttpHeaderValues {

  /**
   * 请求头 "X_Man_For"的一种可选值,代表来自于客户端
   */
  public static final String X_Man_For_App = "xApp";
}
