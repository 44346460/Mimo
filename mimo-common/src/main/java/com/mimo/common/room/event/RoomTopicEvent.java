package com.mimo.common.room.event;

import java.util.Date;
import java.util.Objects;

import org.springframework.util.Assert;
import org.springframework.util.StringUtils;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.mimo.common.utils.RandomStringUtils;

public class RoomTopicEvent {

  protected String id;
  protected String from;
  protected String roomId;
  protected String member;
  protected RoomEventType type;
  protected Date timestamp;

  /**
   * @param from
   *          用于标记来自于哪一方的应用
   * @param roomId
   * @param member
   *          如果type为 MemberJoint或者MemberLeft,则member不能为空
   * @param type
   */
  @JsonCreator
  public RoomTopicEvent(@JsonProperty("from") String from, @JsonProperty("roomId") String roomId,
      @JsonProperty("member") String member, @JsonProperty("type") RoomEventType type) {
    super();
    Objects.requireNonNull(from);
    Objects.requireNonNull(roomId);
    Objects.requireNonNull(type);
    if (type == RoomEventType.MemberJoint || type == RoomEventType.MemberLeft) {
      Assert.isTrue(StringUtils.hasText(member), "type为 MemberJoint或者MemberLeft时,member不能为空");
    }
    this.from = from;
    this.roomId = roomId;
    this.member = member;
    this.type = type;
    this.id = RandomStringUtils.uniqueRandom();
    this.timestamp = new Date();
  }

  public static RoomTopicEvent createdEvent(String from, String roomId) {
    return new RoomTopicEvent(from, roomId, null, RoomEventType.Created);
  }

  public static RoomTopicEvent destoriedEvent(String from, String roomId) {
    return new RoomTopicEvent(from, roomId, null, RoomEventType.Destoried);
  }

  public static RoomTopicEvent jointEvent(String from, String roomId, String member) {
    return new RoomTopicEvent(from, roomId, member, RoomEventType.MemberJoint);
  }

  public static RoomTopicEvent leftEvent(String from, String roomId, String member) {
    return new RoomTopicEvent(from, roomId, member, RoomEventType.MemberLeft);
  }

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public Date getTimestamp() {
    return timestamp;
  }

  public void setTimestamp(Date timestamp) {
    this.timestamp = timestamp;
  }

  public String getFrom() {
    return from;
  }

  public void setFrom(String from) {
    this.from = from;
  }

  public String getRoomId() {
    return roomId;
  }

  public void setRoomId(String roomId) {
    this.roomId = roomId;
  }

  public String getMember() {
    return member;
  }

  public void setMember(String member) {
    this.member = member;
  }

  public RoomEventType getType() {
    return type;
  }

  public void setType(RoomEventType type) {
    this.type = type;
  }

}
