package com.mimo.common.room.event;

public enum RoomEventType {
  /**
   * 房间创建
   */
  Created,

  /**
   * 房间销毁
   */
  Destoried,

  /**
   * 成员加入
   */
  MemberJoint,

  /**
   * 成员离开
   */
  MemberLeft;
}
