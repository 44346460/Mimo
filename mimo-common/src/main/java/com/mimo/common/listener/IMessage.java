package com.mimo.common.listener;

/**
 * 主要用于提供类似于redis的pub/sub的抽象监听接口
 */
public interface IMessage {
  /**
   * 用于事件广播时的监听
   */
  void onMessage(String event);

  /**
   * 表明要监听的Topic
   * 
   * @param topic
   */
  String getTopic();

  /**
   * 一个统一的投递方式，一般来说，是往对应的topic投递
   * 
   * @param message
   *          该数据在发送时，内部会尽力转为json字符串输出
   */
  public default void send(Object message) {
    throw new UnsupportedOperationException();
  }
}
