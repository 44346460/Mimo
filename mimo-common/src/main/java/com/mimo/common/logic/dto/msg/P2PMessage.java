package com.mimo.common.logic.dto.msg;

/**
 * {"id":"b6b94bdfd9a04588ba8be87be06104a5","type":"P2P","from":"test","target":"hongyu","content":"AAAAAAAAAAA"}
 * 
 * @author Hongyu
 */

public class P2PMessage extends BaseDispatchMessage {
  public P2PMessage() {
    super();
    this.type = DispatchMsgType.P2P;
  }

}
