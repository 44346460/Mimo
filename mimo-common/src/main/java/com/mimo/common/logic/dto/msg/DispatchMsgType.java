package com.mimo.common.logic.dto.msg;

public enum DispatchMsgType {
  P2P(P2PMessage.class), Room2Peer(Room2PeerMessage.class), Peer2Room(Peer2RoomMessage.class);

  private Class<? extends BaseDispatchMessage> clz;

  DispatchMsgType(Class<? extends BaseDispatchMessage> clz) {
    this.clz = clz;
  }

  public Class<? extends BaseDispatchMessage> getClz() {
    return clz;
  }
}
