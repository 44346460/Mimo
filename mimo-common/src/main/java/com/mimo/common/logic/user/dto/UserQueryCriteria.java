package com.mimo.common.logic.user.dto;

import java.util.Date;

import io.swagger.annotations.ApiModelProperty;

public class UserQueryCriteria {

  @ApiModelProperty(value = "用户ID")
  private String userId;

  @ApiModelProperty(value = "起始时间(包含)")
  private Date begin;

  @ApiModelProperty(value = "截止时间(不含)")
  private Date end;

  private Boolean online;

  @ApiModelProperty(value = "页码")
  private int page = 0;

  @ApiModelProperty(value = "页长")
  private int size = 10;

  private Date loginBegin;

  private Date loginEnd;

  public String getUserId() {
    return userId;
  }

  public void setUserId(String userId) {
    this.userId = userId;
  }

  public Date getBegin() {
    return begin;
  }

  public void setBegin(Date begin) {
    this.begin = begin;
  }

  public Date getEnd() {
    return end;
  }

  public void setEnd(Date end) {
    this.end = end;
  }

  public Boolean getOnline() {
    return online;
  }

  public void setOnline(Boolean online) {
    this.online = online;
  }

  public int getPage() {
    return page;
  }

  public void setPage(int page) {
    this.page = page;
  }

  public int getSize() {
    return size;
  }

  public void setSize(int size) {
    this.size = size;
  }

  public Date getLoginBegin() {
    return loginBegin;
  }

  public void setLoginBegin(Date loginBegin) {
    this.loginBegin = loginBegin;
  }

  public Date getLoginEnd() {
    return loginEnd;
  }

  public void setLoginEnd(Date loginEnd) {
    this.loginEnd = loginEnd;
  }
}
