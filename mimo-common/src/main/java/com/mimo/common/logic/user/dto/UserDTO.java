package com.mimo.common.logic.user.dto;

import java.io.Serializable;
import java.util.Date;

import com.mimo.common.comet.dto.ZoneDTO;
import com.mimo.common.comet.dto.user.DeviceDTO;

public class UserDTO implements Serializable {

  private static final long serialVersionUID = 1L;

  private String uid;

  private String token;

  private String terminal; // 设备类型

  private String version; // 客户端的SDK版本

  private ZoneDTO zone;

  private boolean online;

  private String loginIp;

  private String deviceId;

  private DeviceDTO device;

  // 维护用户最近一次登陆的时间
  private Date loginDate;

  private Date createdDate;

  public String getUid() {
    return uid;
  }

  public void setUid(String uid) {
    this.uid = uid;
  }

  public String getToken() {
    return token;
  }

  public void setToken(String token) {
    this.token = token;
  }

  public String getTerminal() {
    return terminal;
  }

  public void setTerminal(String terminal) {
    this.terminal = terminal;
  }

  public String getVersion() {
    return version;
  }

  public void setVersion(String version) {
    this.version = version;
  }

  public ZoneDTO getZone() {
    return zone;
  }

  public void setZone(ZoneDTO zone) {
    this.zone = zone;
  }

  public boolean isOnline() {
    return online;
  }

  public void setOnline(boolean online) {
    this.online = online;
  }

  public String getLoginIp() {
    return loginIp;
  }

  public void setLoginIp(String loginIp) {
    this.loginIp = loginIp;
  }

  public String getDeviceId() {
    return deviceId;
  }

  public void setDeviceId(String deviceId) {
    this.deviceId = deviceId;
  }

  public DeviceDTO getDevice() {
    return device;
  }

  public void setDevice(DeviceDTO device) {
    this.device = device;
  }

  public Date getLoginDate() {
    return loginDate;
  }

  public void setLoginDate(Date loginDate) {
    this.loginDate = loginDate;
  }

  public Date getCreatedDate() {
    return createdDate;
  }

  public void setCreatedDate(Date createdDate) {
    this.createdDate = createdDate;
  }
}
