package com.mimo.common.logic.room.dto;

import org.springframework.validation.annotation.Validated;

import io.swagger.annotations.ApiModelProperty;

@Validated
public class RoomMemberQueryCriteria {

  @ApiModelProperty(value = "房间ID", required = true)
  String roomId;

  @ApiModelProperty(value = "用户ID")
  String userId;

  @ApiModelProperty(value = "页码")
  private int page = 0;

  @ApiModelProperty(value = "页长")
  private int size = 10;

  public String getRoomId() {
    return roomId;
  }

  public void setRoomId(String roomId) {
    this.roomId = roomId;
  }

  public String getUserId() {
    return userId;
  }

  public void setUserId(String userId) {
    this.userId = userId;
  }

  public int getPage() {
    return page;
  }

  public void setPage(int page) {
    this.page = page;
  }

  public int getSize() {
    return size;
  }

  public void setSize(int size) {
    this.size = size;
  }

}
