package com.mimo.common.logic.dto.msg;

import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiModelProperty.AccessMode;

/**
 * 主要用于房间里消息分发时,转到用户端时的消息结构
 * <p>
 * 即 room -----> user
 * 
 * @author Hongyu
 */
public class Room2PeerMessage extends BaseDispatchMessage {

  @ApiModelProperty(value = "推送到客户端时，需要知道来源的房间ID", required = true, accessMode = AccessMode.READ_ONLY)
  protected String roomId;

  @ApiModelProperty(value = "消息分拆用于映射回源消息ID", required = true, accessMode = AccessMode.READ_ONLY)
  protected String originalId;

  public Room2PeerMessage() {
    super();
    this.type = DispatchMsgType.Room2Peer;
  }

  public String getOriginalId() {
    return originalId;
  }

  public void setOriginalId(String originalId) {
    this.originalId = originalId;
  }

  public String getRoomId() {
    return roomId;
  }

  public void setRoomId(String roomId) {
    this.roomId = roomId;
  }

}
