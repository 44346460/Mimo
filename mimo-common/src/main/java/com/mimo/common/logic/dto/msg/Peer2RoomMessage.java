package com.mimo.common.logic.dto.msg;

/**
 * 表示 成员 -> 房间发言
 * 
 * @author Hongyu
 */
public class Peer2RoomMessage extends BaseDispatchMessage {

  public Peer2RoomMessage() {
    super();
    this.type = DispatchMsgType.Peer2Room;
  }

}
