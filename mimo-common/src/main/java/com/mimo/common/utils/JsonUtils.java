package com.mimo.common.utils;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Map;
import java.util.TimeZone;

import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * 建立于Jackson JSON框架的工具库包装
 * 
 * @author Hongyu
 */
public abstract class JsonUtils {

  private static final ObjectMapper mapper = new ObjectMapper();
  static {
    mapper.setSerializationInclusion(Include.NON_NULL);
    mapper.setTimeZone(TimeZone.getTimeZone("Asia/Chongqing"));
    mapper.setDateFormat(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"));
    mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
    mapper.configure(DeserializationFeature.ACCEPT_EMPTY_STRING_AS_NULL_OBJECT, true);
  }

  public static String toJsonString(Object obj) {
    try {
      return mapper.writeValueAsString(obj);
    } catch (JsonProcessingException e) {
      throw new IllegalArgumentException(e);
    }
  }

  public static byte[] toJsonBytes(Object obj) {
    try {
      return mapper.writeValueAsBytes(obj);
    } catch (JsonProcessingException e) {
      throw new IllegalArgumentException(e);
    }
  }

  public static <T> T parseJson(Map<String, ?> param, Class<T> clz) {
    try {
      return mapper.readValue(mapper.writeValueAsString(param), clz);
    } catch (IOException e) {
      throw new IllegalArgumentException(e);
    }
  }

  public static <T> T parseJson(String json, Class<T> clz) {
    try {
      return mapper.readValue(json, clz);
    } catch (IOException e) {
      throw new IllegalArgumentException(e);
    }
  }

  @SuppressWarnings({ "rawtypes", "unchecked" })
  public static <T> T parseJson(String json, TypeReference valueTypeRef) {
    try {
      return (T) mapper.readValue(json, valueTypeRef);
    } catch (IOException e) {
      throw new IllegalArgumentException(e);
    }
  }

}
