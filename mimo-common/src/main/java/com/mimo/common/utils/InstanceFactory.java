package com.mimo.common.utils;

import java.lang.reflect.Modifier;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import org.springframework.util.Assert;

/**
 * 用于内部自己管理缓存的类与类实例
 * 
 * @author Hongyu
 */
public abstract class InstanceFactory {
  private static Map<Class<?>, Object> INSTANCE_CACHES = new HashMap<>();
  private static Map<Class<?>, Class<?>> GENERIC_TYPE_CACHES = new HashMap<>();

  private static final Object INSTANCE_LOCK = new Object();
  private static final Object ARGS_LOCK = new Object();

  /**
   * 基于默认构造函数的对象创建
   * 
   * @param <T>
   * @param clz
   * @return
   */
  public static <T> T getInstance(Class<T> clz) {
    return getInstance(clz, null, null);
  }

  /**
   * 基于带参构造函数的对象创建
   * 
   * @param <T>
   * @param clz
   * @param argClz
   * @param argValue
   * @return
   */
  @SuppressWarnings("unchecked")
  public static <T> T getInstance(Class<T> clz, Class<?> argClz, Object argValue) {
    Objects.requireNonNull(clz);

    Object instance = INSTANCE_CACHES.get(clz);
    if (Objects.isNull(instance)) {
      synchronized (INSTANCE_LOCK) {
        instance = INSTANCE_CACHES.get(clz);
        // 不存在就创建
        if (Objects.isNull(instance)) {
          if (Objects.isNull(argClz) && Objects.isNull(argValue)) {
            instance = ReflectionUtils.newInstanceOf(clz);
          } else {
            instance = ReflectionUtils.newInstanceOf(clz, argClz, argValue);
          }
          INSTANCE_CACHES.put(clz, instance);
        }

        // 更新引用
        instance = INSTANCE_CACHES.get(clz);
      }
    }
    return (T) instance;
  }

  /**
   * @param clz
   *          当前查询的子类
   * @param baseClass
   *          对应于clz的最高层的抽象类,不支持接口, 因为接口无法保存泛型信息
   * @return
   */
  public static Class<?> getGenericType(Class<?> clz, Class<?> baseClass) {
    Objects.requireNonNull(clz);
    Objects.requireNonNull(baseClass);
    Assert.isTrue(!baseClass.isInterface() && Modifier.isAbstract(baseClass.getModifiers()), "baseClass 必须是抽象类");
    Class<?> ret = GENERIC_TYPE_CACHES.get(clz);
    if (Objects.isNull(ret)) {
      synchronized (ARGS_LOCK) {
        ret = GENERIC_TYPE_CACHES.get(clz);
        if (Objects.isNull(ret)) {
          Class<?> z = ReflectionUtils.getClass(clz.getGenericSuperclass(), 0);
          while (z != baseClass && baseClass.isAssignableFrom(z) && z != Object.class) {
            z = ReflectionUtils.getClass(z.getGenericSuperclass(), 0);
          }
          ret = z;
          GENERIC_TYPE_CACHES.put(clz, ret);
        }
        ret = GENERIC_TYPE_CACHES.get(clz);
      }
    }
    return ret;

  }

  /**
   * 用于获取类泛型化时的具体类型,如果泛型是体现于接口的, 那就永远是返回Object.
   * <p>
   * 同时, 由于类继承机制的原因, 该方法只会查询当前子类的上一级父类.不会自动查询整个继承树. 如果需要对继承树进行上行查找,则使用{@link #getGenericType(Class, Class)}
   * 
   * @param clz
   * @return
   */
  public static Class<?> getGenericType(Class<?> clz) {
    Objects.requireNonNull(clz);

    Class<?> ret = GENERIC_TYPE_CACHES.get(clz);
    if (Objects.isNull(ret)) {
      synchronized (ARGS_LOCK) {
        ret = GENERIC_TYPE_CACHES.get(clz);
        if (Objects.isNull(ret)) {
          ret = ReflectionUtils.getClass(clz.getGenericSuperclass(), 0);
          GENERIC_TYPE_CACHES.put(clz, ret);
        }
        ret = GENERIC_TYPE_CACHES.get(clz);
      }
    }
    return ret;
  }

}
