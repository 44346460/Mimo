package com.mimo.common.utils;

import org.springframework.util.StringUtils;

/**
 * 消息体内容的基本校验和验证
 * 
 * @author Hongyu
 */
public abstract class MessageUtil {

  // 主动消息内容,长度最大不能超过128K
  public static final int MAX_CONTENT_BYTES = 128 * 1024;

  /**
   * 使用默认的长度检测 ，最长为128KB
   * 
   * @param content
   * @return
   */
  public static boolean isValidLength(String content) {
    return isValidLength(content, 0, MAX_CONTENT_BYTES);
  }

  public static boolean isValidLength(String content, int min, int max) {
    return StringUtils.hasText(content) && (content.getBytes().length >= min && content.getBytes().length <= max);
  }

}
