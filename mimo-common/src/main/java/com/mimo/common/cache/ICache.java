package com.mimo.common.cache;

/**
 * <b>缓存使用,特别事项如下：</b>
 * <li>V作为返回值，请保持只读状态，切不能随意变更其信息。同一个对象多方操作，会导致缓存本身的意义丢失
 * <li>具体实现器如果为了防止业务方变更关联缓存的对象信息，则应该考虑永远的返回数据副本，以防止意外变更带来的不可控性
 * 
 * @param <K>
 *          key
 * @param <V>
 *          通过Key得到的Value
 * @param <E>
 *          往Key中添加元素时的Element，这里的E与V不一定同一个类型
 * @author Hongyu
 */
public interface ICache<K, V, E> {

  /**
   * 用于返回缓存中的命中统计,考虑到具体统计的内容变化性比较大，由上下文自行决定返回的结构类型
   * 
   * @return
   */
  Object getStatistics();

  void create(K key);

  /**
   * 添加缓存
   * 
   * @param key
   * @param element
   * @return 整个Value值
   */
  default V add(K key, E element) {
    return null;
  }

  /**
   * 移除K中对应缓存的元素
   * 
   * @param key
   * @param element
   * @return
   */
  default void remove(K key, E element) {
  }

  /**
   * 缓存获取
   * 
   * @param key
   * @return
   */
  default V get(K key) {
    return null;
  }

  /**
   * 清空整个缓存
   */
  default void clear() {

  }

  /**
   * 对Key做清空
   * 
   * @param key
   */
  default void clear(K key) {

  }
}
