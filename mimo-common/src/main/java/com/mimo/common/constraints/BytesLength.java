package com.mimo.common.constraints;

import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.ElementType.PARAMETER;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

/**
 * @author Hongyu
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = { BytesLengthValidator.class })
@Target({ FIELD, PARAMETER })
public @interface BytesLength {

  String message() default "内容长度不合法";

  Class<? extends Payload>[] payload() default {};

  Class<?>[] groups() default {};

  /**
   * @return size the element must be higher or equal to
   */
  int min() default 0;

  /**
   * @return size the element must be lower or equal to
   */
  int max() default Integer.MAX_VALUE;
}
