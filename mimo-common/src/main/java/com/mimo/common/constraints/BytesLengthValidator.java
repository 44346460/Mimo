package com.mimo.common.constraints;

import java.util.Objects;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import com.mimo.common.utils.MessageUtil;

public class BytesLengthValidator implements ConstraintValidator<BytesLength, String> {

  private int min;
  private int max;

  @Override
  public void initialize(BytesLength annotation) {
    this.min = annotation.min();
    this.max = annotation.max();
  }

  @Override
  public boolean isValid(String value, ConstraintValidatorContext context) {
    if (Objects.nonNull(value)) {
      return MessageUtil.isValidLength(value, min, max);
    }
    return true;
  }

}
