package com.mimo.common.result;

import java.io.Serializable;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * 定义一套基本的JSON返回类型，其中包括 返回码 及 返回信息
 */
@ApiModel(value = "BaseResult", description = "define the basic response")
public class BaseResult implements Serializable {

  private static final long serialVersionUID = 1L;

  protected static final int SUCCESS_CODE = 0;

  protected static final int SYSTEM_ERROR_CODE = -1;
  protected static final String SYSTEM_ERROR_MSG = "Something goes wrong, try it again.";

  public static final BaseResult SUCCESS_RET = new BaseResult();
  public static final BaseResult ERROR_RET = new BaseResult(SYSTEM_ERROR_CODE, SYSTEM_ERROR_MSG);

  protected int code;
  protected String msg;
  protected String token;

  public BaseResult() {
    code = SUCCESS_CODE;
    msg = "";
  }

  public BaseResult(int code, String msg) {
    this.code = code;
    this.msg = msg;
  }

  @ApiModelProperty(value = "标记响应结果", allowableValues = "true,false", required = true)
  public boolean isSuccess() {
    return code == SUCCESS_CODE;
  }

  @ApiModelProperty(value = "响应编码", required = true)
  public int getCode() {
    return code;
  }

  public void setCode(int code) {
    this.code = code;
  }

  @ApiModelProperty(value = "响应的详细信息", required = true)
  public String getMsg() {
    return msg;
  }

  public void setMsg(String msg) {
    this.msg = msg;
  }

  @ApiModelProperty(value = "认证Token", notes = "一般用于特定接口时，会返回。")
  public String getToken() {
    return token;
  }

  public void setToken(String token) {
    this.token = token;
  }

  @Override
  public String toString() {
    return "BaseResult [code=" + code + ", msg=" + msg + "]";
  }

}
