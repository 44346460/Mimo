package com.mimo.common.result;

import java.util.Collection;
import java.util.Objects;

/**
 * 主要用于一些通用的分类
 *
 * @author Hongyu
 * @param <T>
 */
public class CollectionResult<T> extends BaseResult {
  private static final long serialVersionUID = 1L;

  protected Collection<T> items;

  protected Long total;

  public CollectionResult() {
    super();
  }

  public CollectionResult(Collection<T> items) {
    super();
    Objects.requireNonNull(items);
    this.items = items;
    this.total = (long) items.size();
  }

  public CollectionResult(Collection<T> items, Long total) {
    super();
    Objects.requireNonNull(items);
    Objects.requireNonNull(total);
    this.items = items;
    this.total = total;
  }

  public Collection<T> getItems() {
    return items;
  }

  public void setItems(Collection<T> items) {
    this.items = items;
  }

  public Long getTotal() {
    return total;
  }

  public void setTotal(Long total) {
    this.total = total;
  }

}
