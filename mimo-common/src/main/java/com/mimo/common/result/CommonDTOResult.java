package com.mimo.common.result;

import io.swagger.annotations.ApiModelProperty;

/**
 * 对于只返回某些单个实体的DTO，可以直接继承或者包装其中
 * 
 * @author Hongyu
 * @param <T>
 */
public class CommonDTOResult<T> extends BaseResult {
  private static final long serialVersionUID = 1L;

  @ApiModelProperty(value = "此次单体响应的内容", required = true)
  private T detail;

  public CommonDTOResult() {
    super();
  }

  public CommonDTOResult(T detail) {
    super();
    this.detail = detail;
  }

  public T getDetail() {
    return detail;
  }

  public void setDetail(T detail) {
    this.detail = detail;
  }

}
