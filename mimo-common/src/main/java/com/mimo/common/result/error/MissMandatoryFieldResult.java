package com.mimo.common.result.error;

import com.mimo.common.result.BaseResult;

public class MissMandatoryFieldResult extends BaseResult {
  private static final long serialVersionUID = 1L;

  public MissMandatoryFieldResult(String msg) {
    code = 1111;
    this.msg = msg;
  }

}
