package com.mimo.common.result.error;

import com.mimo.common.result.BaseResult;

public class SignatureErrorResult extends BaseResult {
  public SignatureErrorResult() {
    code = 3001;
    msg = "Error Authentication";
  }
}
