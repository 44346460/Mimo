package com.mimo.common.result.error;

import com.mimo.common.result.BaseResult;

public class AccessLimitErrorResult extends BaseResult {
  public AccessLimitErrorResult() {
    code = 3002;
    msg = "Access Limited";
  }
}
