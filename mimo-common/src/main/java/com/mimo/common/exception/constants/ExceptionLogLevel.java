package com.mimo.common.exception.constants;

/**
 * 一般用于记录某些异常在统一日志处理时,以什么样的方式对日志做打印
 * 
 * @author Hongyu
 */
public enum ExceptionLogLevel {
  ERROR, WARNING, INFO, NONE
}