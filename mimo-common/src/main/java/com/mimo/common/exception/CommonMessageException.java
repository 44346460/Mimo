package com.mimo.common.exception;


/**
 * 用于处理只需要错误信息，无需返回特定状态码的异常场景
 */
public class CommonMessageException extends ApplicationException {

  public CommonMessageException(String messageKey, Object... args) {
    this.data = new Object[] { messageKey, args };
  }

}
