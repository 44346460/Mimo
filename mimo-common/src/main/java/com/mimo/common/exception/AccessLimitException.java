package com.mimo.common.exception;

import com.mimo.common.exception.constants.ExceptionLogLevel;

public class AccessLimitException extends ApplicationException {
  public AccessLimitException() {
    super();
    level = ExceptionLogLevel.WARNING;
  }

}
