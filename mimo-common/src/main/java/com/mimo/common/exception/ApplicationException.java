package com.mimo.common.exception;

import java.util.UUID;

import com.mimo.common.exception.constants.ExceptionLogLevel;

public class ApplicationException extends RuntimeException {

  private static final long serialVersionUID = 7544912576185580459L;

  protected final String exceptionId;

  // 用于附加此次异常的交互数据
  protected transient Object data;

  protected ExceptionLogLevel level;

  public ApplicationException() {
    super();
    level = ExceptionLogLevel.ERROR;
    exceptionId = uniqueId();
  }

  public ApplicationException(Object data) {
    this();
    this.data = data;
  }

  public ApplicationException(String message) {
    super(message);
    level = ExceptionLogLevel.ERROR;
    exceptionId = uniqueId();
  }

  public ApplicationException(String message, Throwable cause) {
    super(message, cause);
    level = ExceptionLogLevel.ERROR;
    exceptionId = uniqueId();
  }

  public ApplicationException(Throwable cause) {
    super(cause);
    level = ExceptionLogLevel.ERROR;
    exceptionId = uniqueId();
  }

  protected ApplicationException(String message, Throwable cause, boolean enableSuppression,
      boolean writableStackTrace) {
    super(message, cause, enableSuppression, writableStackTrace);
    exceptionId = uniqueId();
    level = ExceptionLogLevel.ERROR;
  }

  private static String uniqueId() {
    return UUID.randomUUID().toString().replace("-", "");
  }

  public String getExceptionId() {
    return exceptionId;
  }

  @Override
  public String getMessage() {
    return "Exception Id:" + exceptionId + "\t" + super.getMessage();
  }

  public Object getData() {
    return data;
  }

  public void setData(Object data) {
    this.data = data;
  }

  @Override
  public String toString() {
    return "Exception Id:" + exceptionId + "\t" + super.toString();
  }

  public ExceptionLogLevel getLevel() {
    return level;
  }

  public void setLevel(ExceptionLogLevel level) {
    this.level = level;
  }

}
