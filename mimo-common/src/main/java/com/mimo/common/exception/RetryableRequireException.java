package com.mimo.common.exception;

import com.mimo.common.exception.constants.ExceptionLogLevel;

/**
 * 用于业务上需要明确重试的异常需求提示
 * 
 * @author Hongyu
 */
public class RetryableRequireException extends ApplicationException {
  private static final long serialVersionUID = 1L;

  public RetryableRequireException() {
    super();
    this.level = ExceptionLogLevel.NONE;
  }

}
