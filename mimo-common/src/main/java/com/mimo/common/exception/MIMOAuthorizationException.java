package com.mimo.common.exception;

import com.mimo.common.exception.constants.ExceptionLogLevel;

/**
 * 用于统一处理自家平台上有接口非法访问的异常
 * 
 * @author Hongyu
 */
public class MIMOAuthorizationException extends ApplicationException {

  private static final long serialVersionUID = 1L;

  public MIMOAuthorizationException() {
    super();
    level = ExceptionLogLevel.NONE;
  }

}
