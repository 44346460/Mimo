package com.mimo.common.dto;

import java.io.Serializable;
import java.util.Objects;

public class Entry<K, V> implements Serializable {
  private static final long serialVersionUID = 1L;

  private K key;
  private V value;

  /**
   * 正常情况下，该构造参数主要用于反序列化的运行
   */
  public Entry() {
    super();
  }

  public Entry(K key, V value) {
    super();
    Objects.requireNonNull(key);
    this.key = key;
    this.value = value;
  }

  public K getKey() {
    return key;
  }

  public void setKey(K key) {
    this.key = key;
  }

  public V getValue() {
    return value;
  }

  public void setValue(V value) {
    this.value = value;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((key == null) ? 0 : key.hashCode());
    result = prime * result + ((value == null) ? 0 : value.hashCode());
    return result;
  }

  @Override
  public String toString() {
    return "Entry [key=" + key + ", value=" + value + "]";
  }

  @SuppressWarnings("rawtypes")
  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    Entry other = (Entry) obj;
    if (key == null) {
      if (other.key != null)
        return false;
    } else if (!key.equals(other.key))
      return false;
    if (value == null) {
      if (other.value != null)
        return false;
    } else if (!value.equals(other.value))
      return false;
    return true;
  }
}
