local key=KEYS[1];
local count=tonumber(ARGV[1]);
local expireMill=tonumber(ARGV[2]);
local preAlarmMill=expireMill-10;
local alarmKey=key..':ALARM';

if redis.call('EXISTS',alarmKey)==0 then
	redis.call('SET',alarmKey,alarmKey,'PX',preAlarmMill);
	redis.call('SET',key, 0,'PX',expireMill);
	return redis.call('INCR',key) <= count;
end;

if redis.call('TTL',key)==-1 then
	redis.call('PEXPIRE',key,expireMill);
end;
return redis.call('INCR',key) <= count;