package com.mimo.logic.message.service;

import java.util.Collection;

import com.mimo.common.logic.code.StatusCode;
import com.mimo.common.logic.dto.msg.BaseDispatchMessage;

/**
 * 作为所有消息的入口，主要是业务发送消息。
 * <li>P2P
 * <li>Room Message
 * <p>
 * 需要在业务处理之前，对消息进行先行持久化，消息分发之后 ，再对该消息进行清理.
 * <p>
 * <note>目前只对P2P消息要求客户端进行确认,而直播间消息，则以推送为主，成功与否，都不会再推
 * 
 * @author Hongyu
 */
public interface IMessageDispatcher {

  public static final int MAX_SLOT = 100;

  /**
   * 根据桶的Slot返回对应超时段的消息ID
   * 
   * @param slot
   * @param start
   * @param end
   * @param count
   * @return
   */
  Collection<String> getP2PMsgByBucketSlotAndExpireBetween(int slot, long start, long end, long count);

  /**
   * 对于消息发送，如果是发到房间里的消息，需要判断用户与房间的归属关系.即需要定义相关的响应体用于区分这种异常情况
   * <p>
   * 该方法为消息分发的主核心入口, 分发进broker前，需要对入口消息进行持久化，才能开始处理所谓的业务
   * 
   * @param msg
   * @return
   */
  public StatusCode dispatch(BaseDispatchMessage msg);

  public void onDispatch(BaseDispatchMessage msg);

  /**
   * 客户端对消息id进行确认
   * <p>
   * 服务在收到客户端的确认消息之后，会对数据立即清理
   * 
   * @param userId
   * @param msgId
   */
  public void ack(String userId, String msgId);

  /**
   * 直接清除对应的msgId
   * 
   * @param msgId
   */
  public void remove(String msgId);

  /**
   * 业务上用于返回特定用户未确认的通讯消息。
   * <li>目前只返回待确认的P2P消息
   * <li>考虑到有可能用户有大量的堆积消息(比如N万条消息),每次最多只返回1000条
   * 
   * @param userId
   * @return
   */
  public Collection<BaseDispatchMessage> loadUnacksByUserId(String userId);
}
