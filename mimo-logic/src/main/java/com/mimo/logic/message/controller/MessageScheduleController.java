package com.mimo.logic.message.controller;

import java.time.Duration;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mimo.common.result.BaseResult;
import com.mimo.logic.message.service.IMessageScheduleService;

import io.swagger.annotations.ApiOperation;

@Validated
@RestController
@RequestMapping("/message/schedule")
public class MessageScheduleController {
  private static final Logger log = LoggerFactory.getLogger(MessageScheduleController.class);

  private static final Duration THREE_DAYS = Duration.ofDays(3);

  private static final long MAX_COUNT = 10000;

  @Autowired
  private IMessageScheduleService messageClearService;

  @Scheduled(cron = "0 0 6 * * ?")
  @PostMapping(value = "/clear")
  @ApiOperation(value = "定时(每天凌晨六点)检查存量P2P消息的有效性(消息最多保存3天)", response = BaseResult.class)
  public BaseResult clear() {
    log.warn("开始清除过期消息");
    messageClearService.clear(THREE_DAYS, MAX_COUNT);
    log.warn("过期消息清除完成");
    return BaseResult.SUCCESS_RET;
  }

}
