package com.mimo.logic.message.service;

import com.mimo.common.logic.dto.msg.BaseDispatchMessage;
import com.mimo.common.logic.dto.msg.Peer2RoomMessage;

/**
 * 对消息上下行的频次限制进行抽取
 * 
 * @author Hongyu
 */
public interface IMessageUpstreamConstraintsService {

  public boolean checkPeerUpstreamConstraint(BaseDispatchMessage evt);

  public boolean checkP2RoomConstraint(Peer2RoomMessage evt);

}
