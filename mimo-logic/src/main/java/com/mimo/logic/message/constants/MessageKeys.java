package com.mimo.logic.message.constants;

/**
 * @author Hongyu
 */
public abstract class MessageKeys {

  /**
   * 用于控制定时任务在跑的时候,减少同时进入的可能
   * <p>
   * 此处为：过期消息清理
   */
  public static final String LOGIC_MSG_SCHEDULE_CLEAR_KEY = "logic:msg:schedule:clear:key";

  /**
   * 考虑到消息量有可能会非常的大，所以，此处添加一个尺寸为一百的HASH桶 <b> 即对消息ID进行 %100 得到桶的位置</b>
   * <p>
   * 采用ZET用于保存所有的消息体,其中Key为消息ID, score为消息时间,以全于后面对过期数据进行清理
   * <li>logic:msg:bucket:p2p:[0,100)
   * <li>evtId ------> timestamp
   */
  public static final String LOGIC_MSG_BUCKET_P2P_PATTERN = "logic:msg:bucket:p2p:{0}";

  /**
   * HASH
   * <li>logic:msg:bucket:p2p:detail:[0,100):
   * <li>evtId ---->json detail
   */
  public static final String LOGIC_MSG_BUCKET_P2P_DETAIL_PATTERN = "logic:msg:bucket:p2p:detail:{0}";

  /**
   * 和{@link #LOGIC_MSG_BUCKET_P2P_PATTERN}保持一致，桶序号也为[0,100)。用于存储每个消息的用户归属
   * <p>
   * 采用HASH存储所有的消息详情
   * <li>logic:msg:p2p:belongs:[0,100)
   * <li>evtId ----------> targetUserID
   */
  public static final String LOGIC_MSG_P2P_BELONGS_PATTERN = "logic:msg:p2p:belongs:{0}";

  /**
   * 存储单个用户所有待确认的ID消息详情
   * <p>
   * 基于HASH存储
   * <li>logic:msg:user:p2p:{uid}
   * <li>evtId ------>json
   */
  public static final String LOGIC_MSG_USER_P2P_PATTERN = "logic:msg:user:p2p:{0}";
}
