package com.mimo.logic.message.dto;

import java.util.Collection;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

import org.springframework.validation.annotation.Validated;

import com.mimo.common.constraints.BytesLength;
import com.mimo.common.utils.MessageUtil;

import io.swagger.annotations.ApiModelProperty;

@Validated
public class SendMessageDTO {

  @ApiModelProperty(value = "发送来源", required = true)
  @NotEmpty
  private String from;

  @ApiModelProperty(value = "目标ID(可能是房间ID，也可能是用户ID，取决于业务上下文),集合数量范围【1，10】", required = true)
  @NotEmpty
  @Size(min = 1, max = 10)
  private Collection<String> targets;

  @NotEmpty
  @BytesLength(message = "长度最大不能超过128K", max = MessageUtil.MAX_CONTENT_BYTES)
  private String content;

  public SendMessageDTO() {
    super();
  }

  public SendMessageDTO(String from, @NotEmpty Collection<String> targets, @NotEmpty String content) {
    super();
    this.from = from;
    this.targets = targets;
    this.content = content;
  }

  public SendMessageDTO(@NotEmpty Collection<String> targets, @NotEmpty String content) {
    super();
    this.targets = targets;
    this.content = content;
  }

  public String getFrom() {
    return from;
  }

  public void setFrom(String from) {
    this.from = from;
  }

  public Collection<String> getTargets() {
    return targets;
  }

  public void setTargets(Collection<String> targets) {
    this.targets = targets;
  }

  public String getContent() {
    return content;
  }

  public void setContent(String content) {
    this.content = content;
  }

  @Override
  public String toString() {
    return "SendMessageDTO [from=" + from + ", targets=" + targets + "]";
  }

}
