package com.mimo.logic.message.service;

import java.time.Duration;

/**
 * 针对消息的一些定时处理逻辑
 * 
 * @author Hongyu
 */
public interface IMessageScheduleService {

  /**
   * 定时清洗过期消息
   * 
   * @param expired
   * @param count
   */
  public void clear(Duration expired, long count);
}
