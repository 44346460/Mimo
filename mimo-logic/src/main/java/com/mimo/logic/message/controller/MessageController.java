package com.mimo.logic.message.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.mimo.common.logic.dto.msg.BaseDispatchMessage;
import com.mimo.common.result.BaseResult;
import com.mimo.common.result.CollectionResult;
import com.mimo.logic.message.service.IMessageDispatcher;

import io.swagger.annotations.ApiOperation;

@Validated
@RestController
@RequestMapping("/message")
public class MessageController {

  @Autowired
  private IMessageDispatcher messageService;

  @PostMapping(value = "/dispatch")
  @ApiOperation(value = "消息分发", notes = "会返回一个系统生成的消息ID,以便于客户端对消息进行确认", response = BaseResult.class)
  public BaseResult dispatch(@Valid @RequestBody BaseDispatchMessage msg) {
    messageService.dispatch(msg);
    return BaseResult.SUCCESS_RET;
  }

  @PostMapping("/list")
  @ApiOperation(value = "根据用户返回待确认的(单次最多1000条)所有消息", notes = "包括P2P，Room二类消息", response = BaseDispatchMessage.class)
  public BaseResult loadUnacksByUserId(@RequestParam String userId) {
    return new CollectionResult<>(messageService.loadUnacksByUserId(userId));
  }

}
