package com.mimo.logic.message.dto;

import com.mimo.common.result.BaseResult;

import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiModelProperty.AccessMode;

/**
 * 考虑到有一些场景，特别是批量接口的。存在部分异常的可能，所以，需要单独再做一个DTO用来表示个体元素的异常信息
 * 
 * @author Hongyu
 */
public class FailedReasonDTO {
  @ApiModelProperty(value = "发送的目标", accessMode = AccessMode.READ_ONLY)
  private String key;

  @ApiModelProperty(value = "发送失败的原因描述", accessMode = AccessMode.READ_ONLY)
  private BaseResult detail;

  public String getKey() {
    return key;
  }

  public void setKey(String key) {
    this.key = key;
  }

  public BaseResult getDetail() {
    return detail;
  }

  public void setDetail(BaseResult detail) {
    this.detail = detail;
  }

}
