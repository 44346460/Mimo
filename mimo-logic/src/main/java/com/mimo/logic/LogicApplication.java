package com.mimo.logic;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.autoconfigure.orm.jpa.HibernateJpaAutoConfiguration;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.context.annotation.Import;
import org.springframework.data.mongodb.config.EnableMongoAuditing;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;

import com.mimo.common.configuration.distributed.DistributedConfiguration;
import com.mimo.common.configuration.limit.AccessLimitConfiguration;
import com.mimo.common.configuration.security.rbac.AccessControlAutoConfiguration;
import com.mimo.logic.binder.LogicMessageDispatchProcessor;
import com.mimo.logic.binder.LogicMessageReceptionProcessor;
import com.spring4all.mongodb.EnableMongoPlus;

import springfox.documentation.swagger2.annotations.EnableSwagger2;

@EnableBinding(value = { LogicMessageDispatchProcessor.class, LogicMessageReceptionProcessor.class })
@EnableMongoAuditing
@EnableMongoPlus
@EnableScheduling
@EnableAsync
@EnableSwagger2
@EnableDiscoveryClient
@SpringBootApplication(exclude = { HibernateJpaAutoConfiguration.class, DataSourceAutoConfiguration.class })
@Import(value = { DistributedConfiguration.class, AccessControlAutoConfiguration.class,
    AccessLimitConfiguration.class })
public class LogicApplication {

  public static void main(String[] args) {
    SpringApplication.run(LogicApplication.class, args);
  }
}
