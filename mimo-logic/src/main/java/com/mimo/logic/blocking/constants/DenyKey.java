package com.mimo.logic.blocking.constants;

public abstract class DenyKey {

  private DenyKey() {
    throw new UnsupportedOperationException();
  }

  // 全局禁止（包括登陆以及一切用户主动行为）
  public static final String LOGIC_DENY_GLOBAL = "logic:deny:global:all";

  // 全局禁言
  public static final String LOGIC_DENY_GLOBAL_MUTE = "logic:deny:global:mute";

  // 基于用户视角，对来访者的block
  // zset结构，value为被禁止的用户, socre 用于表达特定超时日期
  public static final String LOGIC_DENY_P2P = "logic:deny:p2p:{0}";

  // 参数为room Id
  // zset结构，value为被禁止的用户, socre 用于表达特定超时日期
  public static final String LOGIC_DENY_ROOM_MUTE = "logic:deny:room:mute:{0}";

  // 参数为Room_ID
  // zset结构，value为被禁止的用户, socre 用于表达特定超时日期
  public static final String LOGIC_DENY_ROOM_JOINT = "logic:deny:room:joint:{0}";

}
