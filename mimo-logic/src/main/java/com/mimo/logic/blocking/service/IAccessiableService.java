package com.mimo.logic.blocking.service;

import com.mimo.common.logic.code.StatusCode;
import com.mimo.logic.blocking.constants.DenyOperation;
import com.mimo.logic.blocking.dto.AddDenyItemReq;
import com.mimo.logic.blocking.dto.RemoveDenyItemReq;

public interface IAccessiableService {
  /**
   * 一个简单的风控入口
   * <p>
   * 用于表示 user 是否在 target 的封禁列表中
   * 
   * @param user
   * @param target
   * @return
   */
  public StatusCode checkAccessiable(String user, String target, DenyOperation deny);

  public void addDenyItem(AddDenyItemReq req);

  public void removeDenyItem(RemoveDenyItemReq req);

  public void onRoomDestory(String roomId);

}
