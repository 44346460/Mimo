package com.mimo.logic.blocking.constants;

import com.mimo.common.logic.code.StatusCode;

/**
 * 以下的相关禁止动作会各自维护在其所在维度之中，即不同维度的添加和删减，不会影响其他维度的检测。<br>
 * 但是，维度在检测时，确有优先级之别，即按如下顺序：
 * <p>
 * 全局封禁--用户的所有主动行为都会被阻止,除了退出登陆和退出聊天室等事件。
 * <p>
 * 禁言系列
 * <li>全局禁言
 * <li>房间内禁言
 * <li>私信禁言
 * <p>
 * 加聊天室动作，则是单独的。
 * 
 * @author Hongyu
 */
public enum DenyOperation {
  // 全局封禁
  GlobalBlock(StatusCode.UserBlocked),

  // 全局禁言
  GlobalMute(StatusCode.RoomMemberMute),

  // 进入房间
  JoinRoom(StatusCode.RoomMemberKickoff),

  // 房间内
  RoomMute(StatusCode.RoomMemberMute),

  // 私信
  P2P(StatusCode.P2PMessageReject);

  StatusCode code;

  DenyOperation(StatusCode code) {
    this.code = code;
  }

  public StatusCode getStatusCode() {
    return this.code;
  }
}
