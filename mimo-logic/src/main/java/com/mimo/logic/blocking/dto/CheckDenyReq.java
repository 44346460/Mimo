package com.mimo.logic.blocking.dto;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import org.springframework.validation.annotation.Validated;

import com.mimo.common.utils.JsonUtils;
import com.mimo.logic.blocking.constants.DenyOperation;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * 检查封禁项
 * 
 * @author Hongyu
 */
@Validated
@ApiModel(value = "CheckDenyReq", description = "用于表达：source是否处于 target 的封禁列表中，其中如果封禁类型为全局类型，则target为空")
public class CheckDenyReq {

  @ApiModelProperty(value = "被操作用户", required = true)
  @NotEmpty
  private String source;

  @ApiModelProperty(value = "主控目标,对于全局型封禁，该字段可以为空", required = true)
  private String target;

  @NotNull
  @ApiModelProperty(value = "封禁类型", required = true)
  private DenyOperation type;

  public String getSource() {
    return source;
  }

  public void setSource(String source) {
    this.source = source;
  }

  public String getTarget() {
    return target;
  }

  public void setTarget(String target) {
    this.target = target;
  }

  public DenyOperation getType() {
    return type;
  }

  public void setType(DenyOperation type) {
    this.type = type;
  }

  @Override
  public String toString() {
    return JsonUtils.toJsonString(this);
  }
}
