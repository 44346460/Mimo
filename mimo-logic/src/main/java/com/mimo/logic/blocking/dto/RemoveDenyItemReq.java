package com.mimo.logic.blocking.dto;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import org.springframework.validation.annotation.Validated;

import com.mimo.common.utils.JsonUtils;
import com.mimo.logic.blocking.constants.DenyOperation;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@Validated
@ApiModel(value = "RemoveDenyItemReq", description = "用于表达：将source 从 target 的封禁列表中移除，其中如果封禁类型为全局类型，则target为空")
public class RemoveDenyItemReq {

  @ApiModelProperty(value = "被操作用户", required = true)
  @NotEmpty
  private String source;

  @ApiModelProperty(value = "主控用户,如果type为全局类型,则该字段允许为空", required = false)
  private String target;

  @NotNull
  private DenyOperation type;

  public String getSource() {
    return source;
  }

  public void setSource(String source) {
    this.source = source;
  }

  public String getTarget() {
    return target;
  }

  public void setTarget(String target) {
    this.target = target;
  }

  public DenyOperation getType() {
    return type;
  }

  public void setType(DenyOperation type) {
    this.type = type;
  }

  @Override
  public String toString() {
    return JsonUtils.toJsonString(this);
  }

}
