package com.mimo.logic.blocking.dto;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import org.springframework.validation.annotation.Validated;

import com.mimo.common.utils.JsonUtils;
import com.mimo.logic.blocking.constants.DenyOperation;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * 添加封禁项
 * <p>
 * 将source 加入到 target 的封禁列表中，其中如果封禁类型为全局类型，则target为空
 * 
 * @author Hongyu
 */
@Validated
@ApiModel(value = "AddDenyItemReq", description = "用于表达：将source 加入到 target 的封禁列表中，其中如果封禁类型为全局类型，则target为空")
public class AddDenyItemReq {

  @ApiModelProperty(value = "被操作用户", required = true)
  private String source;

  @ApiModelProperty(value = "主控目标,如果type为全局类型,则该字段允许为空", required = true)
  private String target;

  @NotNull(message = "拒绝类型 不允许为空")
  @ApiModelProperty(value = "拒绝类型", required = true)
  private DenyOperation type;

  @NotNull
  @Min(value = 1, message = "拒绝时长,单位时间是:分钟 取值范围是[1,600000]")
  @Max(value = 600_000, message = "拒绝时长,单位时间是:分钟 取值范围是[1,600000]")
  @ApiModelProperty(value = "拒绝时长,单位时间是:分钟 取值范围是[1,600000]", required = true)
  private Long period;

  public String getSource() {
    return source;
  }

  public void setSource(String source) {
    this.source = source;
  }

  public String getTarget() {
    return target;
  }

  public void setTarget(String target) {
    this.target = target;
  }

  public DenyOperation getType() {
    return type;
  }

  public void setType(DenyOperation type) {
    this.type = type;
  }

  public Long getPeriod() {
    return period;
  }

  public void setPeriod(Long period) {
    this.period = period;
  }

  @Override
  public String toString() {
    return JsonUtils.toJsonString(this);
  }

}
