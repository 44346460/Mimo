package com.mimo.logic.blocking.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mimo.common.configuration.security.rbac.annotation.AccessFor;
import com.mimo.common.result.BaseResult;
import com.mimo.common.result.CommonDTOResult;
import com.mimo.logic.blocking.dto.AddDenyItemReq;
import com.mimo.logic.blocking.dto.CheckDenyReq;
import com.mimo.logic.blocking.dto.RemoveDenyItemReq;
import com.mimo.logic.blocking.service.IAccessiableService;

import io.swagger.annotations.ApiOperation;

@Validated
@RestController
@RequestMapping("/block")
public class BlockController {

  @Autowired
  private IAccessiableService accessiableService;

  @AccessFor
  @PostMapping(value = "/check")
  @ApiOperation(value = "基于用户检查其下的封禁项", response = BaseResult.class)
  public BaseResult checkDenyItem(@Valid @RequestBody CheckDenyReq req) {
    return new CommonDTOResult<>(accessiableService.checkAccessiable(req.getSource(), req.getTarget(), req.getType()));
  }

  @AccessFor
  @PostMapping(value = "/add")
  @ApiOperation(value = "添加封禁项", response = BaseResult.class)
  public BaseResult addDenyItem(@Valid @RequestBody AddDenyItemReq req) {
    accessiableService.addDenyItem(req);
    return BaseResult.SUCCESS_RET;
  }

  @AccessFor
  @PostMapping(value = "/remove")
  @ApiOperation(value = "移除封禁项", response = BaseResult.class)
  public BaseResult removeDenyItem(@Valid @RequestBody RemoveDenyItemReq req) {
    accessiableService.removeDenyItem(req);
    return BaseResult.SUCCESS_RET;
  }

}
