package com.mimo.logic.binder;

import org.springframework.cloud.stream.annotation.Input;
import org.springframework.cloud.stream.annotation.Output;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.SubscribableChannel;

/**
 * 为了提高客户端的快速投递处理,所有的消息，一旦经过逻辑判断，直接进MQ，以提高吞吐率
 * 
 * @author Hongyu
 */
public interface LogicMessageReceptionProcessor {

  String OUTPUT = "LogicMessageReceptionOutput";

  String INPUT = "LogicMessageReceptionInput";

  @Input(INPUT)
  SubscribableChannel input();

  @Output(OUTPUT)
  MessageChannel output();

}
