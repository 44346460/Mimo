package com.mimo.logic.binder;

import org.springframework.cloud.stream.annotation.Output;
import org.springframework.messaging.MessageChannel;

/**
 * 消息通讯信息转储
 * 
 * @author Hongyu
 */
public interface LogicMessageDispatchProcessor {

  String OUTPUT = "LogicMessageDispatchOutput";

  @Output(OUTPUT)
  MessageChannel output();

}
