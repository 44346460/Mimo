package com.mimo.logic.config;

import static springfox.documentation.builders.PathSelectors.regex;

import java.util.Collections;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.context.request.async.DeferredResult;

import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;

@Configuration
public class SwaggerConfig {

  @Bean
  public Docket scheduleApi() {
    // @formatter:off
    return new Docket(DocumentationType.SWAGGER_2).groupName("Schedule").genericModelSubstitutes(DeferredResult.class)
        .useDefaultResponseMessages(false).forCodeGeneration(false).pathMapping("/").select()
        .paths(regex("/.*/schedule.*"))// 过滤的接口
        .build().apiInfo(new ApiInfo("定时任务接口", // title
            "用户会话,房间会话", // description
            "0.1.0", // version
            "", // termsOfServiceUrl
            new Contact("Hongyu", "", ""), // contact
            "", // license
            "",// licenseUrl
            Collections.emptyList()
    ));
    // @formatter:on
  }

  @Bean
  public Docket sessionApi() {
    // @formatter:off
    return new Docket(DocumentationType.SWAGGER_2).groupName("Session").genericModelSubstitutes(DeferredResult.class)
        .useDefaultResponseMessages(false).forCodeGeneration(false).pathMapping("/").select()
        .paths(regex("/session.*"))// 过滤的接口
        .build().apiInfo(new ApiInfo("用户绑定会话相关", // title
            "查询会话所在区域，查询所有当前绑定的会话信息，添加或者更新一个会话，强制会话过期", // description
            "0.1.0", // version
            "", // termsOfServiceUrl
            new Contact("Hongyu", "", ""), // contact
            "", // license
            "",// licenseUrl
            Collections.emptyList()
    ));
    // @formatter:on
  }

  @Bean
  public Docket metricApi() {
    // @formatter:off
    return new Docket(DocumentationType.SWAGGER_2).groupName("Metric").genericModelSubstitutes(DeferredResult.class)
        .useDefaultResponseMessages(false).forCodeGeneration(false).pathMapping("/").select()
        .paths(regex("/metric.*"))// 过滤的接口
        .build().apiInfo(new ApiInfo("业务系统统计的标量数据", // title
            "维度有 用户新增、用户登陆、单聊上行消息、单聊下行消息、房间上行消息、房间下行分发消息、房间创建", // description
            "0.1.0", // version
            "", // termsOfServiceUrl
            new Contact("Hongyu", "", ""), // contact
            "", // license
            "",// licenseUrl
            Collections.emptyList()
    ));
    // @formatter:on
  }

  @Bean
  public Docket blockApi() {
    // @formatter:off
    return new Docket(DocumentationType.SWAGGER_2).groupName("Blocking").genericModelSubstitutes(DeferredResult.class)
        .useDefaultResponseMessages(false).forCodeGeneration(false).pathMapping("/").select()
        .paths(regex("/block.*"))// 过滤的接口
        .build().apiInfo(new ApiInfo("多维度封禁", // title
            "全局封禁、禁止加入房间、房间内禁言、私信黑名单", // description
            "0.1.0", // version
            "", // termsOfServiceUrl
            new Contact("Hongyu", "", ""), // contact
            "", // license
            "",// licenseUrl
            Collections.emptyList()
    ));
    // @formatter:on
  }

  @Bean
  public Docket messageApi() {
    // @formatter:off
    return new Docket(DocumentationType.SWAGGER_2).groupName("Message").genericModelSubstitutes(DeferredResult.class)
        .useDefaultResponseMessages(false).forCodeGeneration(false).pathMapping("/").select()
        .paths(regex("/message.*"))// 过滤的接口
        .build().apiInfo(new ApiInfo("用于S2S的对外Http消息操作接口", // title
            "消息投递(点对点，房间消息)、获取待确认消息列表、过期消息的定时清洗", // description
            "0.1.0", // version
            "", // termsOfServiceUrl
            new Contact("Hongyu", "", ""), // contact
            "", // license
            "",// licenseUrl
            Collections.emptyList()
    ));
    // @formatter:on
  }

  @Bean
  public Docket roomApi() {
    // @formatter:off
    return new Docket(DocumentationType.SWAGGER_2).groupName("Room").genericModelSubstitutes(DeferredResult.class)
        .useDefaultResponseMessages(false).forCodeGeneration(false).pathMapping("/").select()
        .paths(regex("/room.*"))// 过滤的接口
        .build().apiInfo(new ApiInfo("用于S2S的对外Http房间操作接口", // title
            "房间(远程和本地缓存)维度的基本信息(创建、销毁、查询)、房间过期信息的清洗", // description
            "0.1.0", // version
            "", // termsOfServiceUrl
            new Contact("Hongyu", "", ""), // contact
            "", // license
            "",// licenseUrl
            Collections.emptyList()
    ));
    // @formatter:on
  }

  @Bean
  public Docket userApi() {
    // @formatter:off
    return new Docket(DocumentationType.SWAGGER_2).groupName("User").genericModelSubstitutes(DeferredResult.class)
        .useDefaultResponseMessages(false).forCodeGeneration(false).pathMapping("/").select()
        .paths(regex("/user.*"))// 过滤的接口
        .build().apiInfo(new ApiInfo("用于S2S的对外Http用户操作接口", // title
            "创建账户或者重置账户密码、强制登出、基础信息查看", // description
            "0.1.0", // version
            "", // termsOfServiceUrl
            new Contact("Hongyu", "", ""), // contact
            "", // license
            "",// licenseUrl
            Collections.emptyList()
    ));
    // @formatter:on
  }

}
