package com.mimo.logic.config.heartbeat;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration(proxyBeanMethods = false)
public class HeartbeatConfig {

  @Bean
  public HeartbeatAspect heartbeatAspect() {
    return new HeartbeatAspect();
  }

}
