package com.mimo.logic.room.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mimo.common.configuration.mrlock.annotation.DistributedRedisLock;
import com.mimo.logic.room.constants.RoomKeys;
import com.mimo.logic.room.service.IRoomScheduleService;
import com.mimo.logic.room.service.IRoomService;

@Service
public class RoomScheduleServiceImpl implements IRoomScheduleService {
  private static final Logger log = LoggerFactory.getLogger(RoomScheduleServiceImpl.class);

  @Autowired
  private IRoomService roomService;

  @Override
  @DistributedRedisLock(key = RoomKeys.LOGIC_ROOM_SCHEDULE_CLEAR_KEY, expired = 60, waited = 60)
  public void clear(long count) {
    roomService.getExpireRooms(count).forEach(r -> {
      log.warn("room[{}] 过期", r);
      roomService.delete(r);
    });
  }

}
