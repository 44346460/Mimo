package com.mimo.logic.room.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.mimo.common.cache.ICache;
import com.mimo.logic.room.service.IRoomService;
import com.mimo.logic.room.service.impl.RoomCache;

@Configuration
public class CacheConfig {

  @Bean
  public ICache<String, String[], String> roomCache(@Autowired IRoomService source) {
    return new RoomCache(source);
  }
}
