package com.mimo.logic.room.model;

import java.util.Date;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import org.springframework.validation.annotation.Validated;

import com.mimo.common.utils.JsonUtils;

import io.swagger.annotations.ApiModelProperty;

@Validated
public class RoomDefinitionInfo {

  // 默认的TTL
  public static final long DEFAULT = -1L;

  @NotEmpty
  @ApiModelProperty(value = "房间ID,调用方需要保证其唯一性", required = true)
  private String roomId;

  /**
   * 房间的拥有者，即管理者，一般是创建人，当然也可以由接口直接指定
   */
  @NotEmpty
  @ApiModelProperty(value = "房间的拥有者，即管理者，一般是创建人，当然也可以由接口直接指定", required = true)
  private String owner;

  /**
   * 房间允许的人数上限,一旦创建, 该字段则不允许再变更.
   * <p>
   * 人数上限取值范围为[1_000, 10_000]
   */
  @NotNull
  @Min(1000)
  @Max(10_000)
  @ApiModelProperty(value = "房间允许的人数上限,一旦创建, 该字段则不允许再变更.人数上限取值范围为[1_000, 10_000]", required = true)
  private Integer capacity;
  /**
   * 一个房间最大生存时间（单位秒，默认值为-1，取值范围为[-1, 3天] ）
   * <li>自创建开始，一旦到达ttl的规定时限，则房间会强制销毁
   * <li>如果值为-1，则系统认定不受ttl时限. 此时系统会存在二种处理方式
   * <ul>
   * <li>不定期检查房间人数, 当房间人数为0时，则强制销毁。此为系统目前默认行为
   * <li>考虑到一些特定的场景，需要保证房间的永久可用性，-1也代表着系统不会做任何主动的清除动作。统一由业务调用方自行删除
   * </ul>
   * <p>
   * 一旦创建, 该字段则不允许再变更
   */
  @Min(-1)
  @Max(3)
  @ApiModelProperty(value = "一个房间最大生存时间（单位秒，默认值为-1，取值范围为[-1, 3天] ) ,且不允许为 0", required = true)
  private long ttl = DEFAULT;

  @ApiModelProperty(value = "房间的创建日期", hidden = true)
  private Date createdDate = new Date();

  public String getOwner() {
    return owner;
  }

  public void setOwner(String owner) {
    this.owner = owner;
  }

  public String getRoomId() {
    return roomId;
  }

  public void setRoomId(String roomId) {
    this.roomId = roomId;
  }

  public Integer getCapacity() {
    return capacity;
  }

  public void setCapacity(Integer capacity) {
    this.capacity = capacity;
  }

  public long getTtl() {
    return ttl;
  }

  public void setTtl(long ttl) {
    this.ttl = ttl;
  }

  public Date getCreatedDate() {
    return createdDate;
  }

  public void setCreatedDate(Date createdDate) {
    this.createdDate = createdDate;
  }

  @Override
  public String toString() {
    return JsonUtils.toJsonString(this);
  }

}
