package com.mimo.logic.room.controller;

import javax.annotation.Resource;

import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.google.common.cache.CacheStats;
import com.mimo.common.cache.ICache;
import com.mimo.common.result.BaseResult;
import com.mimo.common.result.CommonDTOResult;

import io.swagger.annotations.ApiOperation;

@Validated
@RestController
@RequestMapping("/room/cache")
public class RoomCacheController {

  @Resource(name = "roomCache")
  private ICache<String, String[], String> roomCache;

  @GetMapping(value = "/statistics")
  @ApiOperation(value = "获取缓存的命中统计", response = CacheStats.class)
  public BaseResult getStatistics() {
    return new CommonDTOResult<>(roomCache.getStatistics().toString());
  }

  @PostMapping(value = "/load")
  @ApiOperation(value = "加载本地缓存的房间信息", response = String.class)
  public BaseResult load(@RequestParam String id) {
    return new CommonDTOResult<>(roomCache.get(id));
  }

  @PostMapping(value = "/clear")
  @ApiOperation(value = "清除本地房间特定信息", response = BaseResult.class)
  public BaseResult clear(@RequestParam String id) {
    roomCache.clear(id);
    return BaseResult.SUCCESS_RET;
  }

  @PostMapping(value = "/reset")
  @ApiOperation(value = "重置本地的房间缓存", response = BaseResult.class)
  public BaseResult reset() {
    roomCache.clear();
    return BaseResult.SUCCESS_RET;
  }

}
