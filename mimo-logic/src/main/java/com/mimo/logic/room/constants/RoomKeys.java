package com.mimo.logic.room.constants;

/**
 * @author Hongyu
 */
public abstract class RoomKeys {

  /**
   * 用于控制定时任务在跑的时候,减少同时进入的可能
   * <p>
   * 此处为：过期聊天室清理
   */
  public static final String LOGIC_ROOM_SCHEDULE_CLEAR_KEY = "logic:room:schedule:clear:key";

  /**
   * 用于加锁的key，粒度基于房间ID
   * <p>
   * e.g locker:logic:room:{gid}
   */
  public static final String LOGIC_ROOM_LOCKER_PREFIX = "locker:logic:room:";

  /**
   * 用于维护每一个房间模板（房间ID，生存时间，房间人数上限）的定义, 随着房间的创建而创建，销毁而销毁。
   * <p>
   * 以HASH方式
   * <li>roomId <---->模板本身定义信息
   */
  public static final String LOGIC_ROOM_DEFINITION = "logic:room:definition";

  /**
   * 由于TTL的存在,需要冗余一个ttl的ZSET来存在房间的未来超时日期
   * <p>
   * e.g roomId -------> expired timestamp
   */
  public static final String LOGIC_ROOM_TTL = "logic:room:ttl";

  /**
   * 用于所有的在线聊天室，以ZSET方式存储， room <---> createDate , 时间也是为了便于做清洗
   */
  public static final String LOGIC_ROOM_BUCKET = "logic:room:bucket";

  /**
   * 用于表示特定房间里的成员,以ZSET方式存储，uid <-----> jointDate, 时间也是为了便于清洗
   */
  public static final String LOGIC_ROOM_MEMBERS = "logic:room:{0}:members";

  /**
   * 冗余存储,用于维护房间里的当前群成员人数, 基于ZSET存储, roomId-----> size
   */
  public static final String LOGIC_ROOM_SIZE = "logic:room:size";

  /**
   * 用于跟踪用户目前所加入的房间,基于ZSET方式存储, roomId-->jointDate
   * <p>
   * etc.
   * <li>key:logic:room:user:hongyu ->{ room1: 1234512312}
   */
  public static final String LOGIC_ROOM_BY_USER = "logic:room:user:{0}";
}
