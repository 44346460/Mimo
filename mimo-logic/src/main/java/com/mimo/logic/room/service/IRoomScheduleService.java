package com.mimo.logic.room.service;

/**
 * 聊天室的定时任务抽取
 * 
 * @author Hongyu
 */
public interface IRoomScheduleService {

  /**
   * 定时清理过期的聊天室信息
   * 
   * @param count
   */
  public void clear(long count);
}
