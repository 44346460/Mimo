package com.mimo.logic.room.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mimo.common.result.BaseResult;
import com.mimo.logic.room.service.IRoomScheduleService;

import io.swagger.annotations.ApiOperation;

@Validated
@RestController
@RequestMapping("/room/schedule")
public class RoomScheduleController {
  private static final Logger log = LoggerFactory.getLogger(RoomScheduleController.class);

  private static final long MAX_COUNT = 10000;

  @Autowired
  private IRoomScheduleService roomScheduleService;

  @Scheduled(cron = "0 0 0/4 * * ?")
  @PostMapping(value = "/clear")
  @ApiOperation(value = "定时(每4个小时)检查房间有效性", response = BaseResult.class)
  public BaseResult clear() {
    log.warn("开始清除过期聊天室");
    roomScheduleService.clear(MAX_COUNT);
    log.warn("完成清除过期聊天室");
    return BaseResult.SUCCESS_RET;
  }

}
