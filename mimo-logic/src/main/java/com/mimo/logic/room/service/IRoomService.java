package com.mimo.logic.room.service;

import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import com.mimo.common.dto.Entry;
import com.mimo.common.logic.code.StatusCode;
import com.mimo.common.logic.room.dto.RoomMemberQueryCriteria;
import com.mimo.common.result.CollectionResult;
import com.mimo.logic.room.model.RoomDefinitionInfo;
import com.mimo.logic.room.model.RoomStatisticsDTO;

public interface IRoomService {

  /**
   * 查询需要清除的房间
   * 
   * @param count
   *          最大查询上限
   * @return
   */
  public Collection<String> getExpireRooms(long count);

  /**
   * 根据用户获取其已经加入的房间
   * 
   * @param userId
   * @return
   */
  public Collection<RoomStatisticsDTO> listJointRoomsByUserId(String userId);

  public Collection<String> listJointRooms(String userId);

  /**
   * 用于判断房间的存在性
   * 
   * @param roomId
   * @return
   */
  public boolean exists(String roomId);

  /**
   * 聚拢房间的基本信息
   * 
   * @param roomId
   * @return
   */
  public Optional<RoomStatisticsDTO> load(String roomId);

  /**
   * 用于判断用户和房间的成员关系
   * 
   * @param roomId
   * @param userId
   * @return
   */
  boolean isMemberOf(String roomId, String userId);

  /**
   * 根据房间模板创建房间
   * <p>
   * 同一个room id 重复创建，会返回false
   * 
   * @param definition
   * @return
   */
  public boolean create(RoomDefinitionInfo definition);

  public void delete(String roomId);

  /**
   * <li>用于用户重复加入房间,会统一返回true
   * <li>如果加入时，房间不存在, 则返回false
   * 
   * @param roomId
   * @param uid
   * @return
   */
  public StatusCode add(String roomId, String uid);

  /**
   * <li>用于用户重复离开房间,会统一返回true
   * <li>如果加入时，房间不存在, 则返回false
   * 
   * @param roomId
   * @param uid
   * @return
   */
  public boolean leave(String roomId, String uid);

  public int getSizeByRoom(String roomId);

  /**
   * 需要考虑房间数人太多的情
   *
   * @param roomId
   * @return
   */
  public Set<String> getUsersByRoom(String roomId);

  /**
   * 用于mimo后台查询所有的存在的房间
   *
   * @return
   */
  List<RoomStatisticsDTO> queryAllRooms();

  /**
   * 用于mimo后台查询某个特定的房间内得用户
   *
   * @param roomId
   * @return
   */

  CollectionResult<Entry<String, Double>> queryRoomMembers(RoomMemberQueryCriteria query);
}
