package com.mimo.logic.room.controller;

import java.util.Objects;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.Assert;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.mimo.common.configuration.security.rbac.annotation.AccessFor;
import com.mimo.common.dto.Entry;
import com.mimo.common.logic.room.dto.RoomMemberQueryCriteria;
import com.mimo.common.result.BaseResult;
import com.mimo.common.result.CollectionResult;
import com.mimo.common.result.CommonDTOResult;
import com.mimo.logic.room.model.RoomDefinitionInfo;
import com.mimo.logic.room.model.RoomStatisticsDTO;
import com.mimo.logic.room.service.IRoomService;

import io.swagger.annotations.ApiOperation;

@Validated
@RestController
@RequestMapping("/room")
public class RoomController {

  @Autowired
  private IRoomService roomService;

  @AccessFor
  @PostMapping(value = "/create")
  @ApiOperation(value = "新建房间", response = BaseResult.class)
  public BaseResult create(@Valid @RequestBody RoomDefinitionInfo definition) {
    Assert.isTrue(!Objects.equals(definition.getTtl(), 0L), "取值为-1，代表永远。正数为有效天数。不允许输入为0");
    roomService.create(definition);
    return BaseResult.SUCCESS_RET;
  }

  @GetMapping("/exists")
  @ApiOperation(value = "房间是否存在", response = Boolean.class)
  public BaseResult exists(@RequestParam String roomId) {
    return new CommonDTOResult<>(roomService.exists(roomId));
  }

  @PostMapping(value = "/member/leave")
  @ApiOperation(value = "成员离开房间", response = BaseResult.class)
  public BaseResult leave(@RequestParam String roomId, @RequestParam String userId) {
    roomService.leave(roomId, userId);
    return BaseResult.SUCCESS_RET;
  }

  @PostMapping(value = "/member/join")
  @ApiOperation(value = "成员加入房间", response = BaseResult.class)
  public BaseResult join(@RequestParam String roomId, @RequestParam String userId) {
    return roomService.add(roomId, userId).toBaseResult();
  }

  @AccessFor
  @PostMapping(value = "/destory")
  @ApiOperation(value = "销毁房间", response = BaseResult.class)
  public BaseResult destory(@RequestParam String roomId) {
    roomService.delete(roomId);
    return BaseResult.SUCCESS_RET;
  }

  @PostMapping(value = "/load")
  @ApiOperation(value = "加载房间信息", response = RoomStatisticsDTO.class)
  public BaseResult load(@RequestParam String roomId) {
    BaseResult ret = BaseResult.SUCCESS_RET;
    Optional<RoomStatisticsDTO> opt = roomService.load(roomId);
    if (opt.isPresent()) {
      ret = new CommonDTOResult<>(opt.get());
    }
    return ret;
  }

  @AccessFor
  @PostMapping(value = "/members/online")
  @ApiOperation(value = "用于返回房间当前在线人数", notes = "即使房间压根不存在,系统一样会返回数值,默认为0", response = Integer.class)
  public BaseResult getSizeOfMembers(@RequestParam String roomId) {
    return new CommonDTOResult<>(roomService.getSizeByRoom(roomId));
  }

  @PostMapping("/admin/list")
  @ApiOperation(value = "mimo后台查询所有的房间", response = RoomStatisticsDTO.class)
  public CollectionResult<RoomStatisticsDTO> query() {
    return new CollectionResult<>(roomService.queryAllRooms());
  }

  @PostMapping("/admin/members")
  @ApiOperation(value = "基于房间维度查询成员")
  public CollectionResult<Entry<String, Double>> queryRoomMembers(@Valid @RequestBody RoomMemberQueryCriteria query) {
    return roomService.queryRoomMembers(query);
  }

  @PostMapping("/admin/listJointRooms")
  @ApiOperation(value = "mimo后台根据userId查询该用户加入的所有的房间")
  public CollectionResult<RoomStatisticsDTO> listJointRooms(@RequestParam String userId) {
    return new CollectionResult<>(roomService.listJointRoomsByUserId(userId));
  }
}
