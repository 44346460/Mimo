package com.mimo.logic.room.service;

/**
 * 回调的主要目的有以下几个：
 * <li>允许对房间及其成员做本地缓存，以减少过多的网络IO
 * <li>允许对房间及其成员信息变动时，能产生广播消息，以便于其它的结点能同步更新本地的缓存
 * 
 * @author Hongyu
 */
public interface IRoomCallback {

  // 聊天室创建通知
  void onCreated(String roomId);

  // 聊天室销毁通知
  void onDestroy(String roomId);

  // 用户成功加入聊天室后通知
  void onJoint(String roomId, String uid);

  // 用户成功离开聊天室后通知
  void onLeft(String roomId, String uid);

}
