package com.mimo.logic.metric.constants;

/**
 * 基于自然日维度对信息进行统计, 于Redis上统一使用HASH类型，其中key部分为 yyyyMMdd
 * 
 * @author Hongyu
 */
public abstract class MetricKeys {

  // 用户创建
  public static final String LOGIC_METRIC_USER_CREATE_KEY = "logic:metric:user:create";
  // 用户登陆,同一个用户的登陆次数也会被反复计算
  public static final String LOGIC_METRIC_USER_LOGIN_KEY = "logic:metric:user:login";
  // 活跃用户数
  // 基于用户ID做UV去重
  public static final String LOGIC_METRIC_USER_ACTIVE_KEY = "logic:metric:user:active:{0}"; // 需要基于Set做去重，参数为日期

  // 个人消息
  public static final String LOGIC_METRIC_MSG_P2P_UPSTREAM_KEY = "logic:metric:msg:p2p:upstream";
  public static final String LOGIC_METRIC_MSG_P2P_DOWNSTREAM_KEY = "logic:metric:msg:p2p:downstream";

  // 房间消息
  public static final String LOGIC_METRIC_MSG_ROOM_UPSTREAM_KEY = "logic:metric:msg:room:upstream";
  public static final String LOGIC_METRIC_MSG_ROOM_DOWNSTREAM_KEY = "logic:metric:msg:room:downstream";

  // 房间创建
  public static final String LOGIC_METRIC_ROOM_CREATE_KEY = "logic:metric:room:create";

}
