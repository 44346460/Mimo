package com.mimo.logic.metric.service;

import java.util.Collection;
import java.util.Date;

import com.mimo.common.dto.Entry;
import com.mimo.logic.metric.constants.MetricType;

public interface IMetricService {

  /**
   * 累计该维度信息
   * 
   * @param type
   * @param source
   * @return
   */
  public long accumulate(MetricType type, String source);

  /**
   * 累计该维度信息
   * 
   * @param type
   * @param source
   * @param delta
   *          单次累计数量
   * @return 累计后的数值
   */
  public long accumulate(MetricType type, String source, long delta);

  public Collection<Entry<String, Long>> getMetric(MetricType type, Date from, Date to);
}
