package com.mimo.logic.metric.constants;

/**
 * 用于定义各个维度的标量
 * 
 * @author Hongyu
 */
public enum MetricType {
  // 用户新增
  User_Create,

  // 用户登陆维度
  User_Login,

  // 活跃用户维度
  User_Active,

  // 单聊上行消息
  Msg_P2P_Upstream,

  // 单聊下行
  Msg_P2P_Downstream,

  // 房间上行消息
  Msg_Room_Upstream,

  // 房间下行消息, 即消息分发情况
  Msg_Room_Downstream,

  // 房间创建
  Room_Create,
}
