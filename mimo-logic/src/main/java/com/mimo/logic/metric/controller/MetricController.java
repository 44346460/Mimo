package com.mimo.logic.metric.controller;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.mimo.common.result.BaseResult;
import com.mimo.common.result.CollectionResult;
import com.mimo.logic.metric.constants.MetricType;
import com.mimo.logic.metric.service.IMetricService;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@Validated
@RestController
@RequestMapping("/metric")
public class MetricController {
  @Autowired
  private IMetricService metricService;

  @PostMapping(value = "/query")
  @ApiOperation(value = "基于自然日格式定位统计维度的数值", response = Long.class)
  public BaseResult query(@RequestParam MetricType type,
      @ApiParam(required = true, name = "from", value = "基于yyyy-MM-dd HH:mm:ss的格式定位自然日,起始时间") @RequestParam Date from,
      @ApiParam(required = true, name = "to", value = "基于yyyy-MM-dd HH:mm:ss的格式定位自然日,结束时间") @RequestParam Date to) {
    return new CollectionResult<>(metricService.getMetric(type, from, to));
  }
}
