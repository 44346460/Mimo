package com.mimo.logic.session.service;

import java.util.Collection;
import java.util.Optional;

import org.springframework.lang.Nullable;

import com.mimo.common.comet.dto.ZoneDTO;
import com.mimo.logic.session.model.SessionInfo;

public interface ISessionService {

  /**
   * 返回特定用户的最近一次访问时间
   * 
   * @param uid
   * @return
   */
  Optional<Long> getLastAccessTimeByUid(String uid);

  /**
   * 用于获取最近一次心跳落在某个时间区间内的所有用户
   * 
   * @param start
   * @param end
   * @param count
   *          单次返回的数据量
   * @return
   */
  Collection<String> getByLastAccessTimeBetween(long start, long end, long count);

  /**
   * 维护会话的有效性, 需要一个心跳维持
   * 
   * @param uid
   * @param zone
   */
  boolean touch(String uid, @Nullable ZoneDTO zone);

  SessionInfo upsert(SessionInfo session);

  /**
   * 会话被强制清除的同时，会触发与会话绑定的相关房间也被离群处理
   * 
   * @param uid
   * @return 如果会话被成功删除，则返回被删除的对象。如果不存在，则返回null
   */
  Optional<SessionInfo> delete(String uid);

  /**
   * 根据用户ID返回会话信息
   * 
   * @param uid
   * @return 有可能会话不存在，则返回null
   */

  Optional<SessionInfo> findSessionByUser(String uid);

  Collection<SessionInfo> listAll();
}
