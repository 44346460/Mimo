package com.mimo.logic.session.constants;

/**
 * @author Hongyu
 */
public abstract class SessionKeys {

  /**
   * 用于控制定时任务在跑的时候,减少同时进入的可能
   * <p>
   * 此处为：过期会话清理
   */
  public static final String LOGIC_SESSION_SCHEDULE_CLEAR_KEY = "logic:session:schedule:clear:key";

  /**
   * 用于加锁的key，粒度基于会话中的用户ID
   */
  public static final String LOGIC_SESSION_LOCKER_PREFIX = "locker:logic:session:";

  /**
   * 用于所有的在线会话与最近一次心跳，以ZSET方式存储， uid <---> heart beat , 时间也是为了便于做清洗
   */
  public static final String LOGIC_SESSION_BEAT = "logic:session:beat";

  /**
   * 用于存储所有的在线会话详情, 以HASH方式存储， uid <------> session_detail, 主要是为了方便业务间的直接读取
   */
  public static final String LOGIC_SESSION_DETAILS = "logic:session:details";

}
