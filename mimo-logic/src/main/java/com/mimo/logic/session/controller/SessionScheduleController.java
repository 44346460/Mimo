package com.mimo.logic.session.controller;

import java.time.Duration;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mimo.common.result.BaseResult;
import com.mimo.logic.session.service.ISessionScheduleService;

import io.swagger.annotations.ApiOperation;

@Validated
@RestController
@RequestMapping("/session/schedule")
public class SessionScheduleController {
  private static final Logger log = LoggerFactory.getLogger(SessionScheduleController.class);

  private static final Duration SESSION_EXPIRE_MINS = Duration.ofMinutes(2);

  private static final long MAX_COUNT = 10000;

  @Autowired
  private ISessionScheduleService sessionScheduleService;

  @Scheduled(cron = "0 0/1 * * * ?")
  @PostMapping(value = "/clear")
  @ApiOperation(value = "定时(每分钟)检查会话有效性(会话超时2分钟)", response = BaseResult.class)
  public BaseResult clear() {
    log.warn("开始清除过期会话");
    sessionScheduleService.clear(SESSION_EXPIRE_MINS, MAX_COUNT);
    log.warn("清除过期会话完成");
    return BaseResult.SUCCESS_RET;
  }

}
