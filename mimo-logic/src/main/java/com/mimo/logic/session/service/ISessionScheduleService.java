package com.mimo.logic.session.service;

import java.time.Duration;

/**
 * 针对会话，做相关的定时任务
 * 
 * @author Hongyu
 */
public interface ISessionScheduleService {
  /**
   * 定时清洗过期会话
   * 
   * @param expired
   * @param count
   */
  public void clear(Duration expired, long count);
}
