package com.mimo.logic.session.model;

import java.util.Date;
import java.util.Objects;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import org.springframework.util.DigestUtils;
import org.springframework.validation.annotation.Validated;

import com.mimo.common.comet.dto.ZoneDTO;

@Validated
public class SessionInfo {

  @NotEmpty
  private String uid;

  /**
   * 每一次用户登陆后，需要在会话层实时的维护用户与具体物理连接之间的关系
   */
  @NotNull
  @Valid
  private ZoneDTO zone;

  /**
   * <li>一般来说,该字段等于createdDate，但如果一个用户快速登进，登出，则该字段会记录最近一次的时间
   * <li>该字段为冗余字段，也便于跟踪和清除脏数据时，提供一个标记
   */
  private Date lastModifiedDate;

  // 会话创建时间，一般为登陆认证成功后
  private Date createdDate;

  public String toMd5() {
    Objects.requireNonNull(uid, "用户ID不能为空");
    Objects.requireNonNull(zone, "用户登陆的区域不能为空");
    Objects.requireNonNull(zone.getIp(), "物理连接的所在服务器IP");
    Objects.requireNonNull(zone.getPort(), "物理连接的所在服务器端口");
    return DigestUtils.md5DigestAsHex((uid + zone.getName() + zone.getIp() + zone.getPort()).getBytes());
  }

  public String getUid() {
    return uid;
  }

  public void setUid(String uid) {
    this.uid = uid;
  }

  public ZoneDTO getZone() {
    return zone;
  }

  public void setZone(ZoneDTO zone) {
    this.zone = zone;
  }

  public Date getLastModifiedDate() {
    return lastModifiedDate;
  }

  public void setLastModifiedDate(Date lastModifiedDate) {
    this.lastModifiedDate = lastModifiedDate;
  }

  public Date getCreatedDate() {
    return createdDate;
  }

  public void setCreatedDate(Date createdDate) {
    this.createdDate = createdDate;
  }

}
