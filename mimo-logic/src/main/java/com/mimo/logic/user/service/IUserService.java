package com.mimo.logic.user.service;

import com.mimo.common.comet.dto.user.UserLoginReq;
import com.mimo.common.logic.code.StatusCode;
import com.mimo.logic.user.model.UserInfo;

public interface IUserService {

  public static final int MAX_USER_SLOT = 1000;

  public UserInfo load(String uid);

  /**
   * 用于验证该用户是否是真实存在的用户
   * 
   * @param uid
   * @return
   */
  public boolean isValid(String uid);

  /**
   * 不存在就创建，存在就overwrite，并返回最新的用户信息
   * <p>
   * Token的有效性为永久,但每次刷新时都会更新，并导致旧有的Token失效
   * 
   * @param uid
   * @param nickname
   * @return
   */
  public UserInfo generate(String uid);

  /**
   * 用于登陆验证
   * <p>
   * 业务上游应该根据登陆结果调整物理连接与用户的绑定关系
   * 
   * @return
   */
  public StatusCode login(UserLoginReq loginReq);

  /**
   * 退出登陆，不管是什么原因造成
   * 
   * @param uid
   * @return
   */
  public StatusCode logout(String uid);
}
