package com.mimo.logic.user.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "DeviceInfo")
public class DeviceInfo {
  /**
   * 基于用户ID的关联
   */
  @Id
  private String id;

  /**
   * 设备类型: WEB,android,ios
   */
  private String terminal;

  /**
   * 操作系统版本
   */
  private String os;

  /**
   * 设备品牌，比如OPPO，VIVO
   */
  private String brand;

  /**
   * 手机型号
   */
  private String model;

  /**
   * 宿主对应的应用版本，比如小象直播的应用版本
   */
  private String appVersion;

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getTerminal() {
    return terminal;
  }

  public void setTerminal(String terminal) {
    this.terminal = terminal;
  }

  public String getOs() {
    return os;
  }

  public void setOs(String os) {
    this.os = os;
  }

  public String getBrand() {
    return brand;
  }

  public void setBrand(String brand) {
    this.brand = brand;
  }

  public String getModel() {
    return model;
  }

  public void setModel(String model) {
    this.model = model;
  }

  public String getAppVersion() {
    return appVersion;
  }

  public void setAppVersion(String appVersion) {
    this.appVersion = appVersion;
  }

}
