package com.mimo.logic.user.dao;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.mimo.logic.user.model.DeviceInfo;

public interface IDeviceDao extends MongoRepository<DeviceInfo, String> {

}
