package com.mimo.logic.user.dao;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;

import com.mimo.logic.user.model.UserInfo;

public interface IUserDao extends MongoRepository<UserInfo, String>, QuerydslPredicateExecutor<UserInfo> {

}
