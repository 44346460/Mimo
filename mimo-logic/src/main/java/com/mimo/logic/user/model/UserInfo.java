package com.mimo.logic.user.model;

import java.util.Date;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.mongodb.core.index.CompoundIndex;
import org.springframework.data.mongodb.core.index.CompoundIndexes;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import com.mimo.common.utils.JsonUtils;

@Document(collection = "UserInfo")
@CompoundIndexes({ @CompoundIndex(def = "{'online':1}"), @CompoundIndex(def = "{'createdDate':-1}"),
    @CompoundIndex(def = "{'loginDate':-1}", sparse = true) })
public class UserInfo {
  @Id
  private String uid;

  private String token;

  private String terminal; // 设备类型

  private String version; // 客户端的SDK版本

  private String deviceId;// 用户最近一次登陆时的设备号

  @DBRef
  private ZoneInfo zone;

  @DBRef
  private DeviceInfo device;

  // 为了便于查询状态,维护一个在线状态
  private boolean online;

  // 维护用户最近一次登陆时的IP位置
  private String loginIp;

  // 维护用户最近一次登陆的时间
  @LastModifiedDate
  private Date loginDate;

  @CreatedDate
  private Date createdDate;

  public DeviceInfo getDevice() {
    return device;
  }

  public void setDevice(DeviceInfo device) {
    this.device = device;
  }

  public String getUid() {
    return uid;
  }

  public void setUid(String uid) {
    this.uid = uid;
  }

  public String getToken() {
    return token;
  }

  public void setToken(String token) {
    this.token = token;
  }

  public String getTerminal() {
    return terminal;
  }

  public void setTerminal(String terminal) {
    this.terminal = terminal;
  }

  public String getVersion() {
    return version;
  }

  public void setVersion(String version) {
    this.version = version;
  }

  public String getDeviceId() {
    return deviceId;
  }

  public void setDeviceId(String deviceId) {
    this.deviceId = deviceId;
  }

  public ZoneInfo getZone() {
    return zone;
  }

  public void setZone(ZoneInfo zone) {
    this.zone = zone;
  }

  public boolean isOnline() {
    return online;
  }

  public void setOnline(boolean online) {
    this.online = online;
  }

  public String getLoginIp() {
    return loginIp;
  }

  public void setLoginIp(String loginIp) {
    this.loginIp = loginIp;
  }

  public Date getLoginDate() {
    return loginDate;
  }

  public void setLoginDate(Date loginDate) {
    this.loginDate = loginDate;
  }

  public Date getCreatedDate() {
    return createdDate;
  }

  public void setCreatedDate(Date createdDate) {
    this.createdDate = createdDate;
  }

  @Override
  public String toString() {
    return JsonUtils.toJsonString(this);
  }

}
