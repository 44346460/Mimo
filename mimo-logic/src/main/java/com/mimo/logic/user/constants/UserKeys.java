package com.mimo.logic.user.constants;

/**
 * @author Hongyu
 */
public abstract class UserKeys {

  public static final String LOGIC_USER_LOCKER_PREFIX = "locker:logic:user:";

  /**
   * 用于记录所有的有效用户ID,使用ZSET用来记录。其中score在用户登陆和注册时，都会对score进行更新
   */
  public static final String LOGIC_USER_BUCKET_PATTERN = "logic:user:bucket:{0}";

}
