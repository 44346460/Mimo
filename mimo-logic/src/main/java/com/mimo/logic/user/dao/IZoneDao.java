package com.mimo.logic.user.dao;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.mimo.logic.user.model.ZoneInfo;

public interface IZoneDao extends MongoRepository<ZoneInfo, String> {

}
