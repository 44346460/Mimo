package com.mimo.logic.user.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * 用户最近一次登陆时，所属的服务器信息
 * 
 * @author Hongyu
 */
@Document(collection = "ZoneInfo")
public class ZoneInfo {
  @Id
  private String id;

  // 区域名称
  private String name;

  // 服务器IP
  private String ip;

  // 服务器端口
  private Integer port;

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getIp() {
    return ip;
  }

  public void setIp(String ip) {
    this.ip = ip;
  }

  public Integer getPort() {
    return port;
  }

  public void setPort(Integer port) {
    this.port = port;
  }

}
