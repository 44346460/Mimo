package com.mimo.logic.event;

import java.util.Objects;

import com.mimo.common.configuration.broadcast.event.NotificationEvent;
import com.mimo.logic.user.model.UserInfo;

/**
 * 由于用户的状态变化，联动性很高，所以，各自的logic module 统一使用该pub/sub进行分发处理，不允许在logic module内部显示调用其他module
 * 
 * @author Hongyu
 */
public class UserStatusActionEvent extends NotificationEvent {

  private UserInfo user;

  private UserStatusActionType action;

  public UserStatusActionEvent(Object source, UserInfo u, UserStatusActionType action) {
    super(source);
    Objects.requireNonNull(u);
    Objects.requireNonNull(action);
    this.action = action;
    this.user = u;
  }

  public UserInfo getUser() {
    return user;
  }

  public UserStatusActionType getAction() {
    return action;
  }

  @Override
  public String toString() {
    return "UserStatusActionEvent [user=" + user + ", action=" + action + "]";
  }

}
