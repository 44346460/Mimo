package com.mimo.logic.event;

public enum UserStatusActionType {
  Login, // 登陆
  Logout, // 主动登出
  Offline,// 非正常离线
  ;
}
